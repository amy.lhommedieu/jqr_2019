#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include "../3-3-2/src/cd_ll.c"

typedef struct dict_item
{
    int     hash; //index

    void *  p_key; //change to void *
    void *  p_value; //value associated with key, change to void *
    
} dict_item_t;

typedef struct hash_table
{
    //array of cd_lls. Those cd_lls are where the dict_items are
    cd_ll ** pp_table;  

    int size;
    int filled_slots; //# of the cd_lls with at least one element
    int largest_cd_ll; //greatest # of elements in one cd_ll

    int (*hash_func)(struct dict_item *p_di);
    dict_item_t *(*compare)
        (dict_item_t *p_di_1, dict_item_t *p_di_2, int *p_return_val);
    void (*print)(dict_item_t *p_di);
    //key data size
    //value data size
} hash_table_t;

void init_ht(hash_table_t *p_ht, int (*user_hash)(dict_item_t *p_di), 
    dict_item_t *(*user_compare)(dict_item_t *p_di_1, dict_item_t *p_di_2, int *p_return_val), 
    void (*user_print)(dict_item_t *p_di));
dict_item_t *create_di(void *p_k, void *p_v);
void insert_di(hash_table_t *p_ht, dict_item_t *p_di);
node_t * find_di(hash_table_t *p_ht, dict_item_t *p_di);
// void find_di(hash_table_t *p_ht, dict_item_t *p_di, int behavior /*0- next null, 1-p_di*/, int *p_return_index);
void free_di(void *p_di);
void free_ht(hash_table_t *p_ht);
void remove_di(hash_table_t *p_ht, dict_item_t *p_di);
void iterate_ht(hash_table_t *p_ht, void (*func)(dict_item_t *p_di));
bool low_space(hash_table_t *p_ht);
void grow_ht(hash_table_t *p_ht);
bool is_ht_empty(hash_table_t *p_ht);
void reorder_di(hash_table_t *p_ht);
bool is_di_null(dict_item_t *p_di);

//user ht functions
int hash_di_string(dict_item_t *p_di);
dict_item_t *compare_di_string(dict_item_t *p_di_1, dict_item_t *p_di_2, int *p_return_val);
char * ret_di_string(dict_item_t *p_di);
void print_di_string(dict_item_t *p_di);

//user cd_ll functions
node_t *compare_nodes_di(node_t *p_node_di_1, node_t *p_node_di_2, int *p_return_val/*0 if equal, 1 for 1>2, 2 for 1<2, -1 for error*/);
void * find_cd_ll_di(node_t **n); //top layer cd_ll looks into its data(cd_ll) and can then find the data.

int main()
{
    
    hash_table_t *p_table = (hash_table_t *)malloc(sizeof(hash_table_t));

    assert(p_table);

    init_ht(p_table, &hash_di_string, &compare_di_string, &print_di_string);

    char * key1 = "Amy00000";
    void * v_key1 = key1;
    char * val1 = "L'Hommedieu";
    void * v_val1 = val1;
    dict_item_t *one = create_di(v_key1, v_val1);
    printf("Hash 1: %d\n", hash_di_string(one));

    char * key2 = "Tyler000";
    void * v_key2 = key2;
    char * val2 = "L'Hommedieu";
    void * v_val2 = val2;
    dict_item_t *two = create_di(v_key2, v_val2);
    printf("Hash 2: %d\n", hash_di_string(two));

    char * key3 = "Becky000";
    void * v_key3 = key3;
    char * val3 = "L'Hommedieu";
    void * v_val3 = val3;
    dict_item_t *three = create_di(v_key3, v_val3);
    printf("Hash 3: %d\n", hash_di_string(three));

    char * key4 = "Greg0000";
    void * v_key4 = key4;
    char * val4 = "L'Hommedieu";
    void * v_val4 = val4;
    dict_item_t *four = create_di(v_key4, v_val4);
    printf("Hash 4, %d\n", hash_di_string(four));

    insert_di(p_table, one);
    insert_di(p_table, two);
    insert_di(p_table, three);
    insert_di(p_table, four);

    printf("ret_di_string: %s\n", ret_di_string(one));
    printf("print_di_string: ");
    print_di_string(one);

    remove_di(p_table, one);
    remove_di(p_table, two);
    remove_di(p_table, three);
    remove_di(p_table, four);

    free_ht(p_table);
    
    return 1;
    //test all other functions
}


void init_ht(hash_table_t *p_ht, int (*user_hash)(dict_item_t *p_di), 
    dict_item_t *(*user_compare)(dict_item_t *p_di_1, dict_item_t *p_di_2, int *p_return_val), 
    void (*user_print)(dict_item_t *p_di))
{
    p_ht->size = 10; //start size
    p_ht->filled_slots = 0;
    p_ht->largest_cd_ll = 0;

    //points to memory that is 10x larger than one cd_ll. use pointer math to 
    //go thru the different indices of the array of cd_lls.
    p_ht->pp_table = (cd_ll **)malloc(sizeof(cd_ll*) * p_ht->size);
    assert(p_ht->pp_table);

    //for loop to fill p_ht->pp_table with 10 node_t whose data points to an independent cd_ll
    for(int x = 0; x < p_ht->size; x++){
        p_ht->pp_table[x] = (cd_ll*)malloc(sizeof(cd_ll));
        assert(p_ht->pp_table[x]);
        init(p_ht->pp_table[x], &free_di, &compare_nodes_di, NULL, &find_cd_ll_di);
    }

    p_ht->hash_func = user_hash;
    p_ht->compare = user_compare;
    p_ht->print = user_print;
}


dict_item_t *create_di(void *p_k, void *p_v){
    dict_item_t *new_di = (dict_item_t *)malloc(sizeof(dict_item_t));
    assert(new_di);
    if(NULL == p_k || NULL == p_v){
        printf("Error in create_di. Tried to create with null data.\n");
        return NULL;
    }

    new_di->p_key = p_k;
    new_di->p_value = p_v;

    new_di->hash = -1; 

    return new_di;
}


void insert_di(hash_table_t *p_ht, dict_item_t *p_di){
    if(NULL != p_ht || NULL == p_di){
        if(low_space(p_ht)){
            grow_ht(p_ht);
        }

        if(-1 == p_di->hash){
            p_ht->hash_func(p_di);
        }
        int index = p_di->hash % p_ht->size;

        node_t *new_node = create_node(p_di);
        cd_ll *temp_list = p_ht->pp_table[index];
        if(is_empty(temp_list)){
            p_ht->filled_slots++;
        }
        append_node(new_node, temp_list);
        if(p_ht->largest_cd_ll < temp_list->size){
            p_ht->largest_cd_ll = temp_list->size;
        }
    }
    return;
}


node_t *find_di(hash_table_t *p_ht, dict_item_t *p_di){
    if(NULL == p_ht || NULL == p_di){
        return NULL;
    }

    int index = p_di->hash % p_ht->size;
    
    cd_ll *temp_list = p_ht->pp_table[index];
    node_t *ret = find_node(p_di, temp_list);

    return ret;
}

void free_di(void *p_di){
    dict_item_t *temp_di = (dict_item_t *)p_di;
    // free(temp_di->p_key);
    temp_di->p_key = NULL;
    // free(temp_di->p_value);
    temp_di->p_key = NULL;
    temp_di->p_value = NULL;
    free(temp_di);
    temp_di = NULL;
}


void free_ht(hash_table_t *p_ht){
    if(NULL != p_ht){
        for(int x = 0;x < p_ht->size; x++){
            free_cd_ll(p_ht->pp_table[x]);
        }
    }
    free(p_ht->pp_table);
    free(p_ht);
}


void remove_di(hash_table_t *p_ht, dict_item_t *p_di){
    node_t *ret = find_di(p_ht, p_di);
    assert(ret);

    delete_node(ret, p_ht->pp_table[p_di->hash % p_ht->size]);
    p_ht->filled_slots--;

    return;
}


void iterate_ht(hash_table_t *p_ht, void (*func)(dict_item_t *p_di)){
    return;
}


bool low_space(hash_table_t *p_ht){
    bool ret = false;
    int max_size = p_ht->size * 3 / 4;
    int curr_size = p_ht->filled_slots;
    int biggest_cd_ll = p_ht->largest_cd_ll;
    if(max_size <= curr_size || biggest_cd_ll > p_ht->size/2){
        ret = true;
    }
    return ret;
}


void grow_ht(hash_table_t *p_ht){
    assert(p_ht);
    int x = p_ht->size;

    p_ht->size = p_ht->size * 2;

    //more cd_lls to match the size
    p_ht->pp_table = (cd_ll **)realloc(p_ht->pp_table, sizeof(cd_ll*) * p_ht->size);
    assert(p_ht->pp_table);

    for(;x < p_ht->size; x++){
        p_ht->pp_table[x] = (cd_ll*)malloc(sizeof(cd_ll));
        assert(p_ht->pp_table[x]);
        init(p_ht->pp_table[x], &free_di, &compare_nodes_di, NULL, &find_cd_ll_di);
    }

    //go through all the current dict_items and reorganize with the new size.
    reorder_di(p_ht);

    return;
}


bool is_ht_empty(hash_table_t *p_ht){
    int ret = 1;
    if(NULL == p_ht){
        printf("ht is NULL\n");
    }
    else if(0 == p_ht->size){
        printf("ht has a size 0\n");
    }
    else{
        for(int x = 0; x < p_ht->size; x++){
            if(!is_empty(p_ht->pp_table[x])){
                ret = 0;
            }
        }
    }
    return ret;
}

void reorder_di(hash_table_t *p_ht){
    if(!is_ht_empty(p_ht)){
        int count = 0;
        dict_item_t ** temp_array = (dict_item_t **) malloc(sizeof(dict_item_t *));
        //Check all the cd_lls
        for(int x = 0; x < p_ht->size; x ++){
            //If it contains a node_t pop it and put the di onto the temp_array
            while(!is_empty(p_ht->pp_table[x])){
                node_t * temp_node = pop(p_ht->pp_table[x]);
                p_ht->filled_slots--;
                if(NULL != temp_node){
                    count++;
                    temp_array = (dict_item_t **) realloc(temp_array, sizeof(dict_item_t *) * count);
                    temp_array[count-1] = (dict_item_t *)temp_node->data;
                }
            }
        }
        for(int x = 0; x < count; x++){
            if(NULL != temp_array[x]){
                insert_di(p_ht, temp_array[x]);
            }            
        }
        free(temp_array);
    }
    return;
}


bool is_di_null(dict_item_t *p_di){
    bool ret = 0;

    if(NULL == p_di){
        ret = 1;
    }
    if(NULL == p_di->p_key){
        ret = 1;
    }
    return ret;
}



//user ht functions
int hash_di_string(dict_item_t *p_di){
    char *c = strdup(p_di->p_key);
    assert(c);

    int x = sizeof(c) / sizeof(char);
    
    int sum = 0;
    for(int i = 0; i < x; i++){
        sum = sum + c[i];
    }
    free(c);
    p_di->hash = sum;
    return sum;
}


dict_item_t *compare_di_string(dict_item_t *p_di_1, dict_item_t *p_di_2, int *p_return_val/*0 if equal, 1 for 1>2, 2 for 1<2, -1 for error*/){
    dict_item_t *ret = NULL;

    if(NULL == p_di_1 && NULL == p_di_2){
        return NULL;
    }
    else if(NULL == p_di_1){
        return p_di_2;
    }
    else if(NULL == p_di_2){
        return p_di_1;
    }
    else{
        int hash_1 = hash_di_string(p_di_1);
        int hash_2 = hash_di_string(p_di_2);
        if(hash_1 == hash_2){
            *p_return_val = 0;
            return NULL;
        }
        else if(hash_1 > hash_2){
            *p_return_val = 1;
            ret = p_di_1;
            return NULL;
        }
        else if(hash_1 < hash_2){
            *p_return_val = 2;
            ret = p_di_2;
            return NULL;
        }
        else{
            *p_return_val = -1;
            return ret;
        }    
    } 
}


char * ret_di_string(dict_item_t *p_di){
    if(!is_di_null(p_di)){
        return (char*)p_di->p_key;
    }
    return NULL;
}


void print_di_string(dict_item_t *p_di){
    if(!is_di_null(p_di)){
        printf("%s\n", (char*)p_di->p_key);
    }
    return;
}


//user cd_ll functions
node_t *compare_nodes_di(node_t *p_node_di_1, node_t *p_node_di_2, int *p_return_val/*0 if equal, 1 for 1>2, 2 for 1<2, -1 for error*/){
    node_t *ret_node = NULL;
    int *ret_int = (int*)malloc(sizeof(int));
    
    dict_item_t *temp_di = compare_di_string((dict_item_t*)p_node_di_1->data, (dict_item_t*)p_node_di_2->data, ret_int);
    
    *p_return_val = *ret_int;
    if(temp_di == (dict_item_t *) p_node_di_1->data){
        ret_node = p_node_di_1;
    }
    else if(temp_di == (dict_item_t *) p_node_di_2->data){
        ret_node = p_node_di_2;
    }

    free(ret_int);
    return ret_node;
}


void * find_cd_ll_di(node_t **n){ //top layer cd_ll looks into its data(cd_ll) and can then find the data.
    return NULL;
}
