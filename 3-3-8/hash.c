#include <stdio.h>
#include <stdlib.h>
// #include <>

#define SIZE 10

//3.1.13 & 3.3.8

struct dict_item{
    char key; //char *
    int val;
    int og_val; //only used if there is a collision
};

struct hash_table{
    struct dict_item* table[SIZE];

    int size;
    int remaining;
};

struct dict_item *create_item(struct hash_table *table, char key);
int simple_hash_func(struct hash_table *table, char data); //char *
void insert_item(struct hash_table *table, struct dict_item *new_item);
//curr_val helps handle collisions, and will be used in the recursion to affect the flag
//flag value of 1 means this is the first look.
struct dict_item* find_item(struct hash_table *table, int curr_val, char key, int flag/*first call 1*/);
struct dict_item* find_index_item(struct hash_table *table, int elem_num);
void print_table(struct hash_table *table);
void print_dict_item(struct dict_item *elem);
void remove_dict_item(struct hash_table *table, struct dict_item *item);
struct hash_table *grow_table(struct hash_table *table);

int main(){
    struct hash_table *hash_table = (struct hash_table *)malloc(sizeof(struct dict_item *) * (SIZE+1));
    if (hash_table == NULL) { 
        printf("Memory not allocated.\n"); 
        exit(0); 
    } 

    hash_table->remaining = 10;
    hash_table->size = 10;

    for(int i = 0; i < hash_table->size; i++){
        hash_table->table[i] = NULL;
    }

    struct dict_item *a = create_item(hash_table, 'a');
    insert_item(hash_table, a);
    // printf("Table now contains %c\n", (hash_table->table[a->val])->key);
    
    struct dict_item *x = create_item(hash_table, 'x');
    insert_item(hash_table, x);
    // printf("Table now contains %c\n", (hash_table->table[x->val])->key);

    struct dict_item *l = create_item(hash_table, 'l');
    insert_item(hash_table, l);
    // printf("Table now contains %c\n", (hash_table->table[l->val])->key);
    
    struct dict_item *q = create_item(hash_table, 'q');
    insert_item(hash_table, q);
    // printf("Table now contains %c\n", (hash_table->table[q->val])->key);
    
    struct dict_item *t = create_item(hash_table, 't');
    insert_item(hash_table, t);
    // printf("Table now contains %c\n", (hash_table->table[t->val])->key);

    struct dict_item *z = create_item(hash_table, 'a');
    insert_item(hash_table, z);
    // printf("Table now contains %c\n", (hash_table->table[z->val])->key);

    struct dict_item *test = find_item(hash_table, simple_hash_func(hash_table, 't'), 't', 1);

    print_table(hash_table);
    test = find_index_item(hash_table, 4);
    print_dict_item(test);

    while(hash_table->remaining != 10){
        struct dict_item *temp = find_index_item(hash_table, 1);
        print_dict_item(temp);
        remove_dict_item(hash_table, temp);
    }

    print_table(hash_table);

    free(hash_table);
    printf("Freed\n");
}

struct dict_item *create_item(struct hash_table *table, char key){
    struct dict_item *ret = (struct dict_item *)malloc(sizeof(struct dict_item));
    ret->key = key;
    ret->val = simple_hash_func(table, ret->key);
    ret->og_val = ret->val;
    // printf("%d -> %d\n", ret->key, ret->val);
    return ret;
}

int simple_hash_func(struct hash_table *table, char data){
    int result = (int)data;
    result = result % table->size;
    return result;
}


void insert_item(struct hash_table *table, struct dict_item *new_item){
    struct dict_item *temp_dict = table->table[new_item->val];
    if(NULL == temp_dict){
        table->table[new_item->val] = new_item;
        table->remaining--;        
    }
    //spot taken
    else{
        for(int i = 1; i < table->size; i++){
            int index = (i+new_item->og_val)%table->size; //loops with list
            temp_dict = table->table[index];
            if(NULL == temp_dict){
                new_item->val = index;
                table->table[index] = new_item;
                table->remaining--; 
                return; 
            }
        }
    }
}

struct dict_item* find_item(struct hash_table *table, int curr_val, char key, int flag/*first call 1*/){
    struct dict_item *temp_dict = table->table[curr_val];
    int next_val = (curr_val+1) % table->size;
    // printf("Curr: %d, Next: %d\n", curr_val, next_val);
    //in the correct spot, iterated through already
    if(simple_hash_func(table, key) == curr_val && 0 == flag){
        printf("Not Found\n");
        return NULL;
    }
    else if(NULL == temp_dict){
        //first time through
        if(1 == flag){
            printf("Slot empty\n");
            return NULL;
        }
        else{
            printf("Keep Looking\n");
            return find_item(table, next_val, key, 0);
        }
    }
    //found
    else if(key == temp_dict->key){
        printf("Found %c\n", temp_dict->key);
        return temp_dict;
    }
    //not found keep looking in the table
    else{
        printf("This is %c\n", temp_dict->key);
        //increment curr_val (table index) and flag = 0
        return find_item(table, next_val, key, 0);
    }
}

//assuming NULLs don't count. i.e. the 4th element in the table might be at the 8th index of the table
struct dict_item* find_index_item(struct hash_table *table, int elem_num){
    if(elem_num < 1 || elem_num >= table->size){
        perror("invalid element request.");
        return NULL;
    }
    struct dict_item *temp;
    int count = 0;    
    for(int i = 0; i < table->size; i++){
        temp = table->table[i];
        if(NULL != temp){
            count++;
            if(count == elem_num){
                return temp;
            }
        }
    }
    return NULL;
}

void print_table(struct hash_table *table){
    for(int i = 0; i < table->size; i++){
        if(NULL != table->table[i]){
            printf("Table contains '%c' at index %d. %d  hashed from %d.\n", 
                (table->table[i]->key), table->table[i]->val, table->table[i]->og_val, table->table[i]->key);
        }
    }
}

void print_dict_item(struct dict_item *elem){
    if(NULL != elem){
        printf("'%c' value hashed: %d and indexed at %d\n", elem->key, elem->og_val, elem->val);

    }
}

//TODO: handle with malloced data
void remove_dict_item(struct hash_table *table, struct dict_item *item){
    int index = item->val;
    table->table[index] = NULL;
    free(item);
    table->remaining++;
}

struct hash_table *grow_table(struct hash_table *table){
    table->size = table->size * 2; //double size
    table = realloc(table, sizeof(struct dict_item *) * (table->size + 1));
}