#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

// #include "../3-3-2/inc/cd_ll.h"
// #include "../3-3-2/inc/cd_ll_user.h"
// #include "../3-3-2/inc/cd_ll_stack_queue.h"
#include "../3-3-2/src/cd_ll.c"

#define RANGE 20
#define MAX_CHILD 3
#define ALLOW_DUPLICATES 0
#define OUTPUT 0

typedef struct tree_node{
    void *data;
    int num_children;
    int height;
    int depth;

    struct tree_node *left;
    struct tree_node *middle;
    struct tree_node *right;
} tnode;

typedef struct tree{
    int height;
    tnode *root;

    tnode *(*compare)(struct tree *t, tnode *x, tnode *y);
	int (*print)(tnode *n);

} tree_t;

//create a tree with x nodes
void init_tree(tree_t *t, tnode *(*user_compare)(struct tree *t, tnode *x, tnode *y), int (*user_print)(tnode *n));


tnode *create_tnode(void *value);
//values fill tree breadth first
void insert_tnode(tree_t *t, tnode *head, tnode *new_tnode);
void free_tnode(tnode *curr_node);
void free_tree(tree_t *t);
void random_fill(tree_t *t, int num);

tnode *tnode_find(tree_t *t, tnode *node, int behavior /*0 parent-next-null, 1 tnode-parent*/, int *return_val);
int is_parent(tree_t *t, tnode *parent, tnode *target_child);
void print_tnode_int(tnode *curr_node);
tnode *remove_tnode(tree_t *t, tnode *node);
//TODO: test balence
tree_t *balence(tree_t *t);
tnode *merge_two(tree_t *t, tnode *node_a, tnode *node_b);
tnode *merge(tree_t *t, tnode *node_a, tnode *node_b, tnode *node_c);
void print_tnode_tree(tree_t *t, tnode *curr);
int comp_int(void *val_a, void *val_b);

void update_tree(tree_t *t);
int update_children(tnode *node);
int update_height(tree_t * t, tnode *node);
int update_depth(tree_t *t, tnode *node);
int ret_tnodes_int(tnode *curr_node);
//compare x & y. return 'greater' node.
tnode *compare_tnodes_int(struct tree *t, tnode *x, tnode *y);

/*CD_LL functions, NODE_T*/

//compare x & y. return 'greater' node.
node_t *compare_node_t_int(node_t *x, node_t *y);
//return int data type
int cd_ll_print_tnode_int(node_t *n);
//test for adding 1 to each node in list
void tnode_func(node_t **n);


int main(){
    srand(time(NULL)); //makes it random
    tree_t *tree = (tree_t *)malloc(sizeof(tree_t));

    init_tree(tree, &compare_tnodes_int, &ret_tnodes_int);

    // int *value = malloc(sizeof(int));
    // // *value = (rand() % (RANGE));
    // *value = 5;
    // tnode *head = create_tnode(value);
    // insert_tnode(tree, tree->root, head);
    
    // int *value1 = malloc(sizeof(int));
    // // *value = (rand() % (RANGE));
    // *value1 = 1;
    // tnode *one = create_tnode(value1);
    // insert_tnode(tree, tree->root, one);

    // int *value2 = malloc(sizeof(int));
    // // *value = (rand() % (RANGE));
    // *value2 = 2;
    // tnode *two = create_tnode(value2);
    // insert_tnode(tree, tree->root, two);

    random_fill(tree, 18);

    // print_tnode_int(head);

    // int five = 5;
    // void *num1 = &five;

    printf("\n*****Print Tree*****\n");
    print_tnode_tree(tree, tree->root);
    printf("\n");

    tnode *temp = remove_tnode(tree, tree->root);
    free_tnode(temp);

    printf("\n*****Print Tree*****\n");
    print_tnode_tree(tree, tree->root);
    printf("\n");

    // print_tnode_int(head);
    // print_tnode_tree_int(head, 1);

    // free_tnode(head);
    free_tree(tree);
}

void init_tree(tree_t *t, tnode *(*user_compare)(tree_t *t, tnode *x, tnode *y), int (*user_print)(tnode *n)){
    t->root = NULL;
	t->height = 0;

	//assign function pointers their respective addresses
	t->compare = user_compare;
	t->print = user_print;
}


tnode *create_tnode(void *value){
    tnode *curr = (tnode *)malloc(sizeof(tnode));
    curr->data = value;
    curr->depth = 0;
    curr->num_children = 0;
    curr->height = 0;

    curr->left = NULL;
    curr->middle = NULL;
    curr->right = NULL;

    return curr;
}

//Inserts after parent
void insert_tnode(tree_t *t, tnode *parent, tnode *new_tnode){
    if(NULL == t || NULL == t->root){
        t->root = new_tnode;
        t->root->num_children = update_children(t->root);
    }
    else if(NULL == parent){
        if(OUTPUT){
            printf("ERROR: Inserting with NULL parent\n");
        }
    }
    else{
        int ret;
        tnode * curr = tnode_find(t, parent, 0, &ret); //next null

        if(NULL == curr->left){
            curr->left = new_tnode;
        }
        else if(NULL == curr->middle){
            curr->middle = new_tnode;
        }
        else if(NULL == curr->right){
            curr->right = new_tnode;
        }
        else{
            free_tnode(new_tnode);
            if(OUTPUT){
                printf("ERROR: Next empty failed for insert\n");
            }
            return;
        }
        new_tnode->depth = curr->depth + 1;
        curr->num_children = update_children(curr);

        //if this is a new generation update the tree
        if(new_tnode->depth > t->height){
            t->height = new_tnode->depth;
        }

        curr->height = t->height - curr->depth;
    }
}

void free_tnode(tnode *curr_node){
    if(NULL == curr_node){
        if(OUTPUT){
            printf("Trying to free NULL value\n");
        }
        return;
    }
    if(NULL != curr_node->data && OUTPUT){
        printf("Freeing: %d\n", *(int*)curr_node->data);
    }
    else{
       if(OUTPUT){
            printf("Freeing node\n");
       }
    }

    // if(NULL != curr_node->left){
    //     free_tnode(curr_node->left);
    // }
    // if(NULL != curr_node->middle){
    //     free_tnode(curr_node->middle);
    // }
    // if(NULL != curr_node->right){
    //     free_tnode(curr_node->right);
    // }

    free(curr_node->data);

    curr_node->data = NULL;
    curr_node->right = NULL;
    curr_node->middle = NULL;
    curr_node->left = NULL;

    free(curr_node);
    

    curr_node = NULL;
}

void free_tree(tree_t *t){
    while(NULL != t->root){
        tnode *temp = remove_tnode(t, t->root);
        free_tnode(temp);
        printf("********************************\n");
        print_tnode_tree(t, t->root);
    }
    free(t);
    t = NULL;
}


void random_fill(tree_t *t, int num){
    int count = 0;
    printf("NUM: %d\n",num);
    while(count < num){
        if(OUTPUT){
            printf("****************COUNT**************** %d\n", count);
        }

        //random number
        int *value = malloc(sizeof(int));
        *value = (rand() % (RANGE));

        //creating a node using the random number
        // void *node_id =  malloc(sizeof(int));
        // node_id = value;
        
        if(OUTPUT){
            printf("value: %d\n", *(int*)value);
        }
        tnode *new_tnode = create_tnode(value);

        //checks if the number is already being used
        int ret;
        tnode *check = tnode_find(t, new_tnode, 1, &ret);
        if((NULL == check && 0 == ret) || ALLOW_DUPLICATES){
            if(OUTPUT){
                printf("insert\n");
            }

            insert_tnode(t, t->root, new_tnode);
            count++;
            if(OUTPUT){
                print_tnode_tree(t, t->root);
            }
        }
        else{
            if(OUTPUT){
                printf("Duplicate\n");
            }
            free_tnode(new_tnode);
        }
    }
}


tnode *tnode_find(tree_t *t, tnode *node, int behavior /*0 parent-next-null, 1 tnode-parent*/, int *return_val){
    if(NULL == t){
        *return_val = -1;
        return NULL;
    }
    else{
        cd_ll *q = (cd_ll *)malloc(sizeof(cd_ll));
        init(q, &compare_nodes_int, &print_int, &tnode_func);
        node_t *curr; 
        if(0 == behavior){
            curr = create_node(node);
        }
        else{
            curr = create_node(t->root);
        }
       
        push_back(curr, q);

        while(!is_empty(q)){
            node_t *temp = pop(q);
            if(NULL != temp->data){
                tnode *temp_tnode = temp->data;
                
                //find null (0)
                if(0 == behavior){
                    if(NULL == temp_tnode->left || NULL == temp_tnode->middle || NULL == temp_tnode->right){
                        free_cd_ll(q);
                        free(temp);
                        *return_val = 1;
                        return temp_tnode;
                    }
                }
                //find parent or exists (1)
                else if(1 == behavior){
                    if(NULL == compare_tnodes_int(t, node, temp_tnode)){
                        free_cd_ll(q);
                        *return_val = 1;
                        return temp_tnode;
                    }
                    int is_parent_result = is_parent(t, temp_tnode, node);
                    if(1 == is_parent_result){
                        free_cd_ll(q);
                        *return_val = 1;
                        return temp_tnode;
                    }
                }
                else {
                    if(OUTPUT){
                        printf("invalid behavior in tnode_find\n");
                    }
                    *return_val = -1;
                    return NULL; 
                }

                push_back(create_node(temp_tnode->left), q);
                push_back(create_node(temp_tnode->middle), q);
                push_back(create_node(temp_tnode->right), q);
            }  
            free_node(temp);
        }
        //not found
        free_cd_ll(q);
        *return_val = 0;
        return NULL;
    }
}

int is_parent(tree_t *t, tnode *parent, tnode *target_child){
    if(NULL == t || NULL == parent || NULL == target_child){
        return -1;
    }
    else if(NULL != parent->left && NULL == compare_tnodes_int(t, target_child, parent->left)){
        return 1;
    } 
    else if(NULL != parent->middle && NULL == compare_tnodes_int(t, target_child, parent->middle)){
        return 1;
    } 
    else if(NULL != parent->right && NULL == compare_tnodes_int(t, target_child, parent->right)){
        return 1;
    }
    else{
        return 0;
    }
}

void print_tnode_int(tnode *curr_node){
    if(NULL == curr_node || NULL == curr_node->data){
        printf("Node: NULL\n");
    }
    else if(curr_node->data){
            printf("Node: %d\n", *(int*) curr_node->data);
            curr_node->left && curr_node->left->data  ? printf("Left Child: %d\n",  *(int*) curr_node->left->data) : printf(" ");
            curr_node->middle && curr_node->middle->data  ? printf("Middle Child: %d\n",  *(int*) curr_node->middle->data) : printf(" ");
            curr_node->right && curr_node->right->data  ? printf("Right Child: %d\n",  *(int*) curr_node->right->data) : printf(" ");
            printf("\n");
        }
    else{
        printf("Node: ERROR\n");
    }
}

void print_tnode_tree(tree_t *t, tnode *curr){
    if(NULL == t || NULL == t->root){
        printf("Empty Tree\n");
    }
    else if(NULL != curr && NULL != curr->data){
        print_tnode_int(curr);
        print_tnode_tree(t, curr->left);
        print_tnode_tree(t, curr->middle);
        print_tnode_tree(t, curr->right);
    }
}

//balance tree
tnode *remove_tnode(tree_t *t, tnode *node){
    if(NULL == t){
        return NULL;
    }
    else if(NULL == node){
        return NULL;
    }
    else if(t->root == node){
        tnode *temp_left = t->root->left;
        tnode *temp_middle = t->root->middle;
        tnode *temp_right = t->root->right;
        t->root = temp_left;
        t->root = merge(t, temp_left, temp_middle, temp_right);\
    }
    else{
        int ret;
        tnode *temp = tnode_find(t, node, 1, &ret); //find parent
        if(NULL != temp && node != temp){
            //end of the line
            if(0 != node->num_children){
                if(temp->left == node){
                    temp->left = merge(t, node->left, node->middle, node->right);
                }
                else if(temp->middle == node){
                    temp->middle = merge(t, node->left, node->middle, node->right);
                }
                else if(temp->right == node){
                    temp->right = merge(t, node->left, node->middle, node->right);
                }
                else{
                    printf("Error with tnode_find in remove_tnode: node not a child\n");
                }
            }
        }
        else{
            printf("Error with tnode_find in remove_tnode\n");
        }
        update_tree(t);
        node->left = NULL;
        node->middle = NULL;
        node->right = NULL;
    }

    return node;
}


tree_t *balance(tree_t *t){
    if(NULL == t || NULL == t->root){
        return NULL;
    }
    cd_ll *q = (cd_ll *)malloc(sizeof(cd_ll));
    tnode *next_empty_slot_parent = NULL; //need to track the parent of the next null
    int null_flag = 0; //turns true once a null is hit
    int flag = 0; //turns true if it finds a non-null after the null_flag turns true
    tnode *last_val = NULL;
    init(q, &compare_nodes_int, &print_int, &tnode_func);

    node_t *curr = create_node(&t->root);
    push_back(curr, q);

    while(!is_empty(q)){
        node_t *temp = pop(q);
        if(NULL != temp && NULL != temp->data){
            tnode *temp_tnode = temp->data;
            last_val = temp_tnode;
            if(null_flag){
                flag = 1;
            }

            if(NULL == next_empty_slot_parent){
                if(NULL == temp_tnode->left || NULL == temp_tnode->middle || NULL == temp_tnode->right){
                    next_empty_slot_parent = temp_tnode;
                } 
            }
            
            push_back(create_node(&temp_tnode->left), q);
            push_back(create_node(&temp_tnode->middle), q);
            push_back(create_node(&temp_tnode->right), q);
            free_node(temp);
        }  
        else if(NULL != temp && NULL == temp->data){
            null_flag = 1;
        }
        free_node(temp);
    }
    if(NULL != next_empty_slot_parent && NULL != last_val){
        if(0 == flag){
            free_cd_ll(q);
            return t;
        }
        t = balance(t); //try again
    }
    free_cd_ll(q);
    return NULL;
}


tnode *merge_two(tree_t *t, tnode *node_a, tnode *node_b){
    if(NULL == node_a){
        return node_b;
    }
    else if(NULL == node_b){
        return node_a;
    }
    else{
        if(node_a->num_children <= node_b->num_children){
            insert_tnode(t, node_b, node_a);
            return node_b;
        }
        if(node_a->num_children > node_b->num_children){
            insert_tnode(t, node_a, node_b);
            return node_a;
        }   
    }
}

tnode *merge(tree_t *t, tnode *node_a, tnode *node_b, tnode *node_c){
    tnode *temp = merge_two(t, node_a, node_b);
    return merge_two(t, temp, node_c);
}

int comp_int(void *val_a, void *val_b){
    if(NULL != val_a && NULL != val_b){
        if(*(int*) val_a > *(int*) val_b){
            return 1;
        }
        else if(*(int*) val_a < *(int*) val_b){
            return 0;
        }
        //equal each other
        else{
            return 2;
        }
    }
    return -1;
}

void update_tree(tree_t *t){
    update_children(t->root);
    update_height(t, t->root);
    update_depth(t, t->root);
}

int update_children(tnode *node){
    if(NULL == node){
        return 0;
    }
    node->num_children = 0;
    if(NULL != node->left){
        node->num_children = node->num_children + update_children(node->left) + 1;
    }
    if(NULL != node->middle){
        node->num_children = node->num_children + update_children(node->middle) + 1;
    }
    if(NULL != node->right){
        node->num_children = node->num_children + update_children(node->right) + 1;
    }
    return node->num_children;
}

int update_height(tree_t * t, tnode *node){
    if(NULL == node){
        return 1;
    }
    node->height = 0;
    if(NULL != node->left){
        node->height = node->height + update_height(t, node->left);
    }
    if(NULL != node->middle){
        node->height = node->height + update_height(t, node->middle);
    }
    if(NULL != node->right){
        node->height = node->height + update_height(t, node->right);
    }
    t->height = t->root->height;
    return node->height;
}

int update_depth(tree_t *t, tnode *node){
    if(NULL == node){
        return -1;
    }
    node->depth = t->height;
    if(NULL != node->left){
        node->depth = node->depth - update_depth(t, node->left);
    }
    if(NULL != node->middle){
        node->depth = node->depth - update_depth(t, node->middle);
    }
    if(NULL != node->right){
        node->depth = node->depth - update_depth(t, node->right);
    }
    return node->depth;
}


int ret_tnodes_int(tnode *curr_node){
    if(NULL != curr_node->data){
        return *(int*) curr_node->data;
    }
    else{
        return __INT_MAX__;
    }
}

//compare x & y. return 'greater' node.
tnode *compare_tnodes_int(struct tree *t, tnode *x, tnode *y){
    if(NULL == t || NULL == t->root || (NULL == x && NULL == y)){
        return NULL;
    }
    else if(NULL == x){
        return y;
    }
    else if(NULL == y){
        return x;
    }
    else if(*(int*) x->data > *(int*) y->data){
		return x;
	}
	else if(*(int*) y->data > *(int*) x->data){
		return y;
	}
	else{
		return NULL;
	}
}

//compare x & y. return 'greater' node.
node_t *compare_node_t_int(node_t *x, node_t *y){
    tnode *x_tnode = x->data;
    tnode *y_tnode = y->data;
    if(NULL == x_tnode && NULL == y_tnode){
		return NULL;
    }
    else if(NULL == x_tnode){
        return y;
    }
    else if(NULL == y_tnode){
        return x;
    }
	else if(*(int*) x_tnode->data > *(int*) y_tnode->data){
        return x;
    }
    else if(*(int*) y_tnode->data > *(int*) x_tnode->data){
        return y;
    }
    return NULL;
}

//return int data type
int cd_ll_node_int(node_t *n){
	return (NULL == n->data) ? -1 : *(int*) n->data;
}

//test for adding 1 to each node in list
void tnode_func(node_t **n){
	node_t *curr_node = *n;
	
	int *new_data = curr_node->data;
	*new_data = *new_data + 1;
}