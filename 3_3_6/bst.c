#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BST 1
#define RANGE 50

#define INORDER -1
#define PREORDER 0
#define POSTORDER 1

typedef struct tree_node{
    void * data;

    struct tree_node *left;
    struct tree_node *right;
} tnode;


tnode *create_tnode(void *value);
void insert_tnode(tnode *head, void *value);
void free_tnode(tnode *curr_node, void *data);
void random_fill(tnode *head, int num);

tnode *find_tnode(tnode *head, void *data);
void print_tnode_int(tnode *curr_node, void *data);
tnode *find_max(tnode *head);
tnode *remove_tnode(tnode *head, void* data);
int comp_int(void *val_a, void *val_b);
void traverse(tnode *head, void *data, int x/*-1: inorder, 0: preorder, 1:postorder*/, void (*function)(tnode *node, void *data));
void empty_tree(tnode *head);


int main(){
    srand(time(NULL)); //makes it random

    int *value = malloc(sizeof(int));
    *value = (rand() % (RANGE));

    // printf("%lu", sizeof(tnode *));
    tnode *head = create_tnode(value);

    random_fill(head, 5);

    traverse(head, NULL, INORDER, NULL);
    printf("\n");
    
    print_tnode_int(head, NULL);
    traverse(head, NULL, INORDER, &print_tnode_int);
    printf("\n");
    
    remove_tnode(head, head->data);

    traverse(head, NULL, INORDER, NULL);
    printf("\n");
    traverse(head, NULL, INORDER, &print_tnode_int);
    printf("\n");

    empty_tree(head);
}



tnode *create_tnode(void *value){
    tnode *curr = (tnode *)malloc(sizeof(tnode *));
    curr->data = value;
    curr->left = NULL;
    curr->right = NULL;

    return curr;
}

void insert_tnode(tnode *head, void *value){
    if(NULL == head->data){
        head->data = value;
    }
    //find which side to insert it on
    else{
        int ret = comp_int(head->data, value);
        //new value is less (left)
        if(1 == ret){
            //create a new node if there isn't one already
            if(head->left == NULL){
                head->left = create_tnode(value);
            }
            else{
                insert_tnode(head->left, value);
            }
        }    
        //new value is more (right)
        else if(0 == ret){
            //create a new node if there isn't one already
            if(head->right == NULL){
                head->right = create_tnode(value);
            }
            else{
                insert_tnode(head->right, value);
            }
        }
    }
}

void free_tnode(tnode *curr_node, void *data){
    // printf("Freed: %d\n", *(int*)curr_node->data);
    free(curr_node);
    // curr_node->data = NULL;
    // curr_node->right = NULL;
    // curr_node->left = NULL;

    curr_node = NULL;
}

void random_fill(tnode *head, int num){
    for(int i = 0; i < num; i++){
        // srand(time(NULL)); //makes it random
        int *value = malloc(sizeof(int));
        *value = (rand() % (RANGE));

        void *node_id =  malloc(sizeof(int));
        node_id = value;

        
        //checks if the number is already being used
        if(NULL == find_tnode(head, node_id)){
            insert_tnode(head, node_id);
        }
        //Did not use node_id since it was a repeat.
        else{
            i--;
            free(value);
        }
    }

}

tnode *find_tnode(tnode *head, void *data){
    int ret;

    if(NULL != data){
        //first node
        if(NULL != head){
            ret = comp_int(head->data, data);

            //they equal each other
            if(2 == ret){
                return head;
            }

            //head->data > data
            else if(1 == ret){
                if(head->left){
                    return find_tnode(head->left, data);
                }
            }

            //data > head->data
            else if(0 == ret){
                if(head->right){
                    return find_tnode(head->right, data);
                }
            }
        }
    }
    return NULL;
}


void print_tnode_int(tnode *curr_node, void *data){
    if(NULL == curr_node){
        printf("NULL\n");
    }
    else{
        if(curr_node->data){
            printf("Node: %d\n", *(int*) curr_node->data);
            printf("Left Child: ");
            curr_node->left && curr_node->left->data  ? printf("%d\n",  *(int*) curr_node->left->data) : printf("NULL\n");
            printf("Right Child: ");
            curr_node->right && curr_node->right->data ? printf("%d\n\n", *(int*) curr_node->right->data) : printf("NULL\n\n");
        }
        else{
            printf("Node: NULL\n");
        }
    }
}

tnode *find_max(tnode *head){
    if(NULL == head){
        return NULL;
    }
    else if(NULL != head->right){
        return find_max(head->right);
    }
    return head;
}

tnode *remove_tnode(tnode *head, void* data){
        //find node with data
        if(NULL == head){
            return NULL;
        }
        //head->data > data
        if(1 == comp_int(head->data, data)){
            head->left = remove_tnode(head->left, data);
        }
        //head->data < data
        else if(0 == comp_int(head->data, data)){
            head->right = remove_tnode(head->right, data);
        }
        //found it! 
        else{
            //No children
            if(NULL == head->left && NULL == head->right){
                free(head);
                return NULL;
            }
            //one child
            else if(NULL == head->left || NULL == head->right){
                tnode *temp;
                if(NULL == head->left){
                    temp = head->right;
                }
                else{
                    temp = head->left;
                }
                free(head);
            }
            //both children
            else{
                tnode *temp = find_max(head->left);
                head->data = temp->data;
                head->left = remove_tnode(head->left, temp->data);
            }
        }
        return head;
}

int comp_int(void *val_a, void *val_b){
    if(NULL != val_a && NULL != val_b){
        if(*(int*) val_a > *(int*) val_b){
            return 1;
        }
        else if(*(int*) val_a < *(int*) val_b){
            return 0;
        }
        //equal each other
        else{
            return 2;
        }
    }
    return -1;
}

void traverse(tnode *head, void *data, int x/*-1: inorder, 0: preorder, 1:postorder*/, void (*function)(tnode *node, void *data)){
    if(NULL != head){
        if(INORDER == x){
            if(head->left){
                traverse(head->left, data, x, function);
            }
            if(head->data){
                if(NULL != function){
                    function(head, NULL);
                }
                else {
                    printf("- %d ", *(int *) head->data);
                }
            }
            if(head->right){
                traverse(head->right, data, x, function);
            }
        }
        if(PREORDER == x){
            if(head->data){
                if(NULL != function){
                    function(head, data);
                }
                else {
                    printf("- %d ", *(int *) head->data);
                }
            }
            if(head->left){
                traverse(head->left, data, x, function);
            }
            if(head->right){
                traverse(head->right, data, x, function);
            }
        }
        if(POSTORDER == x){
            if(head->left){
                traverse(head->left, data, x, function);
            }
            if(head->right){
                traverse(head->right, data, x, function);
            }
            if(head->data){
                if(NULL != function){
                    function(head, data);
                }
                else {
                    printf("- %d ", *(int *) head->data);
                }
            }
        }
    }
}

void empty_tree(tnode *head){
    traverse(head, NULL, POSTORDER, &free_tnode);
    printf("Tree Empty\n");
}