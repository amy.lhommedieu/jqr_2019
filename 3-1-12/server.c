#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define PORT "5556"

int main(int argc, char *argv[]){
    int status;
    int s; //socket-related

    //creating the structs that hold address information
    struct addrinfo hints;
    struct addrinfo *servinfo; //points to results

    //ensuring hints is zero'ed and initialize how we want
    memset(&hints, 0, sizeof hints); 
    hints.ai_family = AF_UNSPEC;  
    hints.ai_socktype = SOCK_STREAM; //TCP
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me    

    //error checking for pointer to linked-list (servinfo) of struct addrinfos & their structs sockaddr
    if((status = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return 1;
    }
    
    //get the socket descriptor
    s = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol /*0*/);

    //error checking for bind (also binds servinfo to the port passed into getaddrinfo())
    if(bind(s, servinfo->ai_addr, servinfo->ai_addrlen) == -1){
        fprintf(stderr, "bind error\n");
        return 1;
    }

    //sendto()
    //recvfrom()
    //gethostname
    //sethostname
    //getsockopt()
    //setsockopt()

    //getaddrinfo()
    //struct sockaddr{}
    //struct sockaddr_in{}
    // struct sockaddr_un{}

    //prevents "Address already in use" error message
    int yes=1;
    if (setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof yes) == -1) {
        perror("setsockopt");
        return 1;
    }

    //Listen for incoming connections
    if(listen(s, 10)){
        fprintf(stderr, "listen error\n");
        return 1;
    }

    //Accept incoming connections, returns a new sock_fd
    struct sockaddr_storage other_addr;
    socklen_t addr_size;
    int new_s;

    addr_size = sizeof(other_addr);
    new_s = accept(s, (struct sockaddr *)&other_addr, &addr_size);

    //send 10 bytes
    char msg_sent[255] = "Congrats!\0";
    char msg_recv[255];
    int len, bytes_sent, bytes_recv;

    len = strlen(msg_sent);
    bytes_sent = send(new_s, msg_sent, 255, 0);

    if(bytes_sent == -1){
        fprintf(stderr, "send error\n");
        return 1;
    }

    printf("Bytes Sent: %d\n", bytes_sent);

    //recieve 10 bytes
    bytes_recv = recv(new_s, msg_recv, 255, 0);

    if(bytes_recv == -1){
        fprintf(stderr, "recv error\n");
    }
    else if(bytes_recv == 0){
        printf("Connection Closed.\n");
    }
    else{
        printf("%s\n", msg_recv);
    }

    //free the linked-list
    freeaddrinfo(servinfo); 

    return 0;
}


