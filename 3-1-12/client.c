#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>

#define PORT "5556"

int main(int argc, char *argv[]){
    int status;
    int s; //socket-related

    //creating the structs that hold address information
    struct addrinfo hints;
    struct addrinfo *servinfo; //points to results

    //ensuring hints is zero'ed and initialize how we want
    memset(&hints, 0, sizeof hints); 
    hints.ai_family = AF_UNSPEC;  
    hints.ai_socktype = SOCK_STREAM; //TCP
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me 

    //error checking for pointer to linked-list (servinfo) of struct addrinfos & their structs sockaddr
    if((status = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return 1;
    }
    
    //get the socket descriptor
    s = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol /*0*/);
    
    //connect
    if(connect(s, servinfo->ai_addr, servinfo->ai_addrlen) == -1){
        fprintf(stderr, "connect error\n");
        return 1;
    }
    
    freeaddrinfo(servinfo);    

    //send/recieve
    char msg_recv[255];
    int bytes_recv;

    //recieve 10 bytes
    bytes_recv = recv(s, msg_recv, 255, 0);

    if(bytes_recv == -1){
        fprintf(stderr, "recv error\n");
    }
    else if(bytes_recv == 0){
        printf("Connection Closed.\n");
    }
    else{
        printf("%s\n", msg_recv);
    }

    //send 10 bytes
    char msg_sent[255] = "I am here\0";
    int /*len,*/ bytes_sent;

    // len = strlen(msg_sent);
    bytes_sent = send(s, msg_sent, 255, 0);

    if(bytes_sent == -1){
        fprintf(stderr, "send error");
        return 1;
    }

    printf("Bytes Sent: %d\n", bytes_sent);

    //free the linked-list
    freeaddrinfo(servinfo); 

    return 0;
}


