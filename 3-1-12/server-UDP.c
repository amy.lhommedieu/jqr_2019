#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h> 

#define PORT "5556"

int main(int argc, char *argv[]){
    int status;
    int s; //socket file descriptor

    //creating the structs that hold address information
    struct addrinfo hints;
    struct addrinfo *servinfo; //points to results
    
    struct sockaddr_storage other_addr;
    socklen_t addr_size;

    char addr_string[INET6_ADDRSTRLEN];
    char msg_recv[255];
    int int_recv;
    char ip_addr[46]; //allows for ipv6 or v4
    int16_t bytes_recv;

    //ensuring hints is zero'ed and initialize how we want
    memset(&hints, 0, sizeof hints); 
    hints.ai_family = AF_UNSPEC;  
    hints.ai_socktype = SOCK_DGRAM; //UDP
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me    

    memset(&servinfo, 0, sizeof servinfo); 

    //error checking for pointer to linked-list (servinfo) of struct addrinfos & their structs sockaddr
    if((status = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return 1;
    }
    
    //get the socket descriptor
    s = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol /*0*/);


    //prevents "Address already in use" error message
    int yes=1;
    if (setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof yes) == -1) {
        perror("setsockopt");
        return 1;
    }

    //error checking for bind (also binds servinfo to the port passed into getaddrinfo())
    if(bind(s, servinfo->ai_addr, servinfo->ai_addrlen) == -1){
        fprintf(stderr, "bind error\n");
        return 1;
    }

    freeaddrinfo(servinfo);

    addr_size = sizeof(other_addr);
    if((bytes_recv = recvfrom(s, msg_recv, 255, 0, (struct sockaddr *)&other_addr, &addr_size)) == -1){
        perror("recvFrom");
        exit(1);
    }

    msg_recv[bytes_recv] = '\0';

    inet_ntop(other_addr.ss_family, &(((struct sockaddr_in*)((struct socddr *)&other_addr))->sin_addr), ip_addr, sizeof(ip_addr));

    printf("listener: %d bytes\n%s\n -%s\n", bytes_recv, msg_recv, ip_addr);

    if((bytes_recv = recvfrom(s, &int_recv, 255, 0, (struct sockaddr *)&other_addr, &addr_size)) == -1){
            perror("recvFrom");
            exit(1);
        }

    printf("listener: %d bytes\nNetwork order: %d\nHost order: %d\n", bytes_recv, ntohs(int_recv), int_recv);

    //gethostname
    char hostbuf[256];
    if(gethostname(hostbuf, sizeof hostbuf) == -1){
        perror("gethostname");
        exit(1);
    }

    printf("Hostname: %s\n", hostbuf);

    //sethostname (and resetting it)
    sethostname("DumpsterFire", 12);

    if(gethostname(hostbuf, sizeof hostbuf) == -1){
        perror("gethostname");
        exit(1);
    }

    printf("Hostname: %s\n", hostbuf);

    sethostname("dumpster-fire", 13);
    
    //getsockopt()
    int option_val;
    int option_len = sizeof(int);
    if(getsockopt(s, SOL_SOCKET, SO_TYPE, (char *) &option_val, &option_len)==0){
        if(option_val == 2){
            printf("DGRAM\n");
        }
        else if(option_val == 1){
            printf("STREAM\n");
        }
    }

    //setsockopt()
    //look at line 51

    //getaddrinfo()
    //look at line 40    

    //short - 16 bit
    //long - 32 bit

    //htons() Host to Network Short
    //htonl() Host to Network Long
    //ntohs() Network to Host Short
    //ntohl() Network to Host Long

    //Used the same way as sockaddr_in. bind/accept/listen/etc require struct sockaddr
    //The sockaddr has a data section that holds the information depeding if it is 
    //_un or _in, and then it will know how to decipher the bits once it is cast.
    //getaddrinfo is a newer way to fill the sockaddr that is IPv4/6 irrelevant.

    // struct sockaddr {
    //     unsigned short   sa_family; //Address family
    //     char             sa_data[14]; //14 bytes of protocol specific address
    // };

    // struct sockaddr_in {
    //     short int            sin_family; //Address family
    //     unsigned short int   sin_port; //16-bit port number in Network Byte Order.
    //     struct in_addr       sin_addr; //32-bitIP Address in Network Byte Order.
    //     unsigned char        sin_zero[8]; //NULL
    // };

    // struct sockaddr_in{
    //     sa_family_t  sun_family;  //Address family
    //     char         sun_path[];  //Socket pathname
    // }

    close(s);

    return 0;
}


