#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

// struct sockaddr_un {
//     unsigned short sun_family;  /* AF_UNIX */
//     char sun_path[108];
// }

int echo_behavior(unsigned int sock_desc);
int input_behavior(unsigned int sock_desc, char *msg);


int main(){
    //this socket descriptor
    unsigned int s;
    //client socket descriptor
    unsigned int s2;
    //length of the path and the unix family
    int len;

    //using sockaddr_un because this is a unix socket
    struct sockaddr_un local;
    struct sockaddr_un remote;

    //getting socket descriptor
    s = socket(AF_UNIX, SOCK_STREAM, 0);

    //error checking for socket call
    if(s == -1){
        fprintf(stderr, "socket descriptor error\n");
        return 1;
    }

    //bind the socket to the address in the Unix domain
    local.sun_family = AF_UNIX; //local declared above
    //touch unix_socket prior to create the file (maybe not necessary)
    strncpy(local.sun_path, "/home/amy/repos/jqr_2019/3-1-12/unix_socket", sizeof(local.sun_path) - 1);

    //removes the socket if it already exists
    unlink(local.sun_path);
    len = strlen(local.sun_path) + sizeof(local.sun_family);
    
    //error checking for bind (also binds the socket descriptor to the address given)
    if(bind(s, (struct sockaddr *)&local, len) == -1){
        fprintf(stderr, "bind error\n");
        return 1;
    }

    //up to 10 connections can be queued before turned away
    if(listen(s, 10) == -1){
        perror("listen error");
        exit(1);
    }

    int loop = 1; //continuously loop waiting for connections
    char msg_recv[20];
    
    while(loop){
        printf("...\n");
        //accepts the connection from a client and returns a new socket descriptor
        len = sizeof(struct sockaddr_un);
        s2 = accept(s, (struct sockaddr *)&remote, &len); //remote now holds the client's information
        
        if(s2 == -1){
            perror("accept error");
            exit(1);
        }

        printf("Connected\n");

        loop = echo_behavior(s2);

        close(s2);
    }
    return 0;
}

//if it receives anything it will send it right back
int echo_behavior(unsigned int sock_desc){
    char str[20];
    int num_recv = recv(sock_desc, str, 20, 0);

    if(num_recv < 0){
        perror("recv error");
    }

    else if(num_recv > 0){
        if (send(sock_desc, str, num_recv, 0) < 0) {
            perror("send error");
        }
        else{
            for(int i = 0; i < 20; i++){
                if('\n' == str[i]){
                    str[i] = '\0';
                }
            }
            printf("%s\n", str);
            if (!strncmp(str, "exit", 20) || !strncmp(str, "EXIT", 20)) {
                printf("exiting...\n");
                return 0;
            }
            if(0 == echo_behavior(sock_desc)){
                return 0;
            }
        }
    }
    return 1;
}
    