#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h> 

#define PORT "5556"

int main(int argc, char *argv[]){
    int status;
    int s; //socket file descriptor

    //creating the structs that hold address information
    struct addrinfo hints;
    struct addrinfo *servinfo; //points to results
    
    struct sockaddr_storage other_addr;
    socklen_t addr_size;

    char addr_string[INET6_ADDRSTRLEN];
    char msg_sent[255] = "I am here";
    int int_sent = 10;
    int16_t bytes_sent;

    //ensuring hints is zero'ed and initialize how we want
    memset(&hints, 0, sizeof hints); 
    hints.ai_family = AF_UNSPEC;  
    hints.ai_socktype = SOCK_DGRAM; //TCP

    //error checking for pointer to linked-list (servinfo) of struct addrinfos & their structs sockaddr
    if((status = getaddrinfo("127.0.0.1", PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return 1;
    }
    
    //get the socket descriptor
    s = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol /*0*/);

    bytes_sent = sendto(s, msg_sent, 255, 0, servinfo->ai_addr, servinfo->ai_addrlen);

    if(bytes_sent == -1){
        fprintf(stderr, "send error");
        return 1;
    }

    printf("Bytes Sent: %d\n", bytes_sent);

    bytes_sent = sendto(s, &int_sent, 255, 0, servinfo->ai_addr, servinfo->ai_addrlen);

    if(bytes_sent == -1){
        fprintf(stderr, "send error");
        return 1;
    }

    printf("Bytes Sent: %d\nMessage Sent: %d (Network) or %d (Host)\n", bytes_sent, htons(int_sent), int_sent);

    //free the linked-list
    freeaddrinfo(servinfo); 

    return 0;
}


