#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

// struct sockaddr_un {
//     unsigned short sun_family;  /* AF_UNIX */
//     char sun_path[108];
// }

int main(int argc, char *argv[]){
    int s;
    int num_recv;
    int len;

    struct sockaddr_un remote;
    char str[100];

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
        exit(1);
    }

    printf("...");

    remote.sun_family = AF_UNIX;
    strncpy(remote.sun_path, "/home/amy/repos/jqr_2019/3-1-12/unix_socket", sizeof(remote.sun_path) - 1);

    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1) {
        perror("connect error");
        exit(1);
    }

    printf("success\n");

    if(1 < argc){

    }

    //dynamic user input
    else{
        while(printf("> "), fgets(str, 100, stdin), !feof(stdin)) {
            if (send(s, str, strlen(str), 0) == -1) {
                perror("send error");
                exit(1);
            }

            if ((num_recv=recv(s, str, 100, 0)) > 0) {
                str[num_recv] = '\0';
                printf("echo> %s", str);
            } else {
                if (num_recv < 0) perror("recv");
                else printf("Server closed connection\n");
                exit(1);
            }
        }        
    }



    close(s);

    return 0;
}

