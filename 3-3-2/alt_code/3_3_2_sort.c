#include <stdlib.h>
#include <stdio.h>

typedef struct node {

   struct node *next;
   struct node *previous;
   int head;
   int tail;
   void *data;

} node_t;  

//create a list with x nodes
void init(int x, node_t **head, node_t **tail);
//print current node
void print_node(node_t *n);
//print list
void print_all(node_t *head);
//add element to head
void prepend_node(void *data, node_t **head, node_t **tail);
//insert element after node_t *n
void insert_node(void *data, node_t **n, node_t **head, node_t **tail);
//add element to end
void append_node(void *data, node_t **head, node_t **tail);
//delete nodes
void delete_node(node_t **n, node_t **head, node_t **tail);
//empty the list
void empty_list(node_t **head, node_t **tail);
//find first match in list
node_t *find_node(void *val, node_t **head);
//insert a node in a specified index i
void insert(void *val, int i);
//sort alphanumerically
node_t *sort(node_t **head, node_t ** tail, node_t * (*f)(node_t **, node_t **));
//sort swap
void sort_swap(node_t * node_a, node_t * node_b);
//compare function from the user: What is the input?
void compare(void *x, void (*f)(void *, void *));
//free function from the user: What is the input?
void free_data(void *x, void (*f)(void *));
//sort function for integers
node_t *sort_int_ltoh(node_t **head, node_t ** tail);

//TESTING Capability

int main(){
	node_t node_list;

	node_t *head = NULL;
	node_t *tail = head;

	int d = 4;
	int f = 7;
	int c = 3;
	int b = 2;
	int a = 1;
	int e = 6;

	prepend_node(&a, &head, &tail);

	prepend_node(&b, &head, &tail);
	prepend_node(&c, &head, &tail);
	prepend_node(&d, &head, &tail);

	insert_node(&e, &tail, &head, &tail);
	insert_node(&f, &head, &head, &tail);

	printf("Original List: \n\n");
	print_all(head);
	printf("\n");

	// sort_swap(find_node(&f, &head), find_node(&c, &head));

	sort(&head, &tail,  sort_int_ltoh);

	printf("New List: \n\n");
	print_all(head);

	// delete_node(&head, &head, &tail);

	// empty_list(&head, &tail);
	// printf("New List: \n");
	// print_all(head);

}

// init(2, &head, &tail);
/*//create a list with x nodes
void init(int x, node_t **head, node_t **tail){
	node_t *temp_head = (node_t*) malloc(sizeof(node_t));
	temp_head->head = 1;
	temp_head->tail = 0;

	node_t *temp_tail = (node_t*) malloc(sizeof(node_t));
	temp_tail->head = 0;
	temp_tail->tail = 1;

	temp_head->next = temp_tail;
	temp_head->previous = temp_tail;

	temp_tail->next = temp_head;
	temp_tail->previous = temp_head;

	for(int i = 0; i < x; i++){
		node_t *newNode = (node_t*) malloc(sizeof(node_t));
		head = newNode;
	}
}*/

//print current node
void print_node(node_t *n){
	if(n == NULL) {
		printf("Empty Node\n");
	}
	else {
		if(n->head == 1){
			printf("Head: ");
		}
		if(n->tail == 1){
			printf("Tail: ");
		}

		printf("Node: (%d)\n", *(int*)n->data);

		if(n->previous == NULL){
			printf("Previous: NULL\n");
		}
		else {
			printf("Previous: (%d)\n", *(int*)n->previous->data);
		}

		if(n->next == NULL){
			printf("Next: NULL\n");
		}
		else {
			printf("Next: (%d)\n", *(int*)n->next->data);
		}
		printf("\n");
	}
}

//print list
void print_all(node_t *head){
	node_t *temp = head;

	if(temp == NULL){
		printf("Empty List\n");
	}
	else{
		do{
			print_node(temp);
			temp = temp->next;
		} while(temp != NULL);
	}
}

//add element to head
void prepend_node(void *data, node_t **head, node_t **tail){
	node_t *newNode = (node_t*) malloc(sizeof(node_t));
	node_t *temp_head = *head;

	newNode->data = data;

	//empty list
	if(*head == NULL && *tail == NULL){
		newNode->next = NULL;
		newNode->previous = NULL;
		newNode->head = 1;
		newNode->tail = 1;
		*head = newNode;
		*tail = newNode;
	}
	
	//1 element in list
	else if(*head == *tail){
		newNode->next = *head;
		newNode->next->head = 0;
		newNode->head = 1;
		temp_head->previous = newNode;
		newNode->previous = NULL;
		//newNode->previous = *tail;
		*head = newNode;
	}

	//n elements in list
	else{
		newNode->next = *head;
		newNode->next->head = 0;
		newNode->head = 1;
		newNode->next->previous = newNode;
		newNode->previous = NULL;
		//newNode->previous = *tail;
		*head = newNode;
	}
}

//insert element after node_t *n
void insert_node(void *data, node_t **n, node_t **head, node_t **tail){
	node_t *newNode = (node_t*) malloc(sizeof(node_t));
	newNode->data = data;

	//empty list
	if(*n == NULL){
		newNode->next = NULL;
		newNode->previous = NULL;
	}
	//n elements in list
	else{
		newNode->previous = *n;
		newNode->next = newNode->previous->next;
		newNode->previous->next = newNode;
		if(*n == *tail){
			node_t *oldTail = *tail;
			oldTail->tail = 0;
			newNode->tail = 1;
			*tail = newNode;
		}
		else{
			newNode->next->previous = newNode;
		}
	}
}

//add element to end
void append_node(void *data, node_t **head, node_t **tail){
	node_t *newNode = (node_t*) malloc(sizeof(node_t));
	node_t *temp_tail = *tail;

	newNode->data = data;

	//empty list
	if(*head == NULL && *tail == NULL){
		newNode->next = NULL;
		newNode->previous = NULL;
		newNode->head = 1;
		newNode->tail = 1;
		*head = newNode;
		*tail = newNode;
	}
	
	//1 element in list
	else if(*head == *tail){
		newNode->previous = *head;
		newNode->previous->tail = 0;
		newNode->tail = 1;
		*tail = newNode;
	}

	//n elements in list
	else{
		newNode->previous = *tail;
		temp_tail->next = newNode; //previous tail's next now linked to newNode
		temp_tail->tail = 0;
		newNode->tail = 1;
		// newNode->next = *head;
		newNode->next = NULL;
		*tail = newNode;
	}
}


//delete head
void delete_node(node_t **n, node_t **head, node_t **tail){
	node_t *temp = *n;

	printf("Delete ");

	print_node(temp);

	//head & more than one element
	if(*n == *head && temp->next != NULL){
		printf("Head\n");
		temp->next->previous = NULL;
		temp->next->head = 1;
		*head = temp->next;
	}

	//tail & more than one element
	else if(*n == *tail && temp->previous != NULL){
		printf("Tail\n");
		temp->previous->next = NULL;
		temp->previous->tail = 1;
		*tail = temp->previous;
	}

	//middle element, & more than one element
	else if(temp->next != NULL && temp->previous != NULL){
		printf("Middle\n");
		temp->next->previous = temp->previous; 
		temp->previous->next = temp->next;
	}

	//single element
	else{
		*head = NULL;
		*tail = *head;
	}
}


//empty the list
void empty_list(node_t **head, node_t **tail){
	node_t *temp = *head;

	if(temp == NULL){
		printf("%s\n", "Empty List\n");
	}
	else{
		do{
			//printf("(%d),", *(int*)temp->data);
			delete_node(&temp, head, tail);
			temp = temp->next;
		} while(temp != NULL);
	}
}


//find first match in list
node_t *find_node(void *val, node_t **head){
	node_t *temp = *head;

	while(temp != NULL){
		if(temp->data == val) return temp;
		else temp = temp->next;
	}

	printf("Not Found/");
	return NULL;
}



/*
//insert a node as element i
void insert(void *val, int i){
	node_t* curr = head;
	node_t* previous = (node_t*) malloc(sizeof(node_t));
	node_t* n = (node_t*) malloc(sizeof(node_t));
	n->data = &val;

	if(curr == NULL) prepend_node(&val);
	
	//one step behind curr so that you can successfully link in new node
	else{
		curr = curr->next;
		printf("1st Current: ");
		print_node(curr);

		printf("Inserting...\n");
		for(int x = 0; (x < i && curr->next != NULL); x++){
			printf("\nFor Loop x & Current: %d", x);
			print_node(curr);
			previous = curr;
			curr = curr->next;
			printf("(%d)\n", *(int *) curr->data);
		}
		//at the correct index
		if(curr->next != NULL){
			n->next = curr->next;
			//how to do this...
			previous->next = n;
		}
	}
}
*/
//sort alphanumerically?
node_t *sort(node_t **head, node_t ** tail, node_t *(*f)(node_t **, node_t **)){
	*head = (*f)(head, tail);
	return *head;
}

//sort swap, node_a comes directly before node_b
void sort_swap(node_t * node_a, node_t * node_b){
	//preserving the previous and next nodes
	node_t *tmp_previous = node_a->previous;
	node_t *tmp_next = node_b->next;

	if(node_a->head == 1){
		node_b->head = 1;
		node_a->head = 0;
	}
	if(node_b->tail == 1){
		node_a->tail = 1;
		node_b->tail = 0;
	}

	//placing node_b in node_a's spot with supporting connections
	node_b->next = node_a;
	node_b->previous = tmp_previous;
	if(tmp_previous != NULL){
		tmp_previous->next = node_b;
	}
	
	//placing node_a in node_b's spot with supporting connections
	node_a->next = tmp_next;
	node_a->previous = node_b;
	if(tmp_next != NULL){
		tmp_next->previous = node_a;
	}

}

//compare function from the user: What is the input?
void compare(void *x, void (*f)(void *, void *)){

}

//free function from the user: What is the input?
void free_data(void *x, void (*f)(void *)){

}

node_t *sort_int_ltoh(node_t **head, node_t ** tail){
	node_t *count = *head;
    int swapped = 0;
	int elements = 0;
	int index = 0;

	int big = 0; //biggest element seen
	int small = __INT32_MAX__; //smallest element seen

	while(count->next != NULL){
		count = count->next;
		elements++;
	}
    
    do{
		node_t *curr_node = *head;
        swapped = 0;
		index = 0;

		do{
			//pushes the highest val to the end, and then ignores it.
			if(elements < index){
				curr_node = curr_node->next;
			}
			else{
				// print_node(curr_node);
				if((*(int*)curr_node->data) > (*(int*)curr_node->next->data)){
					// printf("swap\n");
					//assigning the new head
					if(curr_node->head == 1){
						curr_node->head = 0;
						curr_node->next->head = 1;
						*head = curr_node->next;
					}
					//assigning the new tail
					if(curr_node->tail == 1){
						curr_node->tail = 1;
						curr_node->next->tail = 0;
						*tail = curr_node->next;
					}
					sort_swap(curr_node, curr_node->next);
					swapped = 1;
				}
				//sets the biggest element seen
				if(*(int*)curr_node->data > big) {
					big = *(int*)curr_node->data;
				}
				//sets the smallest element seen
				if(*(int*)curr_node->data < small) {
					small = *(int*)curr_node->data;
				}
				if(swapped != 1){
					curr_node = curr_node->next;
				}
			}	
			index++;	

		}while(curr_node->next != NULL);

		// }while((&curr_node->next != head)||(curr_node->next != NULL));
		elements--;  
		// print_all(*head);    

    }while(swapped);

    return *head;

}