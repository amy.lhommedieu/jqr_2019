#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct node {

	struct node *next;
 	struct node *previous;

	int head;
	int tail;

	void *data;

} node_t; 

typedef struct circular_doubly_linked_list {

	struct node *head;
    struct node *tail;

    //return 'greater' node. 
	node_t *(*compare)(node_t *x, node_t *y);
	// void (*free)();
} cd_ll;

//create a list with x nodes
void init(cd_ll **list); //cd_ll
//print current node
void print_node(node_t *n);
//print list
void print_all(node_t *head); //cd_ll
//add element to head
void prepend_node(void *data, node_t **head, node_t **tail); //cd_ll
//insert element after node_t *n
void insert_node(void *data, node_t **n, node_t **head, node_t **tail); //cd_ll
//add element to end
void append_node(void *data, node_t **head, node_t **tail); //cd_ll
//delete nodes
void delete_node(node_t **n, node_t **head, node_t **tail); //cd_ll
//pop the top element
node_t pop_first(); //cd_ll
//peek/print at the top element without removing
node_t peek_first(); //cd_ll
//pop the bottom/last element
node_t pop_last(); //cd_ll
//peek/print at the bottom/last element without removing
node_t peek_last(); //cd_ll
//empty the list
void empty_list(node_t **head, node_t **tail); //cd_ll
//find first match in list 
node_t *find_node(void *val, node_t **head); //cd_ll
//sort alphanumerically
node_t *sort(node_t **head, node_t ** tail); //cd_ll
//sort swap
node_t *sort_swap(node_t * node_a, node_t * node_b, node_t **head, node_t ** tail); //cd_ll
//compare nodes
node_t *compare_nodes(node_t *x, node_t *y);
//compare x & y. return 1 if x is greater, 0 if y and -1 if equal.
int compare_int(void *x, void *y);
//free function from the user: What is the input?
void free_data(void *x, void (*f)(void *));

//TESTING Capability

int main(){
	cd_ll *node_list = (cd_ll*)malloc(sizeof(cd_ll));

    init(&node_list);

	node_t *head = NULL;
	node_t *tail = head;

	int d = 4;
	int f = 7;
	int c = 3;
	int b = 2;
	int a = 1;
	int e = 6;

	prepend_node(&a, &head, &tail);

	prepend_node(&b, &head, &tail);
	prepend_node(&c, &head, &tail);
	// prepend_node(&d, &head, &tail);

	insert_node(&e, &tail, &head, &tail);
	insert_node(&f, &tail, &head, &tail);

	append_node(&d, &head, &tail);
	// append_node(&e, &head, &tail);

	// printf("Original List: \n\n");
	// print_all(head);
	// printf("\n");

	sort(&head, &tail);


	// delete_node(&head, &head, &tail);

	// empty_list(&head, &tail);

	// printf("New List: \n\n");
	print_all(head);
}

// init(2, &head, &tail);
//create a list with x nodes
void init(cd_ll **list){
    cd_ll **list = {NULL, NULL, &compare_int};

	// node_t *temp_head = (node_t*) malloc(sizeof(node_t));
	// temp_head->head = 1;
	// temp_head->tail = 0;

	// node_t *temp_tail = (node_t*) malloc(sizeof(node_t));
	// temp_tail->head = 0;
	// temp_tail->tail = 1;

	// temp_head->next = temp_tail;
	// temp_head->previous = temp_tail;

	// temp_tail->next = temp_head;
	// temp_tail->previous = temp_head;

	// for(int i = 0; i < x; i++){
	// 	node_t *newNode = (node_t*) malloc(sizeof(node_t));
	// 	head = newNode;
	// }
}

//print current node
void print_node(node_t *n){
	char title[10] = "";
	char label_next[10] = "";
	char label_previous[10] = "";
	if(n == NULL) {
		printf("Empty Node\n");
	}
	else {
		//head element
		if(n->head == 1){
			strncpy(title, "Head: ", 9);
			strncpy(label_previous, " (Tail)", 9);
		}
		//tail element
		if(n->tail == 1){
			strncpy(title, "Tail: ", 9);
			strncpy(label_next, " (Head)", 9);
		}
		//element right after head
		if(n->previous->head == 1){
			strncpy(label_previous, " (Head)", 9);
		}
		//element right before tail
		if(n->next->tail == 1){
			strncpy(label_next, " (Tail)", 9);
		}

		printf("%sNode: (%d)\n", title, *(int*)n->data);
		printf("Previous%s: (%d)\n", label_previous, *(int*)n->previous->data);
		printf("Next%s: (%d)\n", label_next, *(int*)n->next->data);
		printf("\n");
	}
}

//print list
void print_all(node_t *head){
	node_t *temp = head;

	if(temp == NULL){
		printf("Empty List\n");
	}
	else{
		do{
			print_node(temp);
			temp = temp->next;
		} while(temp->head != 1);
	}
}

//add element to head
void prepend_node(void *data, node_t **head, node_t **tail){
	node_t *newNode = (node_t*) malloc(sizeof(node_t));
	node_t *temp_head = *head;
	node_t *temp_tail = *tail;

	newNode->data = data;

	//empty list
	if(*head == NULL && *tail == NULL){
		//circular to itself.
		newNode->next = newNode;
		newNode->previous = newNode;

		//set flags
		newNode->head = 1;
		newNode->tail = 1;

		//track head/tail
		*head = newNode;
		*tail = newNode;
	}

	//n elements in list
	else{
		//make circular
		newNode->next = *head;
		temp_head->previous = newNode;
		newNode->previous = *tail;
		temp_tail->next = newNode;

		//set flags
		temp_head->head = 0;
		newNode->head = 1;

		//track head/tail
		*tail = temp_tail;
		*head = newNode;
	}
}

//insert element after node_t *n. Will never be head.
void insert_node(void *data, node_t **n, node_t **head, node_t **tail){
	node_t *newNode = (node_t*) malloc(sizeof(node_t));
	node_t *temp_tail = *tail;
	newNode->data = data;

	//empty list, since list is circular if there are any elements there will not be a NULL 
	if(*n == NULL){
		//circular to itself.
		newNode->next = newNode;
		newNode->previous = newNode;

		//set flags
		newNode->head = 1;
		newNode->tail = 1;

		//track head/tail
		*head = newNode;
		*tail = newNode;
	}
	//n elements in list
	else{
		//linking nodes
		newNode->previous = *n;
		newNode->next = newNode->previous->next;
		newNode->previous->next = newNode;
		newNode->next->previous = newNode;

		if(*n == *tail){
			//set flags and track tail
			temp_tail->tail = 0;
			newNode->tail = 1;
			*tail = newNode;
		}
	}
}

//add element to end
void append_node(void *data, node_t **head, node_t **tail){
	insert_node(data, tail, head, tail);
}


//delete head
void delete_node(node_t **n, node_t **head, node_t **tail){
	node_t *temp = *n;

	printf("Delete ");

	print_node(temp);

	//head & more than one element
	// if(*n == *head && temp->next != NULL){
	if(*n == *head && temp->tail != 1){
		printf("Head\n");
		// temp->next->previous = NULL;
		temp->next->previous = *tail;
		temp->next->head = 1;
		*head = temp->next;
	}

	//tail & more than one element
	// else if(*n == *tail && temp->previous != NULL){
	else if(*n == *tail && temp->head != 1){
		printf("Tail\n");
		// temp->previous->next = NULL;
		temp->previous->tail = 1;
		*tail = temp->previous;
	}

	//middle element, & more than one element
	else if(temp->next != NULL && temp->previous != NULL){
		printf("Middle\n");
		temp->next->previous = temp->previous; 
		temp->previous->next = temp->next;
	}

	//single element
	else{
		*head = NULL;
		*tail = *head;
	}
}


//empty the list
void empty_list(node_t **head, node_t **tail){
	node_t *temp = *head;

	if(temp == NULL){
		printf("%s\n", "Empty List\n");
	}
	else{
		do{
			//printf("(%d),", *(int*)temp->data);
			delete_node(&temp, head, tail);
			temp = temp->next;
		} while(temp != NULL);
	}
}


//find first match in list
node_t *find_node(void *val, node_t **head){
	node_t *temp = *head;

	while(temp != NULL){
		if(temp->data == val) return temp;
		else temp = temp->next;
	}

	printf("Not Found/");
	return NULL;
}


//sort according to user sort function.
node_t *sort(node_t **head, node_t ** tail){
	node_t *curr_node = *head;
	node_t *count = *head;
    int swapped = 0;
	int elements = 0;
	int index = 0;

	//counts up the elements in 
	while(count->tail != 1){
		elements++;
		count = count->next;
	}
    
    do{
        swapped = 0;
		index = 0;

		do{
			//pushes the highest val to the end, and then ignores it.
			if(elements < index){
				curr_node = curr_node->next;
			}
			else{
				// print_node(curr_node);
				// if(curr_node == curr_node->compare(curr_node->next)){
				// 	// printf("swap\n");
				// 	curr_node = sort_swap(curr_node, curr_node->next, head, tail);
				// 	swapped = 1;
				// }
				if(compare_int(curr_node->data, curr_node->next->data) == 1){
					// printf("swap\n");
					curr_node = sort_swap(curr_node, curr_node->next, head, tail);
					swapped = 1;
				}
				else{
					swapped = 0;
					curr_node = curr_node->next;
				}
			}	
			index++;	

		}while(curr_node->tail != 1);
		elements--;  
		curr_node = curr_node->next; 
    }while(swapped);

    return *head;
}

//sort swap, node_a comes directly before node_b
node_t *sort_swap(node_t * node_a, node_t * node_b, node_t **head, node_t ** tail){
	//preserving the previous and next nodes
	node_t *tmp_previous = node_a->previous;
	node_t *tmp_next = node_b->next;

	if(node_a->head == 1){
		node_b->head = 1;
		node_a->head = 0;
		*head = node_b;
	}
	if(node_b->tail == 1){
		node_a->tail = 1;
		node_b->tail = 0;
		*tail = node_a;
	}

	//placing node_b in node_a's spot with supporting connections
	node_b->next = node_a;
	node_b->previous = tmp_previous;
	tmp_previous->next = node_b;
	
	//placing node_a in node_b's spot with supporting connections
	node_a->next = tmp_next;
	node_a->previous = node_b;
	tmp_next->previous = node_a;

}

//compare function from the user: What is the input?
node_t *compare_nodes(node_t *x, node_t *y){

}

//compare x & y. return 1 if x is greater, 0 if y and -1 if equal.
int compare_int(void *x, void *y){
	if(*(int*) x > *(int*) y){
		return 1;
	}
	else if(*(int*) y > *(int*) x){
		return 0;
	}
	else{
		return -1;
	}
}

//free function from the user: What is the input?
void free_data(void *x, void (*f)(void *)){

}
