#include <stdlib.h>
#include <stdio.h>

struct node {

   struct node *next;
   int data;
   int key;
};  

struct node *head = NULL;
struct node *tail = NULL;

//create a list with x nodes
void create(int x);
//print current node
void printNode(struct node* n);
//print list
void printNodes();
//add element to head
void addFirst(int key, int data);
//delete nodes
void deleteFirst();
//empty the list
void emptyList();
//find data or key match in list (where i == index in node)
struct node *find(int val, int i);


int main(){
	addFirst(1, 1);
	addFirst(2, 1);
	addFirst(3, 1);

	printf("Original List: ");
	printNodes();

	emptyList();

	printf("New List: ");
	printNodes();

	addFirst(1, 1);
	addFirst(2, 1);
	addFirst(3, 1);

	printf("Searching list for value 2- ");
	printNode(find(2, 1));
	printf("Searching list for value 4- ");
	printNode(find(4, 1));
	printf("Searching list for key 1- ");
	printNode(find(1, 0));
	printf("Searching list for invalid index- ");
	printNode(find(1, 4));

}

/*
//create a list with x nodes
void create(int x){
	for(int i = 0; i < x; i++){
		struct node *newNode = (struct node*) malloc(sizeof(struct node));
		head = newNode;
	}
}
*/

//print current node
void printNode(struct node* n){
	if(n == NULL) printf("%s\n", "Empty Node");
	else printf("Node: (%d, %d)\n", n->key, n->data);
}

//print list
void printNodes(){
	struct node *temp = head;

	if(temp == NULL){
		printf("%s\n", "Empty List");
	}
	else{
		printf("[");

		do{
			printf("(%d, %d),", temp->key, temp->data);
			temp =temp->next;
		} while(temp != NULL);

		printf("]\n");
	}
}

//add element to head
void addFirst(int data, int key){
	struct node *newNode = (struct node*) malloc(sizeof(struct node));

	newNode->data = data;
	newNode->key = key;
	newNode->next = head;

	head = newNode;
}

//delete head
void deleteFirst(){
	struct node *temp = head;

	head = head->next;
}

//empty the list
void emptyList(){
	head = NULL;
	tail = NULL;
}

//find data or key match in list (where i == index in node)
struct node *find(int val, int i){
	if(i > 1) printf("Unexpected Index!");
	else{
		struct node *temp = head;
		//match on Key
		if(i == 0){
			while(temp != NULL){
				if(temp->key == val) return temp;
				else temp = temp->next;
			}
		}
		//match on data
		else {
			while(temp != NULL){
				if(temp->data == val) return temp;
				else temp = temp->next;
			}
		}
		printf("Not Found/");
		return NULL;
	}
}