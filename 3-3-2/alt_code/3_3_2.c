#include <stdlib.h>
#include <stdio.h>

struct node {

   struct node *next;
   int data;
}; 

int size = 0; 

struct node *head = NULL;
struct node *tail = NULL;

//create a list with x nodes
void create(int x);
//print current node
void printNode(struct node *n);
//print list
void printNodes(struct node *n);
//add element to head
void addFirst(int data);
//delete nodes
void deleteFirst();
//empty the list
void emptyList();
//find first match in list
struct node *find(int val);
//iterate to index
struct node *transverse(struct node *n, int i);
//insert a node in a specified place i
void insert(int val, int i);
//join two linked lists
struct node *merge(struct node *left, struct node *right);
//sort alphanumerically, returns the head of the list
struct node *mergeSort(struct node *oldHead, int len);

//Testing

int main(){
	addFirst(1);
	addFirst(2);
	addFirst(3);

	printf("Original List: ");
	printNodes(head);
	printf("Size: %d\n", size);

	emptyList();

	printf("New List: ");
	printNodes(head);
	printf("Size: %d\n", size);

	addFirst(1);
	addFirst(2);
	addFirst(3);
	addFirst(2);

	printf("Next List: ");
	printNodes(head);
	printf("Size: %d\n", size);

	printf("Searching list for value 2- ");
	printNode(find(2));
	printf("Searching list for value 4- ");
	printNode(find(4));

	printf("Before\n");
	printNodes(head);
	printf("Size: %d\n", size);
	insert(6, 2);
	printf("After\n");
	printNodes(head);
	printf("Size: %d\n", size);

	emptyList();
	addFirst(4);
	addFirst(7);
	addFirst(1);

	printf("New List\n");
	printNodes(head);
	printf("Size: %d\n", size);
	printf("MERGING\n");

	printNodes(head);
	mergeSort(head, size);

	printf("End List\n");
	printNodes(head);
	printf("Size: %d\n", size);
}































/*
//create a list with x nodes
void create(int x){
	for(int i = 0; i < x; i++){
		struct node *newNode = (struct node*) malloc(sizeof(struct node));
		head = newNode;
	}
}
*/

//print current node
void printNode(struct node *n){
	if(n == NULL) printf("%s\n", "Empty Node");
	else printf("Node: (%d)\n", n->data);
}

//print list
void printNodes(struct node *n){
	struct node *temp = n;

	if(temp == NULL){
		printf("%s\n", "Empty List");
	}
	else{
		printf("[");

		do{
			printf("(%d),", temp->data);
			temp =temp->next;
		} while(temp != NULL);

		printf("]\n");
	}
}

//add element to head
void addFirst(int data){
	struct node *newNode = (struct node*) malloc(sizeof(struct node));

	newNode->data = data;
	newNode->next = head;

	head = newNode;

	size++;
}

//delete head
void deleteFirst(){
	struct node *temp = head;

	head = head->next;
	
	size--;
}

//empty the list
void emptyList(){
	head = NULL;
	tail = NULL;

	size = 0;
}

//find first match in list
struct node *find(int val){
	struct node *temp = head;

	while(temp != NULL){
		if(temp->data == val) return temp;
		else temp = temp->next;
	}

	printf("Not Found/");
	return NULL;
}

//iterate to index, returns the node BEFORE indexed node.
struct node *transverse(struct node *n, int i){
	printf("...\n");	
	struct node *temp = n;

	//starts at 1 because temp represents the node BEFORE indexed node
	if(i == 0) 
	for(int x = 1; x < i; x++){
		if(temp->next == NULL) break;
		else{
			temp = temp->next;
			printf("(%d)\n", temp->data);
		}
	}
	return temp;	
}

//insert a node as element i
void insert(int val, int i){
	struct node *n = (struct node*) malloc(sizeof(struct node));

	struct node *previous = transverse(head, i);

	n->data = val;

	previous = previous->next;
	n->next = previous->next;
	previous->next = n;

	size++;
}

//join two linked lists
struct node *merge(struct node *left, struct node *right){
	struct node *newHead = NULL;

	printNodes(left);
	printf("\nLeft^Right\n");
	printNodes(right);

	if(left->data > right->data) {
		newHead = right;
		printf("\nNew Head:");
		printNode(newHead);
		newHead->next = merge(left, right->next);
	}
	else{
		newHead = left;
		printf("\nNew Head:");
		printNode(newHead);
		newHead->next = merge(left->next, right);
	}
}

//sort alphanumerically, returns the head of the list
struct node *mergeSort(struct node *oldHead, int len){
	//break the right off from the left
	struct node *newHead = transverse(oldHead, len/2)->next;
	struct node *output = NULL;
	transverse(oldHead, len/2)->next = NULL;

	if(len == 1) {
		
		return oldHead;
	}
	else if (len == 2){
		if(oldHead->data > oldHead->next->data){
			output = oldHead->next;
			output->next = oldHead;
			output->next->next = NULL;
			return output;
		}
		else if(oldHead->data < oldHead->next->data){
			return oldHead;
		}
	}
	else return merge(mergeSort(oldHead, len/2), mergeSort(newHead, len-len/2));
}