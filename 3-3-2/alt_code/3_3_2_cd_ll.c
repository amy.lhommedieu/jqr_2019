#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct node {

	struct node *next;
 	struct node *previous;

	int head;
	int tail;

	void *data;

} node_t; 

typedef struct circular_doubly_linked_list {

	struct node *head;
    struct node *tail;

	node_t *(*compare)(node_t *x, node_t *y);
	void (*free)(node_t *n);
	void (*print)(node_t *n);

} cd_ll;

//create a list with x nodes
void init(cd_ll **list, node_t *(*compare)(node_t *x, node_t *y), void (*free)(node_t *n)); //cd_ll
//print current node
void print_node(node_t *n);
//print list
void print_all(cd_ll *list);
void print_tree(node_t *head);
//add element to head
void prepend_node(void *data, cd_ll **list);\
//insert element after node_t *n
void insert_node(void *data, node_t **n, cd_ll **list);
//add element to end
void append_node(void *data, cd_ll **list);
//delete nodes
void delete_node(node_t **n, node_t **head, node_t **tail); //cd_ll
//pop the top element
node_t pop_first(); //cd_ll
//peek/print at the top element without removing
node_t peek_first(); //cd_ll
//pop the bottom/last element
node_t pop_last(); //cd_ll
//peek/print at the bottom/last element without removing
node_t peek_last(); //cd_ll
//empty the list
void empty_list(node_t **head, node_t **tail); //cd_ll
//find first match in list 
node_t *find_node(void *val, node_t **head); //cd_ll
//sort alphanumerically
void sort(cd_ll **list);
//sort swap
node_t *sort_swap(node_t * node_a, node_t * node_b, cd_ll **list);
//compare nodes
node_t *compare_nodes(node_t *x, node_t *y);
//compare x & y. return 1 if x is greater, 0 if y and -1 if equal.
node_t *compare_int(node_t *x, node_t *y);
//free function from the user: What is the input?
void free_int(node_t *x);

//TESTING Capability

int main(){
	cd_ll *node_list = (cd_ll*)malloc(sizeof(cd_ll));

    init(&node_list, &compare_int, &free_int);
	
	int d = 4;
	int f = 7;
	int c = 3;
	int b = 2;
	int a = 1;
	int e = 6;

	prepend_node(&a, &node_list);

	prepend_node(&b, &node_list);
	prepend_node(&c, &node_list);
	// prepend_node(&d, &node_list);

	insert_node(&e, &node_list->tail, &node_list);
	insert_node(&f, &node_list->tail, &node_list);

	append_node(&d, &node_list);

	// append_node(&e, &node_list);

	printf("Original List: \n");
	// print_all(node_list->head);
	// printf("\n");
	print_tree(node_list->head);

	sort(&node_list);


	// delete_node(&node_list->head, &node_list);

	// empty_list(&node_list);

	printf("New List: \n");
	print_tree(node_list->head);
	// print_all(node_list);
}

//create a list to hold nodes with appropriate functions for the data type
void init(cd_ll **list, node_t *(*compare)(node_t *x, node_t *y), void (*free)(node_t *n)){
    cd_ll *temp_list = *list;
	temp_list->head = (node_t*)malloc(sizeof(node_t));
	temp_list->head = NULL;

	temp_list->tail = (node_t*)malloc(sizeof(node_t));
	temp_list->tail = temp_list->head;

	temp_list->compare = compare;
	temp_list->free = free;
}

//print current node
void print_node(node_t *n){
	char title[10] = "";
	char label_next[10] = "";
	char label_previous[10] = "";
	if(n == NULL) {
		printf("Empty Node\n");
	}
	else {
		//head element
		if(n->head == 1){
			strncpy(title, "Head: ", 9);
			strncpy(label_previous, " (Tail)", 9);
		}
		//tail element
		if(n->tail == 1){
			strncpy(title, "Tail: ", 9);
			strncpy(label_next, " (Head)", 9);
		}
		//element right after head
		if(n->previous->head == 1){
			strncpy(label_previous, " (Head)", 9);
		}
		//element right before tail
		if(n->next->tail == 1){
			strncpy(label_next, " (Tail)", 9);
		}

		printf("%sNode: (%d)\n", title, *(int*)n->data);
		printf("Previous%s: (%d)\n", label_previous, *(int*)n->previous->data);
		printf("Next%s: (%d)\n", label_next, *(int*)n->next->data);
		printf("\n");
	}
}

//print list
void print_all(cd_ll *list){
	cd_ll *temp = list;

	if(temp->head == NULL || temp->tail == NULL){
		printf("Empty List\n");
	}
	else{
		do{
			print_node(temp->head);
			temp->head = temp->head->next;
		} while(temp->head->head != 1);
	}
}

//print list
void print_tree(node_t *head){
	node_t *temp = head;

	if(temp == NULL){
		printf("Empty List");
	}
	else{
		do{
			if(temp == NULL) {
				printf("Empty Node\n");
			}
			else{
				printf("-(%d)-", *(int*)temp->data);
			}
			temp = temp->next;
		} while(temp->head != 1);
	}
	printf("\n");
}

//add element to head
void prepend_node(void *data, cd_ll **list){
	cd_ll* temp = *list;
	node_t *newNode = (node_t*) malloc(sizeof(node_t));

	newNode->data = data;

	//empty list
	if(temp->head == NULL && temp->tail == NULL){
		//circular to itself.
		newNode->next = newNode;
		newNode->previous = newNode;

		//set flags
		newNode->head = 1;
		newNode->tail = 1;

		//track head/tail
		temp->head = newNode;
		temp->tail = newNode;
	}

	//n elements in list
	else{
		//make circular
		newNode->next = temp->head;
		temp->head->previous = newNode;
		newNode->previous = temp->tail;
		temp->tail->next = newNode;

		//set flags
		temp->head->head = 0;
		newNode->head = 1;

		//track head/tail
		temp->tail = temp->tail;
		temp->head = newNode;
	}
}

//insert element after node_t *n. Will never be head.
void insert_node(void *data, node_t **n, cd_ll **list){
	node_t *newNode = (node_t*) malloc(sizeof(node_t));
	cd_ll *temp = *list;

	newNode->data = data;

	//empty list, since list is circular if there are any elements there will not be a NULL 
	if(*n == NULL){
		//circular to itself.
		newNode->next = newNode;
		newNode->previous = newNode;

		//set flags
		newNode->head = 1;
		newNode->tail = 1;

		//track head/tail
		temp->head = newNode;
		temp->tail = newNode;
	}
	//n elements in list
	else{
		//linking nodes
		newNode->previous = *n;
		newNode->next = newNode->previous->next;
		newNode->previous->next = newNode;
		newNode->next->previous = newNode;

		if(*n == temp->tail){
			//set flags and track tail
			temp->tail->tail = 0;
			newNode->tail = 1;
			temp->tail = newNode;
		}
	}
}

//add element to end
void append_node(void *data, cd_ll **list){
	cd_ll *temp = *list;
	insert_node(data, &temp->tail, list);
}


//delete head
void delete_node(node_t **n, node_t **head, node_t **tail){
	node_t *temp = *n;

	printf("Delete ");

	print_node(temp);

	//head & more than one element
	// if(*n == *head && temp->next != NULL){
	if(*n == *head && temp->tail != 1){
		printf("Head\n");
		// temp->next->previous = NULL;
		temp->next->previous = *tail;
		temp->next->head = 1;
		*head = temp->next;
	}

	//tail & more than one element
	// else if(*n == *tail && temp->previous != NULL){
	else if(*n == *tail && temp->head != 1){
		printf("Tail\n");
		// temp->previous->next = NULL;
		temp->previous->tail = 1;
		*tail = temp->previous;
	}

	//middle element, & more than one element
	else if(temp->next != NULL && temp->previous != NULL){
		printf("Middle\n");
		temp->next->previous = temp->previous; 
		temp->previous->next = temp->next;
	}

	//single element
	else{
		*head = NULL;
		*tail = *head;
	}
}


//empty the list
void empty_list(node_t **head, node_t **tail){
	node_t *temp = *head;

	if(temp == NULL){
		printf("%s\n", "Empty List\n");
	}
	else{
		do{
			//printf("(%d),", *(int*)temp->data);
			delete_node(&temp, head, tail);
			temp = temp->next;
		} while(temp != NULL);
	}
}


//find first match in list
node_t *find_node(void *val, node_t **head){
	node_t *temp = *head;

	while(temp != NULL){
		if(temp->data == val) return temp;
		else temp = temp->next;
	}

	printf("Not Found/");
	return NULL;
}


//sort according to user sort function.
void sort(cd_ll **list){
	cd_ll *temp = *list;

	node_t *curr_node = temp->head;
	node_t *count = temp->head;

    int swapped = 0;
	int elements = 0;
	int index = 0;

	//counts up the elements in 
	while(count->tail != 1){
		elements++;
		count = count->next;
	}
    
    do{
        swapped = 0;
		index = 0;

		do{
			//pushes the highest val to the end, and then ignores it.
			if(elements < index){
				curr_node = curr_node->next;
			}
			else{
				// print_node(curr_node);
				if(curr_node == compare_int(curr_node, curr_node->next)){
					// printf("swap\n");
					curr_node = sort_swap(curr_node, curr_node->next, list);
					swapped = 1;
				}
				// if(compare_int(curr_node->data, curr_node->next->data) == 1){
				// 	// printf("swap\n");
				// 	curr_node = sort_swap(curr_node, curr_node->next, head, tail);
				// 	swapped = 1;
				// }
				else{
					swapped = 0;
					curr_node = curr_node->next;
				}
			}	
			index++;	

		}while(curr_node->tail != 1);
		elements--;  
		curr_node = curr_node->next; 
    }while(swapped);
}

//sort swap, node_a comes directly before node_b
node_t *sort_swap(node_t * node_a, node_t * node_b, cd_ll **list){
	//preserving the previous and next nodes
	node_t *tmp_previous = node_a->previous;
	node_t *tmp_next = node_b->next;
	cd_ll *temp = *list;

	if(node_a->head == 1){
		node_b->head = 1;
		node_a->head = 0;
		temp->head = node_b;
	}
	if(node_b->tail == 1){
		node_a->tail = 1;
		node_b->tail = 0;
		temp->tail = node_a;
	}

	//placing node_b in node_a's spot with supporting connections
	node_b->next = node_a;
	node_b->previous = tmp_previous;
	tmp_previous->next = node_b;
	
	//placing node_a in node_b's spot with supporting connections
	node_a->next = tmp_next;
	node_a->previous = node_b;
	tmp_next->previous = node_a;

}

//compare function from the user: What is the input?
node_t *compare_nodes(node_t *x, node_t *y){

}

//compare x & y. return 1 if x is greater, 0 if y and -1 if equal.
node_t *compare_int(node_t *x, node_t *y){
	if(*(int*) x->data > *(int*) y->data){
		return x;
	}
	else if(*(int*) y->data > *(int*) x->data){
		return y;
	}
	else{
		return NULL;
	}
}

//free function from the user: What is the input?
void free_int(node_t *x){

}
