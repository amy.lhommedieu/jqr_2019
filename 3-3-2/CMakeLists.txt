cmake_minimum_required(VERSION 3.10)
project(JQR VERSION 1.0)
include_directories(inc)
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin) #is relative to build directory
set(CMAKE_BUILD_TYPE Debug)

set(CD_LL
	src/cd_ll.c
)
	
add_executable(cd_ll "cd_ll_main.c" ${CD_LL})
