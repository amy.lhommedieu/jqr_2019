#ifndef CD_LL_USER_HEADER_LHOMMEDIEU
#define CD_LL_USER_HEADER_LHOMMEDIEU

//compare x & y. return greater node.
node_t *compare_nodes_int(node_t *x, node_t *y, int *p_return_val/*0 if equal, 1 for 1>2, 2 for 1<2*/);

//print int data type
int print_int(node_t *n);

//test for adding 1 to each node in list
void add_one(node_t **n);

//
void output_to_file(node_t *n, FILE *file);

// //
// void input_from_file(node_t *n, FILE *file);

#endif