#include "cd_ll.h"
#include "cd_ll_user.h"
// #include "../inc/cd_ll.h"
// #include "../inc/cd_ll_user.h"
// #include "../inc/cd_ll_stack_queue.h"

//create a list to hold nodes with appropriate functions for the data type
void init(cd_ll **list, node_t *(*compare)(node_t *x, node_t *y), int (*print)(node_t *n)){
    cd_ll *temp_list = *list;
	temp_list->head = NULL;

	temp_list->tail = temp_list->head;

	temp_list->compare = compare;
	temp_list->print = print;
}

//free the circular-doubly linked list
void free_cd_ll(cd_ll *list){ //test with no nodes, 1 node, 2 nodes, and multiple nodes.
	//until the end, iterate and free previous
	
	//size
	while(list->head != NULL){
		node_t *temp = list->tail;
		list->tail = list->tail->previous;
		list->tail->next = list->head;
		list->head->previous = list->tail;
		free_node(temp);
		if(list->head == list->tail){
			free_node(list->head);
			list->head = NULL;
			list->tail = list->head;
		}
		list->size--;
	}
	printf("List Freed\n");

	free(list);
	list = NULL;
}

//free the node
void free_node(node_t *x){
	free(x);
	x->previous = NULL;
	x->next = NULL;
	x = NULL;
}

//malloc space for node
node_t *create_node(void *data){
	node_t *new_node = (node_t *) malloc(sizeof(node_t));

	new_node->data = data;

	return new_node;
}

//print current node
void print_node(node_t *n, cd_ll list){
	char title[10] = "";
	char label_next[10] = "";
	char label_previous[10] = "";
	if(NULL == n) {
		printf("Empty Node\n");
	}
	else {
		//head element
		if(n->is_head){
			strncpy(title, "Head: ", 9);
			strncpy(label_previous, " (Tail)", 9);
		}
		//tail element
		if(n->is_tail){
			strncpy(title, "Tail: ", 9);
			strncpy(label_next, " (Head)", 9);
		}
		//element right after head
		if(n->previous->is_head){
			strncpy(label_previous, " (Head)", 9);
		}
		//element right before tail
		if(n->next->is_tail){
			strncpy(label_next, " (Tail)", 9);
		}
		printf("%sNode: (%d)\n", title, list.print(n));
		printf("Previous%s: (%d)\n", label_previous, list.print(n->previous));
		printf("Next%s: (%d)\n", label_next, list.print(n->next));
		printf("\n");
	}
}

//print list
void print_list(cd_ll **list){
	cd_ll *temp = *list;

	if(NULL == temp->head || NULL == temp->tail){
		printf("Empty List\n");
	}
	else{
		do{
			print_node(temp->head, *temp);
			temp->head = temp->head->next;
		} while(!temp->head->is_head);
	}
}

//print whole list in a pretty way.
void print_tree(cd_ll **list){
	cd_ll *temp = *list;
	node_t *temp_node = temp->head;
	do{
		if(NULL == temp_node) {
			printf("Empty List\n");
		}
		else{
			printf("-(%d)-", print_int(temp_node));
		}
		temp_node = temp_node->next;
	} while(!temp_node->is_head);
	printf("\n");
}

//add element to head
void prepend_node(node_t *node, cd_ll **list){
	cd_ll* temp = *list;

	if(NULL != node){
		//size
		//empty list
		if(NULL == temp->head && NULL == temp->tail){
			//circular to itself.
			node->next = node;
			node->previous = node;

			//track head/tail
			temp->head = node;
			temp->tail = node;

			//set flags
			node->is_tail = 1;
		}

		//one element in list
		else if(temp->head == temp->tail){
			//link to old head
			node->next = temp->head;
			node->previous = temp->head;
			temp->head->previous = node;
			temp->head->next = node;
			
			//track head/tail
			temp->tail = temp->head;
			temp->head = node;

			//set flags
			temp->tail->is_head = 0;
			temp->tail->is_tail = 1;
		}

		//n elements in list
		else{
			//make circular
			node->next = temp->head;
			temp->head->previous = node;
			node->previous = temp->tail;
			temp->tail->next = node;

			//track head/tail
			temp->tail = temp->tail;
			temp->head = node;

			//set flags
			node->next->is_head = 0;
		}
		node->is_head = 1;	
		temp->size++;
	}
}


//add element to tail
void append_node(node_t *node, cd_ll **list){
	cd_ll *temp = *list;
	insert_node(node, &temp->tail, list);
}

//insert element after node_t *n. Will never be head.
void insert_node(node_t *node, node_t **n, cd_ll **list){
	cd_ll *temp = *list;

	if(NULL != node){
		//size
		//empty list, since list is circular if there are any elements there will not be a NULL 
		if(NULL == *n){
			//circular to itself.
			node->next = node;
			node->previous = node;

			//set flags
			node->is_head = 1;
			node->is_tail = 1;

			//track head/tail
			temp->head = node;
			temp->tail = node;
		}
		//n elements in list
		else{
			//linking nodes
			node->previous = *n;
			node->next = node->previous->next;
			node->previous->next = node;
			node->next->previous = node;

			if(*n == temp->tail){
				//set flags and track tail
				temp->tail->is_tail = 0;
				node->is_tail = 1;
				temp->tail = node;
			}
		}
		temp->size++;
	}
}

//test
//insert element by index number
void insert_node_index(int index, node_t **n, cd_ll **list){
	cd_ll *temp = *list;
	node_t *temp_node = temp->head;
	
	//size?
	if(temp->size == 0){
		prepend_node(temp_node, list);
	}
	else{
		if(index < temp->size){
			for(index; index > 0;index--){
				temp_node = temp_node->next;
			}
			//link(node);
			temp->size++;
		}
		else if(index == temp->size){
			append_node(temp_node, list);
			temp->size++;
		}
		else{
			printf("Index out of range.\n");
		}
	}
}

//delete head
void delete_node(node_t **n, cd_ll **list){
	cd_ll *temp = *list;
	if(NULL == *n){
		printf("Node doesn't exist.\n");
		return;
	}
	if(NULL == temp->head){
		printf("Trying to delete from an empty list.\n");
		return;
	}
	node_t *temp_node = *n;

	printf("\nDelete ");

	print_node(temp_node, *temp);

	//size
	//head & more than one element
	if(temp_node->is_head && !temp_node->is_tail){
		temp_node->next->is_head = 1;

		temp_node->next->previous = temp->tail;
		temp->head = temp_node->next;
		temp->tail->next = temp->head;
	}

	//tail & more than one element
	else if(temp_node->is_tail && !temp_node->is_head){
		temp->head->previous = temp_node->previous; 
		temp->tail = temp_node->previous; 

		temp->tail->is_tail = 1;
		temp->tail->next = temp->head;
	}

	//middle element, & more than one element
	else if(!temp_node->is_tail && !temp_node->is_head){
		temp_node->previous->next = temp_node->next;
		temp_node->next->previous = temp_node->previous; 
	}

	//single element
	else{
		temp->head = NULL;
		temp->tail = temp->head;
	}
	free_node(temp_node);
	// print_node(*n, *temp);
	temp->size--;
}


//empty the list
void empty_list(cd_ll **list){
	cd_ll *temp = *list;

	//size
	if(NULL == temp->head){
		printf("%s\n", "Empty List\n");
	}
	else{
		do{
			//printf("(%d),", *(int*)temp->data);
			delete_node(&temp->head, list);
			// temp->head = temp->head->next;
		} while(temp->head != NULL);
	}
}


//pop the top element
node_t *pop_first(cd_ll **list){
	cd_ll *temp = *list;

	printf("Pop First\n");

	//size
	if(NULL == temp->head){
		printf("%s\n", "Empty List\n");
		return NULL;
	}
	node_t *ret_node = unlink_node(temp->head, list);
	return ret_node;
}

//peek/print at the top element without removing
node_t *peek_first(cd_ll **list){
	cd_ll *temp = *list;

	printf("Peek First\n");

	//size
	if(NULL == temp->head){
		printf("%s\n", "Empty List\n");
		return NULL;
	}
	return temp->head;
}

//pop the bottom/last element
node_t *pop_last(cd_ll **list){
	cd_ll *temp = *list;

	printf("Pop Last\n");

	//size
	if(NULL == temp->head){
		printf("%s\n", "Empty List\n");
		return NULL;
	}
	node_t *ret_node = unlink_node(temp->tail, list);
	return ret_node;
}

//peek/print at the bottom/last element without removing
node_t *peek_last(cd_ll **list){
	cd_ll *temp = *list;

	printf("Peek Last\n");

	//size
	if(NULL == temp->head){
		printf("%s\n", "Empty List\n");
		return NULL;
	}
	return temp->tail;
}

//push node to front
node_t *push_front(node_t *node, cd_ll **list){
	prepend_node(node, list);
	
}

//push node to back
node_t *push_back(node_t *node, cd_ll **list){
	append_node(node, list);
}

//find first match in list
node_t *find_node(void *val, cd_ll **list){
	cd_ll *temp = *list;
	node_t *curr_node = temp->head;

	while(curr_node != NULL){
		if(curr_node->data == val) {
			return curr_node;
		}
		else {
			curr_node = curr_node->next;
		}
	}

	printf("Node not found\n");
	return NULL;
}

//not switching correctly.
//sort according to compare function.
void sort(cd_ll **list){
	cd_ll *temp = *list;

	if(NULL == temp->head){
		printf("Trying to sort an empty list.\n");
		return;
	}

	node_t *curr_node;
	curr_node = temp->head;
	node_t *count;
	count = temp->head;

    int swapped = 0;
	int elements = 0;
	int index = 0;

	//counts up the elements in 
	while(!count->is_tail){
		elements++;
		count = count->next;
	}

	count = NULL;
    
    do{
        // swapped = 0;
		index = 0;

		do{
			//pushes the highest val to the end, and then ignores it.
			if(elements < index){
				while(!curr_node->is_tail){
					curr_node = curr_node->next;
				}
			}
			else{
				// print_node(curr_node);
				if(curr_node == compare_nodes_int(curr_node, curr_node->next)){
					// printf("swap\n");
					curr_node = swap_nodes(curr_node, curr_node->next, list);
					// swapped = 1;
				}
				else{
					// swapped = 0;
					curr_node = curr_node->next;
				}
				index++;
			}	
		}while(!curr_node->is_tail);
		elements--;  
		curr_node = curr_node->next; 
    }while(elements > 0);
}

//sort swap, node_a comes directly before node_b
node_t *swap_nodes(node_t * node_a, node_t * node_b, cd_ll **list){
	//preserving the previous and next nodes
	node_t *tmp_previous = node_a->previous;
	node_t *tmp_next = node_b->next;
	cd_ll *temp = *list;

	//do more checks

	if(NULL == node_a){
		return node_b;
	}
	if(NULL == node_b){
		return node_a;
	}

	if(1 == node_a->is_head){
		node_b->is_head = 1;
		node_a->is_head = 0;
		temp->head = node_b;
	}
	if(1 == node_b->is_tail){
		node_a->is_tail = 1;
		node_b->is_tail = 0;
		temp->tail = node_a;
	}

	//placing node_b in node_a's spot with supporting connections
	node_b->next = node_a;
	node_b->previous = tmp_previous;
	tmp_previous->next = node_b;
	
	//placing node_a in node_b's spot with supporting connections
	node_a->next = tmp_next;
	node_a->previous = node_b;
	tmp_next->previous = node_a;

	return node_a;
}

//unlink the node, makes next and previous of n NULL
node_t *unlink_node(node_t *n, cd_ll **list){
	cd_ll *temp = *list;

	if(temp->tail != temp->head && NULL != n){
		node_t *tmp_previous = n->previous;
		node_t *tmp_next = n->next;
		//update head/tail pointers
		if(1 == n->is_head){
			tmp_next->is_head = 1;
			n->is_head = 0;
			temp->head = tmp_next;
		}	

		if(1 == n->is_tail){
			tmp_previous->is_tail = 1;
			n->is_tail = 0;
			temp->tail = tmp_previous;
		}	

		//linking the previous and next
		tmp_previous->next = tmp_next;
		tmp_next->previous = tmp_previous;

	}
	return n;
}


//linking the nodes
node_t *link_nodes(node_t *x, node_t *y, node_t *z, cd_ll **list){

}

//USER DEFINED

//compare x & y. return 'greater' node.
node_t *compare_nodes_int(node_t *x, node_t *y){
	if(*(int*) x->data > *(int*) y->data){
		return x;
	}
	else if(*(int*) y->data > *(int*) x->data){
		return y;
	}
	else{
		return NULL;
	}
}

//print int data type
int print_int(node_t *n){
	return (NULL == n->data) ? -1 : *(int*) n->data;
}


//(un)linking the nodes
// node_t *node_connections(node_t *previous, node_t *curr_node, node_t *next, cd_ll **list, int action /*0: unlink, 1: link*/){
/*	cd_ll *temp = *list;

	if(UNLINK == action){
		if(temp->tail != temp->head && NULL != n){
			//update head/tail pointers
			if(1 == curr_node->is_head){
				next->is_head = 1;
				curr_node->is_head = 0;
				temp->head = next;
			}	

			if(1 == curr_node->is_tail){
				previous->is_tail = 1;
				curr_node->is_tail = 0;
				temp->tail = previous;
			}			

			previous->next = next;
			next->previous = previous;

			return curr_node;
		}
	}

	else if(LINK == action){
		//update head/tail pointers: defaults to head
		if(1 == next->is_head){
			curr_node->is_head = 1;
			next->is_head = 0;
			temp->head = curr_node;
		}	

		if(1 == previous->is_tail){
			curr_node->is_tail = 1;
			previous->is_tail = 0;
			temp->tail = curr_node;
		}			

		previous->next = next;
		next->previous = previous;

		return curr_node;
	}

	else{
		printf("Unexpected action input. Nothing happened");
		return curr_node;
	}
}*/

//insert by index
//remove by index
//check for null nodes node_connections
//error check everywhere
//make joint link/unlink code
//simplify all code
//cd_ll size
//bool is_head/is_tail/is_empty/
//test all functions
//finish insert_node_index