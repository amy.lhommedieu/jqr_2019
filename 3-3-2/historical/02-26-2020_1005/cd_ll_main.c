#include "cd_ll.h"
#include "cd_ll_user.h"
// #include "src/cd_ll.c"

#define START_SIZE 4

int main(){
	cd_ll *node_list = (cd_ll*)malloc(sizeof(cd_ll));

    init(&node_list, &compare_nodes_int, &print_int);
	
	int d = 4;
	// int f = 7;
	int c = 3;
	int b = 2;
	int a = 1;
	// int e = 6;

	// node_t *first_node = create_node(&a);
	// // free_node(first_node);

	prepend_node(create_node(&a), &node_list);
	prepend_node(create_node(&b), &node_list);
	prepend_node(create_node(&c), &node_list);
	prepend_node(create_node(&d), &node_list);

	printf("Original List: \n");
	print_tree(&node_list);

	// // insert_node(create_node(&e), &node_list->tail, &node_list);
	// // insert_node(create_node(&f), &node_list->head, &node_list);
	// // insert_node(create_node(&d), &node_list->tail, &node_list);
	// // insert_node(create_node(&e), &node_list->tail, &node_list);

	// sort(&node_list);
	// print_tree(&node_list);


	// // delete_node(&node_list->tail->previous, &node_list);

	// // node_t *found_node = find_node(&d, &node_list);
	// // delete_node(&found_node, &node_list); 

	printf("New List: \n");
	print_tree(&node_list);

	// print_node(peek_first(&node_list), *node_list);
	// node_t *popped_first_node = pop_first(&node_list);
	// print_node(popped_first_node, *node_list);
	// print_tree(&node_list);

	// prepend_node(popped_first_node, &node_list);

	// print_node(peek_last(&node_list), *node_list);
	// node_t *popped_last_node = pop_last(&node_list);
	// print_node(popped_last_node, *node_list);
	// print_tree(&node_list);

	// empty_list(&node_list);

	// printf("Empty List: \n");
	// print_tree(&node_list);

	free_cd_ll(node_list);
}

//How is the data supposed to be input?
//I can't do it dynamically, especially
//because I don't know what data they are 
//giving me.

//if I change c to 9 in the above example,
//any node using that address is also 
//changed.