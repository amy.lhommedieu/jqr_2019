#ifndef CD_LL_STACK_QUEUE_HEADER_LHOMMEDIEU
#define CD_LL_STACK_QUEUE_HEADER_LHOMMEDIEU

//pop the top element
node_t *pop_first(cd_ll **list);
//peek/print at the top element without removing
node_t *peek_first(cd_ll **list);
//pop the bottom/last element
node_t *pop_last(cd_ll **list);
//peek/print at the bottom/last element without removing
node_t *peek_last(cd_ll **list);
//push node to front
node_t *push_front(node_t *node, cd_ll **list);
//push node to back
node_t *push_back(node_t *node, cd_ll **list);

#endif