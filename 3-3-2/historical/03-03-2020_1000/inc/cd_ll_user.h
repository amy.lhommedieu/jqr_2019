#ifndef CD_LL_USER_HEADER_LHOMMEDIEU
#define CD_LL_USER_HEADER_LHOMMEDIEU

//compare x & y. return greater node.
node_t *compare_nodes_int(node_t *x, node_t *y);

//print int data type
int print_int(node_t *n);

//test for adding 1 to each node in list
void add_one(node_t *n);
 
#endif