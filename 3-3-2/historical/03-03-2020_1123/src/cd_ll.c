#define DEBUG 0
// #define DEBUG 1

#if DEBUG
	#include "../inc/cd_ll.h"
	#include "../inc/cd_ll_user.h"
	#include "../inc/cd_ll_stack_queue.h"
#else
	#include "cd_ll.h"
	#include "cd_ll_user.h"
	#include "cd_ll_stack_queue.h"
#endif


//create a list to hold nodes with appropriate functions for the data type
void init(cd_ll *list, node_t *(*user_compare)(node_t *x, node_t *y), int (*user_print)(node_t *n), void (*user_function)){ //cd_ll
	list->head = NULL;
	list->tail = NULL;

	list->size = 0;

	//assign function pointers their respective addresses
	list->compare = user_compare;
	list->print = user_print;
	list->function = user_function;
}

//free the circular-doubly linked list
void free_cd_ll(cd_ll *list){ //test with no nodes, 1 node, 2 nodes, and multiple nodes.
	empty_list(list);
	
	if(OUTPUT){
		printf("List Freed\n");
	}
	free(list);
	list = NULL;
}

//free the node
void free_node(node_t *x){
	free(x);
	x = NULL;
}

//malloc space for node
node_t *create_node(void *data){
	node_t *new_node = (node_t *) malloc(sizeof(node_t));
	new_node->data = data;
	new_node->is_head = 0;
	new_node->is_tail = 0;

	new_node->next = NULL;
	new_node->previous = NULL;

	return new_node;
}

//print current node
void print_node(node_t *n, cd_ll list){
	if(!OUTPUT){
		return;
	}
	char title[10] = "";
	char label_next[10] = "";
	char label_previous[10] = "";
	if(NULL == n) {
		printf("Empty Node\n");
	}
	else {
		//head element
		if(is_head(n)){
			strncpy(title, "Head: ", 9);
			strncpy(label_previous, " (Tail)", 9);
		}
		//tail element
		if(is_tail(n)){
			strncpy(title, "Tail: ", 9);
			strncpy(label_next, " (Head)", 9);
		}
		//element right after head
		if(is_head(n->previous)){
			strncpy(label_previous, " (Head)", 9);
		}
		//element right before tail
		if(is_tail(n->next)){
			strncpy(label_next, " (Tail)", 9);
		}
		printf("%sNode: (%d)\n", title, list.print(n));
		printf("Previous%s: (%d)\n", label_previous, list.print(n->previous));
		printf("Next%s: (%d)\n", label_next, list.print(n->next));
		printf("\n");	
	}
}

//print list
void print_list(cd_ll *list){
	if(!OUTPUT){
		return;
	}
	node_t *curr_node = list->head;

	if(NULL == curr_node){
		printf("Empty List\n");
	}
	else{
		do{
			print_node(curr_node, *list);
			curr_node = curr_node->next;
		} while(!is_head(curr_node));
	}
}

//print whole list in a pretty way.
void print_tree(cd_ll *list){
	if(!OUTPUT){
		return;
	}
	node_t *curr_node = list->head;

	if(NULL == curr_node) {
		printf("Empty List\n");
		return;
	}
	do{
		printf("-(%d)-", list->print(curr_node));
		curr_node = curr_node->next;
	}while(!is_head(curr_node));
	printf("\n");
}

//add element to head
void prepend_node(node_t *node, cd_ll *list){
	if(NULL != node){
		//empty list
		int ret_bool = is_empty(list);
		if(ret_bool){
			link_nodes(node, NULL, NULL);

			//track head/tail
			list->head = node;
			list->tail = node;

			//set flags
			node->is_tail = 1;
		}
		//one element in list
		else if(1 == list->size){
			link_nodes(node, list->head, NULL);
			
			//track head/tail
			list->tail = list->head;
			list->head = node;

			//set flags
			node->next->is_head = 0;
			list->tail->is_tail = 1;
		}
		//n elements in list
		else{
			link_nodes(list->tail, node, list->head);

			//track head/tail
			list->head = node;

			//set flags
			node->next->is_head = 0;
		}
		node->is_head = 1;	
		list->size++;
	}
}


//add element to tail
void append_node(node_t *node, cd_ll *list){
	insert_node(node, list->tail, list);
}

//insert element after node_t *n. Will never be head.
void insert_node(node_t *new_node, node_t *old_node, cd_ll *list){
	if(NULL != new_node){
		//size
		//empty list, since list is circular if there are any elements there will not be a NULL 
		if(NULL == old_node){
			prepend_node(new_node, list);
		}
		//n elements in list
		else{
			//linking nodes
			link_nodes(old_node, new_node, old_node->next);

			if(old_node == list->tail){
				//set flags and track tail
				list->tail->is_tail = 0;
				new_node->is_tail = 1;
				list->tail = new_node;
			}
			list->size++;
		}
	}
}

//insert element by index number
void insert_node_index(int index, node_t *n, cd_ll *list){
	node_t *temp_node = list->head;
	
	if(is_empty(list) && index == 0){
		prepend_node(n, list);
	}
	else if(index >= list->size){
		if(index > list->size){
			printf("Index out of range. Appending to tail.\n");
		}
		append_node(n, list);
	}
	else if(index < list->size){
		for(index; index >0; index--){
			temp_node = temp_node->next;
			index--;
		}
		insert_node(n, temp_node, list);
	}
}

//delete node, need to use find_node unless using head or tail
void delete_node(node_t *n, cd_ll *list){
	if(is_empty(list)){
		printf("Trying to delete from an empty list.\n");
		return;
	}
	if(NULL == n){
		printf("Node doesn't exist.\n");
		return;
	}

	if(OUTPUT){
		printf("\nDelete ");
		print_node(n, *list);
	}
	unlink_node(n, list);
	free_node(n);
	list->size--;
}

//delete node at index
void delete_index(int index, cd_ll *list){
	node_t *temp_node = list->head;
	
	if(is_empty(list)){
		printf("Trying to delete from an empty list.\n");
		return;
	}
	if(index >= list->size){
		if(index > list->size){
			printf("Index out of range.\n");
			return;
		}

	}
	else if(index < list->size){
		for(index; index > 0; index--){
			temp_node = temp_node->next;
		}
	}
	
	if(OUTPUT){
		printf("\nDelete ");
		print_node(temp_node, *list);
	}

	unlink_node(temp_node, list);
	free_node(temp_node);
	list->size--;
}


//empty the list
void empty_list(cd_ll *list){
	if(is_empty(list)){
		printf("%s\n", "Empty List\n");
	}
	else{
		do{
			delete_node(list->head, list);
		} while(!is_empty(list));
	}
	list = NULL;
}


//pop the top element
node_t *pop(cd_ll *list){
	node_t *curr_node = list->head;

	if(is_empty(list)){
		printf("%s\n", "Empty List\n");
		return NULL;
	}
	unlink_node(curr_node, list);
	list->size--;
	return curr_node;
}

//peek/print at the top element without removing
node_t *peek_first(cd_ll *list){
	if(is_empty(list)){
		printf("%s\n", "Empty List\n");
		return NULL;
	}
	return list->head;
}

//pop the bottom/last element
node_t *pop_last(cd_ll *list){
	node_t *curr_node = list->tail;

	if(is_empty(list)){
		printf("%s\n", "Empty List\n");
		return NULL;
	}
	unlink_node(curr_node, list);
	list->size--;
	return curr_node;
}

//peek/print at the bottom/last element without removing
node_t *peek_last(cd_ll *list){
	//size
	if(0 == list->size){
		printf("%s\n", "Empty List\n");
		return NULL;
	}
	return list->tail;
}

//push node to front
void *push(node_t *node, cd_ll *list){
	prepend_node(node, list);
}

//push node to back
void *push_back(node_t *node, cd_ll *list){
	append_node(node, list);
}

//find first match in list
node_t *find_node(void *val, cd_ll *list){
	node_t *curr_node = list->head;

	do{
		if(curr_node->data == val) {
			return curr_node;
		}
		else {
			curr_node = curr_node->next;
		}
	}while(curr_node != list->head);

	printf("Node not found\n");
	return NULL;
}

//sort according to compare function.
void sort(cd_ll *list){
	if(0 == list->size){
		printf("Trying to sort an empty list.\n");
		return;
	}
	//valgrind line 422 not initialized
	node_t *curr_node = list->head;
	node_t *count = list->head;

	int elements = 0;
	int index = 0;

	//counts up the elements in 
	while(!count->is_tail){
		elements++;
		count = count->next;
	}
    do{
		index = 0;

		do{
			//pushes the highest val to the end, and then ignores it.
			if(elements < index){
				while(!curr_node->is_tail){
					curr_node = curr_node->next;
				}
			}
			else{
				if(curr_node == list->compare(curr_node, curr_node->next)){
					//curr_node is node_b before node_a
					curr_node = swap_nodes(curr_node, curr_node->next, list);
				}
				curr_node = curr_node->next;
				index++;
			}	
		}while(!curr_node->is_tail);

		elements--;  
		curr_node = curr_node->next; 
    }while(elements > 0);
}

//sort swap, node_a comes directly before node_b
node_t *swap_nodes(node_t * node_a, node_t * node_b, cd_ll *list){
	//preserving the previous and next nodes
	node_t *tmp_previous = node_a->previous;
	node_t *tmp_next = node_b->next;

	if(NULL == node_a || NULL == node_b){
		printf("Trying to swap when one or both of the nodes is NULL.\n");
		return NULL;
	}

	if(is_head(node_a)){
		node_b->is_head = 1;
		node_a->is_head = 0;
		list->head = node_b;
	}
	if(is_tail(node_b)){
		node_a->is_tail = 1;
		node_b->is_tail = 0;
		list->tail = node_a;
	}

	//(new order) tmp_previous - node_b - node_a - tmp_next 
	link_nodes(tmp_previous, node_b, node_a);
	link_nodes(node_b, node_a, tmp_next);

	return node_b;
}

//unlink the node, makes next and previous of n NULL
void unlink_node(node_t *n, cd_ll *list){
	if(NULL == n){
		printf("Trying to a NULL node.\n");
		return;
	}

	//More than one element
	if(1 < list->size){
		node_t *tmp_previous = n->previous;
		node_t *tmp_next = n->next;
		//update head/tail pointers
		if(is_head(n)){
			tmp_next->is_head = 1;
			n->is_head = 0;
			list->head = tmp_next;
		}	
		if(is_tail(n)){
			tmp_previous->is_tail = 1;
			n->is_tail = 0;
			list->tail = tmp_previous;
		}	

		//linking the previous and next
		tmp_previous->next = tmp_next;
		tmp_next->previous = tmp_previous;

	}
	//single element
	else if(1 == list->size){
		list->head = NULL;
		list->tail = list->head;
	}
	n->next = NULL;
	n->previous = NULL;
}


//linking the nodes
void *link_nodes(node_t *x, node_t *y, node_t *z){
	//all three elements passed in.
	if(NULL != x && NULL != y && NULL != z){
		x->next = y;
		y->previous = x;
		y->next = z;
		z->previous = y;
	}
    //NULL node z (two elements passed in)
    else if(NULL != x && NULL != y && NULL == z){
        x->previous = y;
        x->next = y;
        y->previous = x;
        y->next = x;
    }
    //NULL nodes y & z (one element passed in)
    else if(NULL != x && NULL == y && NULL == z){
        x->previous = x;
        x->next = x;
    }
}

//Checks if node isuser_funche head of the list.
bool is_head(node_t *node){
	return (node->is_head) ? true : false;
}

//Checks if node is the tail of the list.
bool is_tail(node_t *node){
	return (node->is_tail) ? true : false;
}

//Checks if list is empty
bool is_empty(cd_ll *list){
	return (list->size > 0) ? false : true;
}

//function that takes user input and performs specified action on every node
void interative_function(cd_ll *list){
	if(!OUTPUT){
		return;
	}
	node_t *curr_node = list->head;

	if(NULL == curr_node){
		printf("Empty List\n");
	}
	else{
		do{
			list->function(&curr_node);
			curr_node = curr_node->next;
		} while(!is_head(curr_node));
	}
}

//USER DEFINED

//compare x & y. return 'greater' node.
node_t *compare_nodes_int(node_t *x, node_t *y){
	if(*(int*) x->data > *(int*) y->data){
		return x;
	}
	else if(*(int*) y->data > *(int*) x->data){
		return y;
	}
	else{
		return NULL;
	}
}

//print int data type
int print_int(node_t *n){
	return (NULL == n->data) ? -1 : *(int*) n->data;
}

//test for adding 1 to each node in list
void add_one(node_t **n){
	node_t *curr_node = *n;
	
	int *new_data = curr_node->data;
	*new_data = *new_data + 1;
}

//check for null nodes node_connections
//error check everywhere
//test all functions
//how do I free 