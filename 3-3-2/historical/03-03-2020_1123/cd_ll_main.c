#define DEBUG 0
// #define DEBUG 1

#if DEBUG
	#include "src/cd_ll.c"
#else
	#include "cd_ll.h"
	#include "cd_ll_user.h"
	#include "cd_ll_stack_queue.h"
#endif

#define START_SIZE 4

int main(){
	cd_ll *node_list = (cd_ll*)malloc(sizeof(cd_ll));

    init(node_list, &compare_nodes_int, &print_int, &add_one);

	int d = 4;
	int f = 7;
	int c = 3;
	int b = 2;
	int a = 1;
	int e = 6;
	int g = 5;

	node_t *first_node = create_node(&e);
	
	node_t *node_a = create_node(&a);

	prepend_node(node_a, node_list);
	// free(node_a);

	prepend_node(create_node(&b), node_list);
	// prepend_node(create_node(&c), node_list);
	// prepend_node(create_node(&d), node_list);


	// insert_node(create_node(&e), node_list->tail, node_list);
	insert_node(create_node(&f), node_list->head, node_list);

	insert_node(create_node(&g), node_list->tail, node_list);

	// print_tree(node_list);

	sort(node_list);

	// print_tree(node_list);

	insert_node_index(6, first_node, node_list);
	delete_node(node_list->tail->previous, node_list);

	node_t *found_node = find_node(&g, node_list);
	delete_node(found_node, node_list);

	delete_index(0, node_list);
	
	delete_index(4, node_list);

	push(create_node(&c), node_list);
	
	push_back(create_node(&d), node_list);

	node_t *popped_first_node = pop(node_list);
	prepend_node(popped_first_node, node_list);

	insert_node(create_node(&e), node_list->tail, node_list);

	insert_node(create_node(&f), node_list->head, node_list);
		
	node_t *popped_last_node = pop_last(node_list);
	free(popped_last_node);

	print_tree(node_list);

	printf("add one\n");
	interative_function(node_list);

	print_tree(node_list);

	free_cd_ll(node_list);
	
	first_node = NULL;
	node_a = NULL;
	found_node = NULL;
	popped_first_node = NULL;
	popped_last_node = NULL;
}

//How is the data supposed to be input?
//I can't do it dynamically, especially
//because I don't know what data they are 
//giving me.

//if I change c to 9 in the above example,
//any node using that address is also 
//changed.