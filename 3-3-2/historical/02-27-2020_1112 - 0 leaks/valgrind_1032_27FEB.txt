valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes --track-origins=yes -v bin/cd_ll
==5079== Memcheck, a memory error detector
==5079== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==5079== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==5079== Command: bin/cd_ll
==5079== 
--5079-- Valgrind options:
--5079--    --tool=memcheck
--5079--    --leak-check=yes
--5079--    --show-reachable=yes
--5079--    --num-callers=20
--5079--    --track-fds=yes
--5079--    --track-origins=yes
--5079--    -v
--5079-- Contents of /proc/version:
--5079--   Linux version 5.3.0-40-generic (buildd@lcy01-amd64-024) (gcc version 7.4.0 (Ubuntu 7.4.0-1ubuntu1~18.04.1)) #32~18.04.1-Ubuntu SMP Mon Feb 3 14:05:59 UTC 2020
--5079-- 
--5079-- Arch and hwcaps: AMD64, LittleEndian, amd64-cx16-lzcnt-rdtscp-sse3-avx-avx2-bmi
--5079-- Page sizes: currently 4096, max supported 4096
--5079-- Valgrind library directory: /usr/lib/valgrind
--5079-- Reading syms from /home/amy/repos/jqr_2019/build/bin/cd_ll
--5079-- Reading syms from /lib/x86_64-linux-gnu/ld-2.27.so
--5079--   Considering /lib/x86_64-linux-gnu/ld-2.27.so ..
--5079--   .. CRC mismatch (computed 1b7c895e wanted 2943108a)
--5079--   Considering /usr/lib/debug/lib/x86_64-linux-gnu/ld-2.27.so ..
--5079--   .. CRC is valid
--5079-- Reading syms from /usr/lib/valgrind/memcheck-amd64-linux
--5079--   Considering /usr/lib/valgrind/memcheck-amd64-linux ..
--5079--   .. CRC mismatch (computed 41ddb025 wanted 9972f546)
--5079--    object doesn't have a symbol table
--5079--    object doesn't have a dynamic symbol table
--5079-- Scheduler: using generic scheduler lock implementation.
--5079-- Reading suppressions file: /usr/lib/valgrind/default.supp
==5079== embedded gdbserver: reading from /tmp/vgdb-pipe-from-vgdb-to-5079-by-amy-on-???
==5079== embedded gdbserver: writing to   /tmp/vgdb-pipe-to-vgdb-from-5079-by-amy-on-???
==5079== embedded gdbserver: shared mem   /tmp/vgdb-pipe-shared-mem-vgdb-5079-by-amy-on-???
==5079== 
==5079== TO CONTROL THIS PROCESS USING vgdb (which you probably
==5079== don't want to do, unless you know exactly what you're doing,
==5079== or are doing some strange experiment):
==5079==   /usr/lib/valgrind/../../bin/vgdb --pid=5079 ...command...
==5079== 
==5079== TO DEBUG THIS PROCESS USING GDB: start GDB like this
==5079==   /path/to/gdb bin/cd_ll
==5079== and then give GDB the following command
==5079==   target remote | /usr/lib/valgrind/../../bin/vgdb --pid=5079
==5079== --pid is optional if only one valgrind process is running
==5079== 
--5079-- REDIR: 0x401f2f0 (ld-linux-x86-64.so.2:strlen) redirected to 0x580608c1 (???)
--5079-- REDIR: 0x401f0d0 (ld-linux-x86-64.so.2:index) redirected to 0x580608db (???)
--5079-- Reading syms from /usr/lib/valgrind/vgpreload_core-amd64-linux.so
--5079--   Considering /usr/lib/valgrind/vgpreload_core-amd64-linux.so ..
--5079--   .. CRC mismatch (computed 50df1b30 wanted 4800a4cf)
--5079--    object doesn't have a symbol table
--5079-- Reading syms from /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so
--5079--   Considering /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so ..
--5079--   .. CRC mismatch (computed f893b962 wanted 95ee359e)
--5079--    object doesn't have a symbol table
==5079== WARNING: new redirection conflicts with existing -- ignoring it
--5079--     old: 0x0401f2f0 (strlen              ) R-> (0000.0) 0x580608c1 ???
--5079--     new: 0x0401f2f0 (strlen              ) R-> (2007.0) 0x04c32db0 strlen
--5079-- REDIR: 0x401d360 (ld-linux-x86-64.so.2:strcmp) redirected to 0x4c33ee0 (strcmp)
--5079-- REDIR: 0x401f830 (ld-linux-x86-64.so.2:mempcpy) redirected to 0x4c374f0 (mempcpy)
--5079-- Reading syms from /lib/x86_64-linux-gnu/libc-2.27.so
--5079--   Considering /lib/x86_64-linux-gnu/libc-2.27.so ..
--5079--   .. CRC mismatch (computed b1c74187 wanted 042cc048)
--5079--   Considering /usr/lib/debug/lib/x86_64-linux-gnu/libc-2.27.so ..
--5079--   .. CRC is valid
--5079-- REDIR: 0x4edac70 (libc.so.6:memmove) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9d40 (libc.so.6:strncpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edaf50 (libc.so.6:strcasecmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9790 (libc.so.6:strcat) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9d70 (libc.so.6:rindex) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edc7c0 (libc.so.6:rawmemchr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edade0 (libc.so.6:mempcpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edac10 (libc.so.6:bcmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9d00 (libc.so.6:strncmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9800 (libc.so.6:strcmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edad40 (libc.so.6:memset) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ef80f0 (libc.so.6:wcschr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9ca0 (libc.so.6:strnlen) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9870 (libc.so.6:strcspn) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edafa0 (libc.so.6:strncasecmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9840 (libc.so.6:strcpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edb0e0 (libc.so.6:memcpy@@GLIBC_2.14) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9da0 (libc.so.6:strpbrk) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed97c0 (libc.so.6:index) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ed9c70 (libc.so.6:strlen) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ee46c0 (libc.so.6:memrchr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edaff0 (libc.so.6:strcasecmp_l) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edabe0 (libc.so.6:memchr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4ef8eb0 (libc.so.6:wcslen) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4eda050 (libc.so.6:strspn) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edaf20 (libc.so.6:stpncpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edaef0 (libc.so.6:stpcpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edc7f0 (libc.so.6:strchrnul) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4edb040 (libc.so.6:strncasecmp_l) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--5079-- REDIR: 0x4fca3c0 (libc.so.6:__strrchr_avx2) redirected to 0x4c32730 (rindex)
--5079-- REDIR: 0x4ed3070 (libc.so.6:malloc) redirected to 0x4c2faa0 (malloc)
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x108D2F: prepend_node (cd_ll.c:134)
==5079==    by 0x1088AF: main (cd_ll_main.c:25)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x1091EB: empty_list (cd_ll.c:287)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x10903A: delete_node (cd_ll.c:233)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
--5079-- REDIR: 0x4fca1d0 (libc.so.6:__strchrnul_avx2) redirected to 0x4c37020 (strchrnul)

--5079-- REDIR: 0x4fb9100 (libc.so.6:__strncpy_ssse3) redirected to 0x4c32fb0 (strncpy)
--5079-- REDIR: 0x4fca590 (libc.so.6:__strlen_avx2) redirected to 0x4c32cf0 (strlen)
--5079-- REDIR: 0x4fcaab0 (libc.so.6:__mempcpy_avx_unaligned_erms) redirected to 0x4c37130 (mempcpy)
Delete Tail: Node: (1)
Previous (Head): (1)
Next (Tail): (1)

==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x109676: unlink_node (cd_ll.c:463)
==5079==    by 0x1090AB: delete_node (cd_ll.c:246)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x109719: unlink_node (cd_ll.c:484)
==5079==    by 0x1090AB: delete_node (cd_ll.c:246)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
--5079-- REDIR: 0x4ed3950 (libc.so.6:free) redirected to 0x4c30cd0 (free)
==5079== Invalid write of size 8
==5079==    at 0x1089EA: free_node (cd_ll.c:40)
==5079==    by 0x1090BB: delete_node (cd_ll.c:247)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Address 0x522d0b8 is 8 bytes inside a block of size 32 free'd
==5079==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x1089E5: free_node (cd_ll.c:39)
==5079==    by 0x1090BB: delete_node (cd_ll.c:247)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Block was alloc'd at
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x108A1D: create_node (cd_ll.c:47)
==5079==    by 0x108898: main (cd_ll_main.c:23)
==5079== 
==5079== Invalid write of size 8
==5079==    at 0x1089F6: free_node (cd_ll.c:41)
==5079==    by 0x1090BB: delete_node (cd_ll.c:247)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Address 0x522d0b0 is 0 bytes inside a block of size 32 free'd
==5079==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x1089E5: free_node (cd_ll.c:39)
==5079==    by 0x1090BB: delete_node (cd_ll.c:247)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Block was alloc'd at
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x108A1D: create_node (cd_ll.c:47)
==5079==    by 0x108898: main (cd_ll_main.c:23)
==5079== 
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x10921C: empty_list (cd_ll.c:293)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x10898F: free_cd_ll (cd_ll.c:23)
==5079==    by 0x1088C7: main (cd_ll_main.c:70)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
List Freed
==5079== 
==5079== FILE DESCRIPTORS: 3 open at exit.
==5079== Open file descriptor 2: /dev/pts/2
==5079==    <inherited from parent>
==5079== 
==5079== Open file descriptor 1: /dev/pts/2
==5079==    <inherited from parent>
==5079== 
==5079== Open file descriptor 0: /dev/pts/2
==5079==    <inherited from parent>
==5079== 
==5079== 
==5079== HEAP SUMMARY:
==5079==     in use at exit: 0 bytes in 0 blocks
==5079==   total heap usage: 3 allocs, 3 frees, 1,096 bytes allocated
==5079== 
==5079== All heap blocks were freed -- no leaks are possible
==5079== 
==5079== ERROR SUMMARY: 9 errors from 9 contexts (suppressed: 0 from 0)
==5079== 
==5079== 1 errors in context 1 of 9:
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x10898F: free_cd_ll (cd_ll.c:23)
==5079==    by 0x1088C7: main (cd_ll_main.c:70)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== 
==5079== 1 errors in context 2 of 9:
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x10921C: empty_list (cd_ll.c:293)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== 
==5079== 1 errors in context 3 of 9:
==5079== Invalid write of size 8
==5079==    at 0x1089F6: free_node (cd_ll.c:41)
==5079==    by 0x1090BB: delete_node (cd_ll.c:247)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Address 0x522d0b0 is 0 bytes inside a block of size 32 free'd
==5079==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x1089E5: free_node (cd_ll.c:39)
==5079==    by 0x1090BB: delete_node (cd_ll.c:247)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Block was alloc'd at
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x108A1D: create_node (cd_ll.c:47)
==5079==    by 0x108898: main (cd_ll_main.c:23)
==5079== 
==5079== 
==5079== 1 errors in context 4 of 9:
==5079== Invalid write of size 8
==5079==    at 0x1089EA: free_node (cd_ll.c:40)
==5079==    by 0x1090BB: delete_node (cd_ll.c:247)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Address 0x522d0b8 is 8 bytes inside a block of size 32 free'd
==5079==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x1089E5: free_node (cd_ll.c:39)
==5079==    by 0x1090BB: delete_node (cd_ll.c:247)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Block was alloc'd at
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x108A1D: create_node (cd_ll.c:47)
==5079==    by 0x108898: main (cd_ll_main.c:23)
==5079== 
==5079== 
==5079== 1 errors in context 5 of 9:
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x109719: unlink_node (cd_ll.c:484)
==5079==    by 0x1090AB: delete_node (cd_ll.c:246)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== 
==5079== 1 errors in context 6 of 9:
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x109676: unlink_node (cd_ll.c:463)
==5079==    by 0x1090AB: delete_node (cd_ll.c:246)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== 
==5079== 1 errors in context 7 of 9:
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x10903A: delete_node (cd_ll.c:233)
==5079==    by 0x10920D: empty_list (cd_ll.c:292)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== 
==5079== 1 errors in context 8 of 9:
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x1091EB: empty_list (cd_ll.c:287)
==5079==    by 0x1088BB: main (cd_ll_main.c:69)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== 
==5079== 1 errors in context 9 of 9:
==5079== Conditional jump or move depends on uninitialised value(s)
==5079==    at 0x108D2F: prepend_node (cd_ll.c:134)
==5079==    by 0x1088AF: main (cd_ll_main.c:25)
==5079==  Uninitialised value was created by a heap allocation
==5079==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5079==    by 0x10883A: main (cd_ll_main.c:9)
==5079== 
==5079== ERROR SUMMARY: 9 errors from 9 contexts (suppressed: 0 from 0)

