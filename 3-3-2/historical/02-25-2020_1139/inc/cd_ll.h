#ifndef CD_LL_HEADER_LHOMMEDIEU
#define CD_LL_HEADER_LHOMMEDIEU

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct node {

	struct node *next;
 	struct node *previous;

	int is_head;
	int is_tail;

	void *data;

} node_t; 

typedef struct circular_doubly_linked_list {

	struct node *head;
    struct node *tail;

	node_t *(*compare)(node_t *x, node_t *y);
	int (*print)(node_t *n);

} cd_ll;

//create a list with x nodes
void init(cd_ll **list, node_t *(*compare)(node_t *x, node_t *y), int (*print)(node_t *n)); //cd_ll
//free the circular-doubly linked list
void free_cd_ll(cd_ll *list);
//free the node
void free_node(node_t *x);
//malloc space for node
node_t *create_node(void *data);

//print current node
void print_node(node_t *n, cd_ll list);
//print list
void print_list(cd_ll **list);
//print whole list in a pretty way.
void print_tree(cd_ll **list);

//add element to head
void prepend_node(node_t *node, cd_ll **list);
//insert element after node_t *n
void insert_node(node_t *node, node_t **n, cd_ll **list);

//delete nodes
void delete_node(node_t **n, cd_ll **list);
//empty the list
void empty_list(cd_ll **list);

//pop the top element
node_t *pop_first(cd_ll **list);
//peek/print at the top element without removing
node_t *peek_first(cd_ll **list);
//pop the bottom/last element
node_t *pop_last(cd_ll **list);
//peek/print at the bottom/last element without removing
node_t *peek_last(cd_ll **list);
//push node to front
node_t *push_front(cd_ll **list);
//push node to back
node_t *push_back(cd_ll **list);

//find first match in list 
node_t *find_node(void *val, cd_ll **list);

//sort according to compare function.
void sort(cd_ll **list);
//unlink the node
node_t *unlink_node(node_t *n, cd_ll **list);
//linking the nodes
node_t *link_nodes(node_t *x, node_t *y, node_t *z, cd_ll **list);
//sort swap
node_t *swap_nodes(node_t * node_a, node_t * node_b, cd_ll **list);

#endif