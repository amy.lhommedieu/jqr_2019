#!/bin/bash

if [ $1 == 'clean' ]
then
	rm -rf build
else
	if [ ! -d build ]
	then
		mkdir build
	fi

	cd build && \
	cmake ../ && \
	make && \
	cd ..

	#will work if the cMakeLists.txt has an output
	#directory of bin.
	if [ ! -d bin ] && [ -d build/bin ]
	then
		ln -s build/bin ./bin
	fi
fi

date=$(date +%m-%d-%Y_%H%M)

if [ -d 3-3-2/$date ]
	then
		rm -rf 3-3-2/$date
fi

mkdir 3-3-2/$date
mkdir 3-3-2/$date/src
mkdir 3-3-2/$date/inc

cp inc/cd_ll* 3-3-2/$date/inc
cp src/cd_ll* 3-3-2/$date/src
cp cd_ll* 3-3-2/$date
cp CMakeLists.txt 3-3-2/$date

bin/cd_ll