#!/bin/bash

if [ $1 == 'clean' ]
then
	rm -rf build
else
	if [ ! -d build ]
	then
		mkdir build
	fi

	cd build && \
	cmake ../ && \
	make && \
	cd ..

	#will work if the cMakeLists.txt has an output
	#directory of bin.
	if [ ! -d bin ] && [ -d build/bin ]
	then
		ln -s build/bin ./bin
	fi
fi
