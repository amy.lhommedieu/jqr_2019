#define MAX_NODES 5 //max nodes
#define MAX_EDGES 5 //max edges per node
#define MULTI_DIRECTIONAL 0 //0 means edges go both ways
#define VERBOSE 0 //bigger output on printing.

#define DEBUG 1
// #define DEBUG 1

#if DEBUG
	#include "src/cd_ll.c"
	#include "src/3_1_16.c"
#else
	#include "cd_ll.h"
	#include "3_1_16.h"
#endif

void main(){

	int graph[MAX_NODES][MAX_NODES];

	int num_nodes = 0;
	gnode_t *gnode_list = (gnode_t *) malloc(sizeof(num_nodes) * sizeof(gnode_t));
	init_graph(MAX_NODES, graph);


	//node 1
	gnode_t *gnode_1 = malloc(sizeof(gnode_t));
	create_gnode(gnode_1, 1, 0);
	insert_gnode(gnode_1, gnode_list, &num_nodes);


	//node 2
	gnode_t *gnode_2 = malloc(sizeof(gnode_t));
	create_gnode(gnode_2, 2, __INT16_MAX__);
	insert_gnode(gnode_2, gnode_list, &num_nodes);


	//node 3
	gnode_t *gnode_3 = malloc(sizeof(gnode_t));
	create_gnode(gnode_3, 3, __INT16_MAX__);
	insert_gnode(gnode_3, gnode_list, &num_nodes);


	//node 4
	gnode_t *gnode_4 = malloc(sizeof(gnode_t));
	create_gnode(gnode_4, 4, __INT16_MAX__);
	insert_gnode(gnode_4, gnode_list, &num_nodes);

	/*####################################*/
	
	print_gnodes(gnode_list, num_nodes);

	insert_edge(gnode_1, gnode_3, 2, graph);

	insert_edge(gnode_1, gnode_2, 4, graph);

	insert_edge(gnode_2, gnode_4, 1, graph);

	insert_edge(gnode_3, gnode_4, 4, graph);

	sort_array(gnode_2->neighbors, gnode_2->neighbor_weights, gnode_2->neighbor_gnodes, 2);

	for (int i = 0; i < 2; ++i) {
		printf("(Neighbor ID: %d, Weight: %d)\n", gnode_2->neighbors[i], gnode_2->neighbor_weights[i]);
	}

	print_graph(graph);

	// int dist = shortest_path(node_1, node_4);

    return;
}



    /*
	typedef struct edge{
		int id;
	} edge_t;

	edge_t a;
	a.id = 1;

	//-(3)--(7)--(2)--(6)--(4)-
	
	node_t *node_a = create_node(&a);
	printf("%d\n", ((edge_t*)(node_a->data))->id);

	*/
