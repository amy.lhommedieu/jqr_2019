#ifndef CD_LL_HEADER_LHOMMEDIEU
#define CD_LL_HEADER_LHOMMEDIEU

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define OUTPUT 1

typedef struct node {

	struct node *next;
 	struct node *previous;

	int is_head;
	int is_tail;

	void *data;

} node_t; 

typedef struct circular_doubly_linked_list {

	struct node *head;
    struct node *tail;
	int size;

	node_t *(*compare)(node_t *x, node_t *y);
	int (*print)(node_t *n);
	void (*function)(node_t **n); /*add file desciptor optional parameter*/

} cd_ll;

//create a list with x nodes
void init(cd_ll *list, node_t *(*user_compare)(node_t *x, node_t *y), int (*user_print)(node_t *n), void (*user_function)); //cd_ll

//free the circular-doubly linked list
void free_cd_ll(cd_ll *list);

//free the node
void free_node(node_t *x);

//malloc space for node
node_t *create_node(void *data);

//print current node
void print_node(node_t *n, cd_ll list);

//print list
void print_list(cd_ll *list);

//print whole list in a pretty way.
void print_tree(cd_ll *list);

//add element to head
void prepend_node(node_t *node, cd_ll *list);

//add element to tail
void append_node(node_t *node, cd_ll *list);

//insert element after node_t *n
void insert_node(node_t *new_node, node_t *old_node, cd_ll *list);

//insert element by index number
void insert_node_index(int index, node_t *n, cd_ll *list);

//delete node, need to use find_node unless using head or tail
void delete_node(node_t *n, cd_ll *list);

//delete node at index
void delete_index(int index, cd_ll *list);

//empty the list
void empty_list(cd_ll *list);

//find first match in list 
node_t *find_node(void *val, cd_ll *list);

//sort according to compare function.
void sort(cd_ll *list);

//sort swap
node_t *swap_nodes(node_t * node_a, node_t * node_b, cd_ll *list);

//unlink the node
void unlink_node(node_t *n, cd_ll *list);

//linking the nodes circularly. Passing in two means they point to each other circularly.
void *link_nodes(node_t *x, node_t *y, node_t *z);

//Checks if node is the head of the list.
bool is_head(node_t *node);

//Checks if node is the tail of the list.
bool is_tail(node_t *node);

//Checks if list is empty
bool is_empty(cd_ll *list);

//function that takes user input and performs specified action on every node
void interative_function(cd_ll *list);

//changes the value of the data
void change_data(node_t **n, void *new_data);

#endif