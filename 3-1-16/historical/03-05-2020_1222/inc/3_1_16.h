#ifndef GRAPH_HEADER_LHOMMEDIEU
#define GRAPH_HEADER_LHOMMEDIEU

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEBUG 0
// #define DEBUG 1

#if DEBUG
	#include "../inc/cd_ll.h"
	#include "../inc/cd_ll_user.h"
	#include "../inc/cd_ll_stack_queue.h"
#else
	#include "cd_ll.h"
	#include "cd_ll_user.h"
	#include "cd_ll_stack_queue.h"
#endif

#define NUM_NODES 5
#define NUM_EDGES 5
#define MULTI_DIRECTIONAL 0


typedef struct Edge {
    int weight;
    int start_node_id;
    int end_node_id;
} edge_t;

typedef struct Graph_Node {
    int nodeID;
    int num_edges; //trackes edges so you know where to insert in edges[]
    struct Edge edges[NUM_EDGES];
} graph_node_t;


//graph: weighted and non-directional
int graph[NUM_NODES][NUM_NODES];
//TODO make dynamic- https://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/

graph_node_t nodes[NUM_NODES];


//initalizes the graph with all "empty" edges(-1).
void init_graph(int i);

//populates the graph with num random edges. 
void random_edges(int num);

//creates an edge and returns it
edge_t create_edge(int start, int end, int weight);
//graph_node_t create_node(graph_node_t a);

//inserts the edge to the graph, still needs to add it to the edges[]
void insert_edge(edge_t curr_edge);

//deletes the edge from the graph, will eventually also delete from edges[]
edge_t delete_edge(edge_t curr_edge);

//graph_node_t delete_node(graph_node_t a);

//checks if an edge between two nodes exists.
int is_edge(graph_node_t a, graph_node_t b);

//prints the graph in format 'graph[4][0] = 3'
void print_graph();

//more descriptive printing of the node. Shows node id and num of edges to/from
graph_node_t print_graph_node(graph_node_t my_node);

//prints the edge and what nodes it connects at which weight
void print_edge(edge_t my_edge);

//https://brilliant.org/wiki/dijkstras-short-path-finder/
//algorithm for dijkstra's
int shortest_path(node_t *start, node_t *end, cd_ll *list);

#endif