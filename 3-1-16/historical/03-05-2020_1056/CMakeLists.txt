cmake_minimum_required(VERSION 3.10)
project(JQR VERSION 1.0)
include_directories(inc)
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin) #is relative to build directory
set(CMAKE_BUILD_TYPE Debug)

set(GRAPH
	src/3_1_16.c
)

set(LIST
	src/cd_ll.c
)
	
add_executable(3_1_16 "3_1_16_main.c" ${GRAPH} ${LIST})
