#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEBUG 0
// #define DEBUG 1

#if DEBUG
	#include "../inc/3_1_16.h"
	#include "../3-3-2/inc/cd_ll.h"
#else
	#include "3_1_16.h"
	#include "../../3-3-2/inc/cd_ll.h"
#endif


//graph: weighted and non-directional
int graph[NUM_NODES][NUM_NODES];
//TODO make dynamic- https://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/

graph_node_t nodes[NUM_NODES];


//initalizes the graph with all "empty" edges(-1).
void init_graph(int i){
    for(int x = 0; x < i; x++){
        for(int y = 0; y < i; y++){
            //-1 means no edge/node
            graph[x][y] = -1;
        }
    }
}

//populates the graph with num random edges. 
void random_edges(int num){
    srand(time(NULL)); //makes it random
    for(int i = 0; i < num; i++){
        int start_node_id = (rand() % (NUM_NODES - 0 + 1)) + 0; 
        int end_node_id = (rand() % (NUM_NODES - 0 + 1)) + 0;  
        int weight = (rand() % (10 - 0 + 1)) + 0; 
        
        //checks if its a path to itself or if it has already has an edge on that path.
        if(start_node_id != end_node_id && graph[start_node_id][end_node_id] == -1){
            edge_t new_edge = create_edge(start_node_id,end_node_id,weight);
            insert_edge(new_edge);

            int *start_num_edges = NULL;
            start_num_edges = &nodes[start_node_id].num_edges;
            nodes[start_node_id].edges[*start_num_edges] = new_edge; //insert into start Node edge array
            *start_num_edges = *start_num_edges + 1; //change the struct nodes number of edges
            
            if(MULTI_DIRECTIONAL){
                int *end_num_edges = NULL;
                end_num_edges = &nodes[end_node_id].num_edges;
                nodes[end_node_id].edges[*end_num_edges] = new_edge; //insert into start Node edge array
                *end_num_edges = *end_num_edges + 1; //change the struct nodes number of edges
            }
        }
        //decrement to replace the bad edge
        else{
            i--;
        }
    }
}

//creates an edge and returns it
edge_t create_edge(int start, int end, int weight){
    edge_t curr_edge;
    curr_edge.start_node_id = start;
    curr_edge.end_node_id = end;
    curr_edge.weight = weight;

    return curr_edge;
}
//graph_node_t create_node(graph_node_t a);

//inserts the edge to the graph, still needs to add it to the edges[]
void insert_edge(edge_t curr_edge){
    //TODO add to edges[]
    
    graph[curr_edge.start_node_id][curr_edge.end_node_id] = curr_edge.weight;
    
    if(MULTI_DIRECTIONAL){
        graph[curr_edge.end_node_id][curr_edge.start_node_id] = curr_edge.weight; //makes it multi-directional
    }
    //printf("graph[%d][%d] = %d\n", curr_edge.start_node_id, curr_edge.end_node_id, graph[curr_edge.start_node_id][curr_edge.end_node_id]);
}

//deletes the edge from the graph, will eventually also delete from edges[]
edge_t delete_edge(edge_t curr_edge){
    //TODO remove from edges[]

    graph[curr_edge.start_node_id][curr_edge.end_node_id] = -1;
}

//graph_node_t delete_node(graph_node_t a);

//checks if an edge between two nodes exists.
int is_edge(graph_node_t a, graph_node_t b){
    if(graph[a.nodeID][b.nodeID] == -1){
        printf("No edge\n");
        return -1;
    }
    else {
        printf("Edge of weight %d\n", graph[a.nodeID][b.nodeID]);
        return graph[a.nodeID][b.nodeID];
    }
}

//prints the graph in format 'graph[4][0] = 3'
void print_graph(){
    for(int x = 0; x < NUM_NODES; x++){
        for(int y = 0; y < NUM_NODES; y++){
            if(graph[x][y] != -1){
                printf("graph[%d][%d] = %d\n", x, y, graph[x][y]);
            }
        }
    }
}

//more descriptive printing of the node. Shows node id and num of edges to/from
graph_node_t print_graph_node(graph_node_t my_node){
    printf("My node ID: %d\n", my_node.nodeID);
    printf("Number of edges from this node: %d\n", my_node.num_edges);
    for(int x = 0; x < my_node.num_edges; x++){
        print_edge(my_node.edges[x]);
    }
}

//prints the edge and what nodes it connects at which weight
void print_edge(edge_t my_edge){
    printf("Edge from Node%d to Node%d has a weight of %d\n", 
        my_edge.start_node_id, my_edge.end_node_id, my_edge.weight);
}

//https://brilliant.org/wiki/dijkstras-short-path-finder/
//algorithm for dijkstra's
int shortest_path(int a, int b){
    //intialize distance array
    int dist[NUM_NODES];

    //every node distance is infinity except source
    for(int i = 0; i < NUM_NODES; i++){
        dist[i] = __INT32_MAX__;
    }
    dist[a] = 0;

    //create a queue of all nodes
    
}