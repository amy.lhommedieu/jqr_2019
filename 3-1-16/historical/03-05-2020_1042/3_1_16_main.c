#define NUM_NODES 5
#define NUM_EDGES 5
#define MULTI_DIRECTIONAL 0

#define DEBUG 0
// #define DEBUG 1

#if DEBUG
	#include "src/3_1_16.c"
	#include "../3-3-2/src/cd_ll.c"
#else
	#include "3_1_16.h"
	#include "../../3-3-2/inc/cd_ll.h"
#endif

void main(){
    init_graph(NUM_NODES);

    for(int i = 0; i < NUM_NODES; i++){
        nodes[i].nodeID = i;
        nodes[i].num_edges = 0;
    }

    random_edges(NUM_EDGES);

    print_graph();

    for(int i = 0; i < NUM_NODES; i++){
        print_graph_node(nodes[i]);
    }

    //is_edge(nodes[0], nodes[3]);

    return;
}