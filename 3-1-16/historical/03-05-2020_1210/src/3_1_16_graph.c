#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_NODES 5
#define NUM_EDGES 5

#define MULTI_DIRECTIONAL 0

struct Edge {
    int weight;
    int start_node_id;
    int end_node_id;
};

struct Node {
    int nodeID;
    int num_edges; //trackes edges so you know where to insert in edges[]
    struct Edge edges[NUM_EDGES];
};

//graph: weighted and non-directional
int graph[NUM_NODES][NUM_NODES];
//TODO make dynamic- https://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/

struct Node nodes[NUM_NODES];

/*Function Declarations*/
void init_graph(int i);
//void delete_node(struct Node a);
void random_edges(int num);
struct Edge create_edge(int start, int end, int weight);
void insert_edge(struct Edge curr_edge);
void delete_edge(struct Edge curr_edge);
int is_edge(struct Node a, struct Node b);
//int is_node(struct Node a); //checks if there are any connections
void print_graph();
void print_node(struct Node my_node);
void print_edge(struct Edge my_edge);
int shortest_path(int a, int b);


void main(){
    init_graph(NUM_NODES);

    for(int i = 0; i < NUM_NODES; i++){
        nodes[i].nodeID = i;
        nodes[i].num_edges = 0;
    }

    random_edges(NUM_EDGES);

    print_graph();

    for(int i = 0; i < NUM_NODES; i++){
        print_node(nodes[i]);
    }

    //is_edge(nodes[0], nodes[3]);
}


/*FUNCTIONS*/

void init_graph(int i){
    for(int x = 0; x < i; x++){
        for(int y = 0; y < i; y++){
            //-1 means no edge/node
            graph[x][y] = -1;
        }
    }
}

void random_edges(int num){
    srand(time(NULL)); //makes it random
    for(int i = 0; i < num; i++){
        int start_node_id = (rand() % (NUM_NODES - 0 + 1)) + 0; 
        int end_node_id = (rand() % (NUM_NODES - 0 + 1)) + 0;  
        int weight = (rand() % (10 - 0 + 1)) + 0; 
        
        //checks if its a path to itself or if it has already has an edge on that path.
        if(start_node_id != end_node_id && graph[start_node_id][end_node_id] == -1){
            struct Edge new_edge = create_edge(start_node_id,end_node_id,weight);
            insert_edge(new_edge);

            int *start_num_edges = NULL;
            start_num_edges = &nodes[start_node_id].num_edges;
            nodes[start_node_id].edges[*start_num_edges] = new_edge; //insert into start Node edge array
            *start_num_edges = *start_num_edges + 1; //change the struct nodes number of edges
            
            if(MULTI_DIRECTIONAL){
                int *end_num_edges = NULL;
                end_num_edges = &nodes[end_node_id].num_edges;
                nodes[end_node_id].edges[*end_num_edges] = new_edge; //insert into start Node edge array
                *end_num_edges = *end_num_edges + 1; //change the struct nodes number of edges
            }
        }
        //decrement to replace the bad edge
        else{
            i--;
        }
    }
}

struct Edge create_edge(int start, int end, int weight){
    struct Edge curr_edge;
    curr_edge.start_node_id = start;
    curr_edge.end_node_id = end;
    curr_edge.weight = weight;

    return curr_edge;
}

void insert_edge(struct Edge curr_edge){
    //TODO add to edges[]
    
    graph[curr_edge.start_node_id][curr_edge.end_node_id] = curr_edge.weight;
    
    if(MULTI_DIRECTIONAL){
        graph[curr_edge.end_node_id][curr_edge.start_node_id] = curr_edge.weight; //makes it multi-directional
    }
    //printf("graph[%d][%d] = %d\n", curr_edge.start_node_id, curr_edge.end_node_id, graph[curr_edge.start_node_id][curr_edge.end_node_id]);
}

void delete_edge(struct Edge curr_edge){
    //TODO remove from edges[]

    graph[curr_edge.start_node_id][curr_edge.end_node_id] = -1;
}

int is_edge(struct Node a, struct Node b){
    if(graph[a.nodeID][b.nodeID] == -1){
        printf("No edge\n");
        return -1;
    }
    else {
        printf("Edge of weight %d\n", graph[a.nodeID][b.nodeID]);
        return graph[a.nodeID][b.nodeID];
    }
}

void print_graph(){
    for(int x = 0; x < NUM_NODES; x++){
        for(int y = 0; y < NUM_NODES; y++){
            if(graph[x][y] != -1){
                printf("graph[%d][%d] = %d\n", x, y, graph[x][y]);
            }
        }
    }
}

void print_node(struct Node my_node){
    printf("My node ID: %d\n", my_node.nodeID);
    printf("Number of edges from this node: %d\n", my_node.num_edges);
    for(int x = 0; x < my_node.num_edges; x++){
        print_edge(my_node.edges[x]);
    }
}

void print_edge(struct Edge my_edge){
    printf("Edge from Node%d to Node%d has a weight of %d\n", 
        my_edge.start_node_id, my_edge.end_node_id, my_edge.weight);
}

//https://brilliant.org/wiki/dijkstras-short-path-finder/
int shortest_path(int a, int b){
    //intialize distance array
    int dist[NUM_NODES];

    //every node distance is infinity except source
    for(int i = 0; i < NUM_NODES; i++){
        dist[i] = __INT32_MAX__;
    }
    dist[a] = 0;

    //create a queue of all nodes
    
}