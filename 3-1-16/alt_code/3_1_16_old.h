#ifndef GRAPH_HEADER_LHOMMEDIEU
#define GRAPH_HEADER_LHOMMEDIEU

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEBUG 0
// #define DEBUG 1

#if DEBUG
	#include "../inc/cd_ll.h"
	#include "../inc/cd_ll_user.h"
	#include "../inc/cd_ll_stack_queue.h"
#else
	#include "cd_ll.h"
	#include "cd_ll_user.h"
	#include "cd_ll_stack_queue.h"
#endif


typedef struct Graph_Node {
    int nodeID; //single char
    struct Graph_Node *previous;
} gnode_t;

typedef struct Edge {
    int weight;
    gnode_t *node_a;
    gnode_t *node_b;
} edge_t;

typedef struct Weighted_Graph{
    gnode_t *gnode_list;
    int num_gnodes; //number of nodes in weighted graph
    edge_t *edge_list;
    int num_edges; //number of edges in weighted graph
    int *graph[][];
} wg_t;


//initalizes the graph with all "empty" edges(-1).
wg_t *init_graph(wg_t *list, int max_nodes, int max_edges);

//creates an edge and returns it
void create_edge(gnode_t *node_a, gnode_t *node_b, int weight, wg_t *master);

//creates a node with a integer id
void create_gnode(int id, wg_t *master);

//inserts the edge to the graph and edge list
void insert_edge(edge_t *curr_edge, wg_t *master);

//populates the graph with num random edges. 
void random_edges(int num, wg_t *master);

//deletes the edge from the graph and edge list
edge_t *delete_edge(edge_t *curr_edge, wg_t *master);

//deletes the node and any edges between gnode_t a and other nodes
gnode_t *delete_gnode(gnode_t *a, wg_t *master);

//checks if an edge between two nodes exists.
int is_edge(gnode_t a, gnode_t b);

//returns the edge if it exists
edge_t *find_edge(gnode_t a, gnode_t b, wg_t *master);

//prints the graph in format 'graph[4][0] = 3'
void print_graph_distance(wg_t *master);

//more descriptive printing of the node. Shows node id
void print_gnode(gnode_t my_node);

//prints the edge and what nodes it connects at which weight
void print_edge(edge_t my_edge);

//prints the graph in format 'graph[4][0] = 3'
void print_gnode_distance(gnode_t a, gnode_t b, wg_t *master);

//https://brilliant.org/wiki/dijkstras-short-path-finder/
//algorithm for dijkstra's
int shortest_path(gnode_t *start, gnode_t *end, wg_t *master);

#endif

