#define NUM_NODES 5 //max nodes
#define NUM_EDGES 5 //max edges per node
#define MULTI_DIRECTIONAL 0 //0 means edges go both ways

#define DEBUG 1
// #define DEBUG 1

#if DEBUG
	#include "src/cd_ll.c"
	#include "src/3_1_16.c"
#else
	#include "cd_ll.h"
	#include "3_1_16.h"
#endif

void main(){

	cd_ll *node_list;
	init(node_list, &compare_gnode, &print_gnode, NULL);

	//node 1
	int neighbors_1[] = {2,3};
	int weights_1[] = {4,2};

	gnode_t gnode_1 = create_gnode(1, 0, -1, -1, neighbors_1, weights_1);

	node_t *node_1 = create_node(&gnode_1);
	cd_ll *node_1_neighbors;


	//node 2
	int neighbors_2[] = {1,4};
	int weights_2[] = {4,1};

	gnode_t gnode_2 = create_gnode(2, __INT16_MAX__, -1, -1, neighbors_2, weights_2);

	node_t *node_2 = create_node(&gnode_2);
	cd_ll *node_2_neighbors;


	//node 3
	int neighbors_3[] = {1,4};
	int weights_3[] = {2,4};

	gnode_t gnode_3 = create_gnode(3, __INT16_MAX__, -1, -1, neighbors_3, weights_3);

	node_t *node_3 = create_node(&gnode_3);
	cd_ll *node_3_neighbors;


	//node 4
	int neighbors_4[] = {2,3};
	int weights_4[] = {1,4};

	gnode_t gnode_4 = create_gnode(4, __INT16_MAX__, -1, -1, neighbors_4, weights_4);

	node_t *node_4 = create_node(&gnode_4);
	cd_ll *node_4_neighbors;

	node_t *curr_node = node_list->head;
	node_t *neighbor = NULL;

	//Node 1 Neighbors
	prepend_node(node_2, node_1_neighbors);
	prepend_node(node_3, node_1_neighbors);
	print_tree(node_1_neighbors);

	//Node 2 Neighbors
	prepend_node(node_1, node_2_neighbors);
	prepend_node(node_4, node_2_neighbors);
	print_tree(node_2_neighbors);

	//Node 3 Neighbors
	prepend_node(node_1, node_3_neighbors);
	prepend_node(node_4, node_3_neighbors);
	print_tree(node_3_neighbors);

	//Node 4 Neighbors
	prepend_node(node_2, node_4_neighbors);
	prepend_node(node_3, node_4_neighbors);
	print_tree(node_4_neighbors);

	print_tree(node_1_neighbors);


	int dist = shortest_path(node_1, node_4);

    return;
}



    /*
	typedef struct edge{
		int id;
	} edge_t;

	edge_t a;
	a.id = 1;

	//-(3)--(7)--(2)--(6)--(4)-
	
	node_t *node_a = create_node(&a);
	printf("%d\n", ((edge_t*)(node_a->data))->id);

	*/