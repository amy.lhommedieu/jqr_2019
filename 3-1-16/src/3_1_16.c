#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_NODES 5 //max nodes
#define MAX_EDGES 5 //max edges per node
#define MULTI_DIRECTIONAL 0 //0 means edges go both ways
#define VERBOSE 0 //bigger output on printing.

#define DEBUG 1
// #define DEBUG 1

#if DEBUG
	#include "../inc/3_1_16.h"
#else
	#include "3_1_16.h"
#endif


//initalizes the graph with all "empty" edges(-1).
void init_graph(int i, int matrix[][]){
    for(int x = 0; x < i; x++){
        for(int y = 0; y < i; y++){
            //-1 means no edge/node
            matrix[x][y] = -1;
        }
    }
}


// //creates a node with a integer id
gnode_t *create_gnode(gnode_t *node, int id, int weight){
	node->nodeID = id;
	node->weight = weight;

	node->nodeID_previous = -1;
	node->weight_previous = -1;

	node->num_neighbors = 0;
	node->neighbor_gnodes = (gnode_t *) malloc(0 / sizeof(int) * sizeof(gnode_t));
	node->neighbors = malloc(0 * sizeof(int));
	node->neighbor_weights = malloc(0 * sizeof(int));
}


//returns the gnode if it exists
gnode_t find_gnode(gnode_t a, gnode_t *list){
	return a;
}


//inserts the new gnode into the list
void insert_gnode(gnode_t *a, gnode_t *list, int *nodes){
	*nodes = *nodes + 1;

	list = (gnode_t *) realloc(list, sizeof(*nodes) * sizeof(gnode_t));
	list[*nodes - 1] = *a;

	return;
}


//inserts the edge to the graph and gnode lists
void insert_edge(gnode_t *a, gnode_t *b, int weight, int **matrix){
	//add b to a
	a->num_neighbors++;
	int count = a->num_neighbors;

	a->neighbor_gnodes = (gnode_t *) realloc(a->neighbor_gnodes, count * sizeof(gnode_t));
	a->neighbor_gnodes[count-1] = *b;

	a->neighbors = realloc(a->neighbors, count * sizeof(int));
	a->neighbors[count-1] = b->nodeID;
	
	a->neighbor_weights = realloc(a->neighbor_weights, count * sizeof(int));
	a->neighbor_weights[count-1] = weight;

	//add a to b
	b->num_neighbors++;
	count = b->num_neighbors;

	b->neighbor_gnodes = (gnode_t *) realloc(b->neighbor_gnodes, count * sizeof(gnode_t));
	b->neighbor_gnodes[count-1] = *a;

	b->neighbors = realloc(b->neighbors, count * sizeof(int));
	b->neighbors[count-1] = a->nodeID;

	b->neighbor_weights = realloc(b->neighbor_weights, count * sizeof(int));
	b->neighbor_weights[count-1] = weight;

	//add to matrix
	matrix[a->nodeID][b->nodeID];
	if(MULTI_DIRECTIONAL){
		matrix[b->nodeID][a->nodeID];
	}
}


//populates the graph with num random edges. 
void random_edges(int num, int **matrix){
    srand(time(NULL)); //makes it random
    for(int i = 0; i < num; i++){
        int start_node_id = (rand() % (MAX_NODES - 0 + 1)) + 0; 
        int end_node_id = (rand() % (MAX_NODES - 0 + 1)) + 0;  
        int weight = (rand() % (10 - 0 + 1)) + 0; 
        
        //checks if its a path to itself or if it has already has an edge on that path.
        if(start_node_id != end_node_id && matrix[start_node_id][end_node_id] == -1){
			matrix[start_node_id][end_node_id] = weight;


            if(MULTI_DIRECTIONAL){

            }
        }
        //decrement to replace the bad edge
        else{
            i--;
        }
    }
}


// //deletes the edge from the graph and edge list
// edge_t *delete_edge(edge_t *curr_edge, wg_t *master);

// //deletes the node and any edges between gnode_t a and other nodes
// gnode_t *delete_gnode(gnode_t *a, wg_t *master);

//sort function, sorted by array_2 least to greatest
void sort_array(int *ids, int *weights, gnode_t *neighors, int num_elements){
	for (int i = 0; i < num_elements; ++i) {
        for (int j = i + 1; j < num_elements; ++j){
            if (weights[i] > weights[j]){
				//switch array 2
				int a =  weights[i];
				weights[i] = weights[j];
                weights[j] = a;

				//swtich array 1 to match
				int b =  ids[i];
				ids[i] = ids[j];
                ids[j] = b;

				gnode_t c = neighors[i];
				neighors[i] = neighors[j];
                neighors[j] = c;
               }
           }
	}
	return;
}

// //checks if an edge between two nodes exists.
// int is_edge(gnode_t a, gnode_t b);

// //returns the edge if it exists
// edge_t *find_edge(gnode_t a, gnode_t b, wg_t *master);

//prints the graph in format 'graph[4][0] = 3'
void print_graph(int **matrix){
	for (int x = 0; x < MAX_NODES; x++){
		for (int y = 0; x < MAX_NODES; x++){
			if(matrix[x][y] != -1){
				printf("matrix[%d][%d] = %d", x, y, matrix[x][y]);
			}
		}

	}
}

//more descriptive printing of the node. Shows node id
void print_gnode(gnode_t my_gnode){
	if(0 == my_gnode.nodeID){
		return;	
	}
	else{
		if(VERBOSE){
		printf("Gnode ID & Weight: (%d, %d), Previous Gnode & Weight: (%d, %d), Num Neighbors: %d\n", 
			my_gnode.nodeID, my_gnode.weight, my_gnode.nodeID_previous, my_gnode.weight_previous, 
			my_gnode.num_neighbors);
		}
		else{
			printf("Gnode ID & Weight: (%d, %d)\n", 
			my_gnode.nodeID, my_gnode.weight);
		}
	}	
}

//prints the all the gnodes information
void print_gnodes(gnode_t *gnode_list, int size){
	for (int x = 0; x < size; x++){
		print_gnode(gnode_list[x]);
	}
}


//compares the nodes and moves the shortest distance to the front
gnode_t *compare_gnode(gnode_t *node_a, gnode_t *node_b){
	
}

//https://brilliant.org/wiki/dijkstras-short-path-finder/
//algorithm for dijkstra's
int shortest_path(gnode_t *start, gnode_t *end){
	printf("%d\n", start->nodeID);//((edge_t*)(node_a->data))->id

    cd_ll *my_queue;
    // init_cd_ll_queue(my_queue);
    //should I make my own functions?
    
    cd_ll *visited;
    // init(visited, NULL, NULL, NULL);

    return start->nodeID;
}

