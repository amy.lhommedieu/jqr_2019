#ifndef GRAPH_HEADER_LHOMMEDIEU
#define GRAPH_HEADER_LHOMMEDIEU

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// #define DEBUG 0
// #define DEBUG 1

#if DEBUG
	#include "../inc/cd_ll.h"
	#include "../inc/cd_ll_user.h"
	#include "../inc/cd_ll_stack_queue.h"
#else
	#include "cd_ll.h"
	#include "cd_ll_user.h"
	#include "cd_ll_stack_queue.h"
#endif

typedef struct Graph_Node {
    int nodeID;
    int weight;

    int nodeID_previous;
    int weight_previous;

    int num_neighbors;
    struct Graph_Node *neighbor_gnodes;
    int *neighbors;
    int *neighbor_weights;
} gnode_t;

//initalizes the graph with all "empty" edges(-1).
void init_graph(int i, int (*matrix)[]);

//creates a node with a integer id
gnode_t *create_gnode(gnode_t *node, int id, int weight);

//returns the gnode if it exists
gnode_t find_gnode(gnode_t a, gnode_t *list);

//inserts the new gnode into the list
void insert_gnode(gnode_t *a, gnode_t *list, int *nodes);

//inserts the edge to the graph and gnode lists
void insert_edge(gnode_t *a, gnode_t *b, int weight, int **matrix);

//populates the graph with num random edges. 
void random_edges(int num, int **matrix);

// //deletes the edge from the graph and edge list
// edge_t *delete_edge(edge_t *curr_edge, wg_t *master);

// //deletes the node and any edges between gnode_t a and other nodes
// gnode_t *delete_gnode(gnode_t *a, wg_t *master);

//sort function, sorted by array_2 least to greatest
void sort_array(int *ids, int *weights, gnode_t *neighors, int num_elements);

// //checks if an edge between two nodes exists.
// int is_edge(gnode_t a, gnode_t b);

// //returns the edge if it exists
// edge_t *find_edge(gnode_t a, gnode_t b, wg_t *master);

//prints the graph in format 'graph[4][0] = 3'
void print_graph(int **matrix);

//more descriptive printing of the node. Shows node id
void print_gnode(gnode_t my_gnode);

//prints the all the gnodes information
void print_gnodes(gnode_t *gnode_list, int size);

//compares the nodes and moves the shortest distance to the front
gnode_t *compare_gnode(gnode_t *node_a, gnode_t *node_b);

//https://brilliant.org/wiki/dijkstras-short-path-finder/
//algorithm for dijkstra's
int shortest_path(gnode_t *start, gnode_t *end);

#endif