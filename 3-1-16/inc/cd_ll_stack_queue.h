#ifndef CD_LL_STACK_QUEUE_HEADER_LHOMMEDIEU
#define CD_LL_STACK_QUEUE_HEADER_LHOMMEDIEU

//initializes queue
void *init_cd_ll_queue(cd_ll *cd_ll_queue);

//initializes stack
void *init_cd_ll_stack(cd_ll *cd_ll_stack);

//pop the top element
node_t *pop(cd_ll *list);

//peek/print at the top element without removing
node_t *peek_first(cd_ll *list);

//pop the bottom/last element
node_t *pop_last(cd_ll *list);

//peek/print at the bottom/last element without removing
node_t *peek_last(cd_ll *list);

//push node to front
void *push(node_t *node, cd_ll *list);

//push node to back
void *push_back(node_t *node, cd_ll *list);

//ensures the queue is ordered smallest to largest
void *prioritize_queue(cd_ll *cd_ll_queue);

#endif