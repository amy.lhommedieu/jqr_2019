#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define NUM_NODES 5
#define NUM_EDGES 4

#define MULTI_DIRECTIONAL 1

struct Edge {
    int weight;
    int start_node_id;
    int end_node_id;
};

struct Node {
    int nodeID;
    int num_edges; //trackes edges so you know where to insert in edges[]
    struct Edge **edges;
};

//TODO make dynamic- https://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/


/*Function Declarations*/

void random_edges(int **graph, struct Node **nodes, int num);

struct Node *create_node(int node_id, int num_edges);
void delete_node(int **graph, struct Node **nodes, struct Node *curr_node);
struct Node *find_node(int **graph, struct Node **nodes, int node_id);

struct Edge *create_edge(int start, int end, int weight);
void insert_edge(int **graph, struct Node **nodes, struct Edge *curr_edge);
void delete_edge(int **graph, struct Node **nodes, struct Edge *curr_edge);
struct Edge *find_edge(int **graph, struct Node *a, struct Node *b);
int is_edge(int **graph, struct Node *a, struct Node *b);

void print_graph(int **graph);
void print_node(struct Node *my_node);
void print_edge(struct Edge *my_edge);

//is there a path from point a to point b?
int find_path(int **graph, struct Node *a, struct Node *b);
int shortest_path(int a, int b);


void main(){
    struct Node **nodes = (struct Node **)malloc(NUM_NODES * sizeof(struct Node));
    int **graph = (int **)malloc(NUM_NODES * sizeof(int *)); 

    for (int i=0; i<NUM_NODES; i++) 
         graph[i] = (int *)malloc(NUM_NODES * sizeof(int)); 
      
    for(int x = 0; x < NUM_NODES; x++){
        for(int y = 0; y < NUM_NODES; y++){
            //-1 means no edge/node
            graph[x][y] = -1;
        }
    }

    for(int i = 0; i < NUM_NODES; i++){
        nodes[i] = create_node(i, 0);
    }

    random_edges(graph, nodes, NUM_EDGES);
    print_graph(graph);

    for(int i = 0; i < NUM_NODES; i++){
        print_node(nodes[i]);
    }
    
    printf("\n");

    find_path(graph, nodes[0], nodes[3]);

    if(is_edge(graph, nodes[0], nodes[3]) != -1){
        print_edge(find_edge(graph, nodes[0], nodes[3]));
        printf("-\n");
     
        delete_edge(graph, nodes, find_edge(graph, nodes[0], nodes[3]));

        //confirm deleted
        if(is_edge(graph, nodes[0], nodes[3]) == -1){
            printf("deleted\n");
        }
    }

    //destroys the subelements in graph
    for (int i=0; i<NUM_NODES; i++) {
         free(graph[i]);
    }

    //destroys the nodes
    for (int i=0; i<NUM_NODES; i++) {
        delete_node(graph, nodes, nodes[i]);
    }

    //frees the graph
    free(graph);

    printf("Graph freed\n");
}


/*FUNCTIONS*/

void random_edges(int **graph, struct Node **nodes, int num){
    srand(time(NULL)); //makes it random
    // printf("%d\n", num);
    for(int i = 0; i < num; i++){
        // printf("%d\n", i);
        int start_node_id = (rand() % (NUM_NODES - 1)); 
        int end_node_id = (rand() % (NUM_NODES - 1));  
        int weight = (rand() % (10 - 1)); 
        
        // printf("Start: %d, End: %d, Weight: %d\n", start_node_id, end_node_id, weight);
        // printf("graph[%d][%d]: %d\n", start_node_id, end_node_id, graph[start_node_id][end_node_id]);

        //checks if its a path to itself or if it has already has an edge on that path.
        if(start_node_id != end_node_id && graph[start_node_id][end_node_id] == -1){
            // printf("here\n");
            // sleep(1);
            struct Edge *new_edge = create_edge(start_node_id,end_node_id,weight);
            insert_edge(graph, nodes, new_edge);

            int *start_num_edges = NULL;
            start_num_edges = &nodes[start_node_id]->num_edges;
            nodes[start_node_id]->edges[*start_num_edges] = new_edge; //insert into start Node edge array
            *start_num_edges = *start_num_edges + 1; //change the struct nodes number of edges
            
            if(MULTI_DIRECTIONAL){
                int *end_num_edges = NULL;
                end_num_edges = &nodes[end_node_id]->num_edges;
                nodes[end_node_id]->edges[*end_num_edges] = new_edge; //insert into start Node edge array
                *end_num_edges = *end_num_edges + 1; //change the struct nodes number of edges
            }
        }
        //decrement to replace the bad edge
        else{
            i--;
        }
    }
}

struct Node *create_node(int node_id, int num_edges){
    struct Node *curr_node = (struct Node *)malloc(sizeof(struct Node));
    curr_node->nodeID = node_id;
    curr_node->num_edges = num_edges;
    curr_node->edges = (struct Edge **)malloc(NUM_EDGES * sizeof(struct Edge));
    
    return curr_node;
}

void delete_node(int **graph, struct Node **nodes, struct Node *curr_node){
    if(curr_node->num_edges > 0){
        for(int i = curr_node->num_edges; i < 0; i--){
            delete_edge(graph, nodes, curr_node->edges[i-1]);
        }
    }

    free(curr_node->edges);
    curr_node->edges = NULL;

    free(curr_node);
    curr_node = NULL;
}

struct Node *find_node(int **graph, struct Node **nodes, int node_id){
    for(int i = 0; i < NUM_NODES; i++){
        if(nodes[i]->nodeID == node_id){
            return nodes[i];
        }
    }
    return NULL;
}

//TODO: Pass the address
struct Edge *create_edge(int start, int end, int weight){
    struct Edge *curr_edge = (struct Edge *)malloc(sizeof(struct Edge));
    curr_edge->start_node_id = start;
    curr_edge->end_node_id = end;
    curr_edge->weight = weight;

    return curr_edge;
}

void insert_edge(int **graph, struct Node **nodes, struct Edge *curr_edge){
    graph[curr_edge->start_node_id][curr_edge->end_node_id] = curr_edge->weight;
    
    if(MULTI_DIRECTIONAL){
        graph[curr_edge->end_node_id][curr_edge->start_node_id] = curr_edge->weight; //makes it multi-directional
    }
    //TODO add to edges[]
    struct Node *start = find_node(graph, nodes, curr_edge->start_node_id);
    struct Node *end = find_node(graph, nodes, curr_edge->end_node_id);

    if(start == NULL || end == NULL){
        printf("invalid edge\n");
    }

    start->edges[start->num_edges] = curr_edge;
    start->num_edges++;

    end->edges[end->num_edges] = curr_edge;
    end->num_edges++;
}

void delete_edge(int **graph, struct Node **nodes, struct Edge *curr_edge){
    if(MULTI_DIRECTIONAL){
        graph[curr_edge->end_node_id][curr_edge->start_node_id] = -1;
    }
    
    //TODO remove from edges[] & free
    struct Node *start = find_node(graph, nodes, curr_edge->start_node_id);
    struct Node *end = find_node(graph, nodes, curr_edge->end_node_id);

    if(start == NULL || end == NULL){
        printf("invalid edge");
    }

    printf("graph[%d][%d] = %d\n", curr_edge->start_node_id, curr_edge->end_node_id, graph[curr_edge->start_node_id][curr_edge->end_node_id]);

    for(int i = start->num_edges; i > 0; i--){
        if(start->edges[i-1] == curr_edge){
            //it was acting wonky and printing more than one.
            if(start->edges[i-1]->weight == curr_edge->weight && start->edges[i-1]->start_node_id == curr_edge->start_node_id){
                free(start->edges[i-1]);
                start->edges[i-1] = NULL;
            }
        }

    }
}

struct Edge *find_edge(int **graph, struct Node *a, struct Node *b){
    for(int i = a->num_edges; i > 0; i--){
        // printf("1st Node: %d, Start: %d, End: %d\n", a->nodeID, a->edges[i-1]->start_node_id, a->edges[i-1]->end_node_id);
        if(a->edges[i-1]->start_node_id == a->nodeID || a->edges[i-1]->end_node_id == a->nodeID){
            // printf("2nd Node: %d, Start: %d, End: %d\n", b->nodeID, a->edges[i-1]->start_node_id, a->edges[i-1]->end_node_id);
            if(a->edges[i-1]->start_node_id == b->nodeID || a->edges[i-1]->end_node_id == b->nodeID){
                return a->edges[i-1];
            }   
        }
    }
    return NULL;
}

int is_edge(int **graph, struct Node *a, struct Node *b){
    if(graph[a->nodeID][b->nodeID] == -1){
        printf("No edge\n");
        return -1;
    }
    else {
        printf("Edge weight: %d\n", graph[a->nodeID][b->nodeID]);
        
        return graph[a->nodeID][b->nodeID];
    }
}

void print_graph(int **graph){
    for(int x = 0; x < NUM_NODES; x++){
        for(int y = 0; y < NUM_NODES; y++){
            if(graph[x][y] != -1){
                printf("graph[%d][%d] = %d\n", x, y, graph[x][y]);
            }
        }
    }
}

void print_node(struct Node *my_node){
    printf("My node ID: %d\n", my_node->nodeID);
    printf("Number of edges from this node: %d\n", my_node->num_edges);
    for(int x = 0; x < my_node->num_edges; x++){
        print_edge(my_node->edges[x]);
    }
}

void print_edge(struct Edge *my_edge){
    printf("Edge from Node%d to Node%d has a weight of %d\n", 
        my_edge->start_node_id, my_edge->end_node_id, my_edge->weight);
}


int find_path(int **graph, struct Node *a, struct Node *b){
    //make sure there is even a path
    if(a->num_edges == 0 || b->num_edges == 0){
        printf("No path exists.\n");
        return -1;
    }
    else{
        //recursive base case
        if(a == b){
            return 0;
        }
        
        for(int i = 0; i < a->num_edges; i++){
            printf("%d\n", a->edges[i]->weight + find_path(graph, a->edges[i], b));
        }        
    }
    
}

//https://brilliant.org/wiki/dijkstras-short-path-finder/
int shortest_path(int a, int b){
    //intialize distance array
    int dist[NUM_NODES];

    //every node distance is infinity except source
    for(int i = 0; i < NUM_NODES; i++){
        dist[i] = __INT32_MAX__;
    }
    dist[a] = 0;

    //create a queue of all nodes
    
}