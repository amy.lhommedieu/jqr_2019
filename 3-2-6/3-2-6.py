def main():
    curr_date = raw_input("Enter current date in MM/DD/YYYY format : ")
    mm_string, dd_string, yyyy_string = curr_date.split("/")
    

    if(mm_string.isdigit() & dd_string.isdigit() & yyyy_string.isdigit()):
        mm, dd, yyyy = int(mm_string), int(dd_string), int(yyyy_string)
        if(0 <= yyyy):
            if(12 < mm | 0 > mm):
                raise Exception("Not a valid month") 
            if(2 == mm & dd > 28): #February 
                raise Exception("Not a valid date") #Doesn't account for Leap Year
            elif(4 == mm & dd > 30): #April
                raise Exception("Not a valid date")
            elif(6 == mm & dd > 30): #May
                raise Exception("Not a valid date")
            elif(9 == mm & dd > 30): #September
                raise Exception("Not a valid date")
            elif(11 == mm & dd > 30): #November
                raise Exception("Not a valid date")
            elif(dd > 31):
                raise Exception("Not a valid date")
        else:
            raise Exception("Not a valid year") 
    else:
        raise Exception("Not a valid number") 

    print(curr_date)
    return 1

if __name__ == "__main__":
    main()