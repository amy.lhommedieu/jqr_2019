#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int check_days_of_month(int day, int month);
int confirm_year(int year, int age_year);
int valid_year(int year);

int main(int argc, char *argv[]){
    int dd,mm,yyyy;
    int dd_age,mm_age,yyyy_age;
    
    printf("Enter date in MM/DD/YYYY format : ");
    if (scanf("%d/%d/%d", &mm, &dd, &yyyy) != 3){
        perror("date input error");
        exit(1);
    }

    if(1 != check_days_of_month(dd, mm)){
        return 0;
    }

    printf("Enter age in MM/DD/YYYY format : ");
    if (scanf("%d/%d/%d", &mm_age, &dd_age, &yyyy_age) != 3){
        perror("age input error");
        exit(1);
    }

    if(1 == check_days_of_month(dd_age, mm_age) && 1 == confirm_year(yyyy, yyyy_age)){
        printf("Yupp\n");
        return 1;
    }
    printf("Nope.\n"); 
    return 0;
}

int check_days_of_month(int day, int month){
    if(month > 12){
        printf("Invalid month value\n");
        return 0;
    }
    switch(month){            
        case 4 || 6 || 9 || 11:  
            if(day > 30){
                printf("Invalid day value\n");
                return 0;
            }
        case 2:  
            if(day > 28){
                printf("Invalid day value\n");
                return 0;
                //Doesn't account for Leap Year
            }
        default: 
            if(day > 31){
                printf("Invalid day value\n");
                return 0;
            }
    }
    return 1;
}

int confirm_year(int year, int age_year){
    if((year - age_year) < 21){ return 0; }
    else{ return 1; }
}

int valid_year(int year){
    if(0 > year){
        printf("Invalid year value\n");
        return 0;
    }
    return 1;
}
