#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int check_if_even(int num);

int main(int argc, char **argv) {
	//a. For Loop
	printf("a. For Loop\n");
	for(int a = 0; a < 5; a++) printf("%d..\n", a);

	//b. While Loop
	printf("b. While Loop\n");
	int b = 0;
	while(b < 5) {
		printf("%d..\n", b);
		b++;
	}
	
	//c. do while loop
	printf("c. do while loop\n");
	int c = 0;
	do{
		printf("%d..\n", c);
		c++;
	}while(c < 5);

	//d. if statement
	printf("d. if statement\n");
	int d = 0;
	if(d == 0) printf("True\n");

	//e. if->else statement
	printf("e. if->else statement\n");
	int e = 0;
	if(e != 0) printf("True\n");
	else printf("False\n");

	//f. if->else if->else statement
	printf("f. if->else if->else statement\n");
	int f = 10;
	if(f == 0) printf("Zero");
	else if(f%2 == 0) printf("Even\n");
	else printf("Odd\n");
	
	//g. switch statement
	printf("g. switch statement\n");
	int g = 3;
	switch (g)
	{
		case 1: printf("g is 1\n");
			break;
		case 2: printf("g is 2\n");
			break;
		case 3: printf("g is 3\n");
			break;
		default: printf("g is not 1, 2, or 3\n");
	}	

	//h goto labels for a single exit point in function 
	printf("h. goto labels for a single exit point in function \n");
	int h = 7;
	check_if_even(h);
}

int check_if_even(int num){
	if(num % 2 == 0){
		goto even;
	}
	else goto odd;

	even:
		printf("%d is even.\n", num);
		goto exit;
	odd:
		printf("%d is odd.\n", num);
		goto exit;
	exit:
		return num;
}
