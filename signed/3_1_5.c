#include <stdio.h>

int main(){
	int a = 10;
	int b = 5;
	int c = 3;
	int i;

	printf("~~Addition~~\n");
	printf("%d + %d = %d\n", b, a, b+a);
	printf("%d + %d = %d\n", c, b, c+b);
	printf("%d + %d = %d\n", a, c, a+c);

	printf("~~Subtraction~~\n");
	printf("%d - %d = %d\n", b, a, b-a);
	printf("%d - %d = %d\n", c, b, c-b);
	printf("%d - %d = %d\n", a, c, a-c);

	printf("~~Multiplication~~\n");
	printf("%d * %d = %d\n", b, a, b*a);
	printf("%d * %d = %d\n", c, b, c*b);
	printf("%d * %d = %d\n", a, c, a*c);

	printf("~~Division~~\n");
	printf("%d / %d = %d\n", b, a, b/a);
	printf("%d / %d = %d\n", c, b, c/b);
	printf("%d / %d = %d\n", a, c, a/c);

	printf("~~Modulus~~\n");
	printf("%d %c %d = %d\n", b, '%', a, b%a);
	printf("%d %c %d = %d\n", c, '%', b, c%b);
	printf("%d %c %d = %d\n", a, '%', c, a%c);

	printf("~~Incrementation~~\n");
	for (i=0; i<4; i++){
		printf("%d + 1 = %d\n", i, i+1);
	}

	printf("~~Decrementation~~\n");
	for (i=5; i>0; i--){
		printf("%d - 1 = %d\n", i, i-1);
	}

}