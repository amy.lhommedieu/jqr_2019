#include <stdio.h>

int main(){

	int i,j,k;

	int singleArray[] = {0,1,2,3,4,5};

	//Two different ways to declare multidimensional array
	int multiArray[3][2] = {{0,1},{2,3},{4,5}};
	int multiArrayAlt[3][2] = {0,1,2,3,4,5};

	for (i = 0; i < 6; i++){
		printf("Single Array: Index %d equals %d\n", i, singleArray[i]);
	}

	for (j = 0; j < 3; j++){
		for (k = 0; k < 2; k++){
			printf("Multi-Array: Index %dx%d equals %d\n", j, k, multiArray[j][k]);
		}
	}

	for (j = 0; j < 3; j++){
		for (k = 0; k < 2; k++){
			printf("Multi-Array Alternate: Index %dx%d equals %d\n", j, k, multiArrayAlt[j][k]);
		}
	}
}