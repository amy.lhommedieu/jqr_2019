#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv, char **envp) {

	if(argc == 2){
		printf("The argument is %s\n", argv[1]);
	}
	else if (argc > 2){
		printf("Too many arguments.\n");
	}
	else{
		printf("Please enter one argument.\n");
	}
	
	if(*argv[1] == '1'){
		printf("True\n");
	}
	else{
		printf("False\n");
	}

    while(*envp) printf("%s\n",*envp++);
	
	char *home = getenv("HOME");
	printf("%s\n", home);
}