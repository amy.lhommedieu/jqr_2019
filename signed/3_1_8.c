#include <stdio.h>
#include <stdlib.h>
 
void testB();
int testC(int a, int b);
int testD(char(* line)[]);
int testE(int a, int b);
//Also tried char* to get it to print
void* testF(char(* line)[]);
void testG();
int testH();
int testI();

/*int main(int argc, char const *argv[])*/

int main(){
	int a = 5, b = 7, c = 9, d = 3;
	char string[] = "My name is Amy.";
	char secondString[] = "I live in GA.";

	testB();
	testC(a, b);
	testD(&string);
	testE(c, d);
	testF(&secondString);
	testG();
	int (*testH_ptr)(int) = &testH;
	(*testH_ptr)(3);
	printf("%d is the factorial of 5\n", testI(5)); //could make the number come from a global variable set by testG() input.

}

void testB(){
	printf("This is printing a string to the command line.\n");
}

int testC(int a, int b){
	int c = a+b;
	printf("%d\n", c);
}

//Any changes made to 'line' in testD would actually change the original string[]
int testD(char(* line)[]){
	//Why is it not printing?
	printf("%s\n", *line);
}

int testE(int a, int b){
	int c = a+b;
	return(c);
}

void* testF(char(* line)[]){
	printf("%s\n", *line);
	return(*line);
}

void testG(){
	//Why is it not printing?
	printf("Enter your favorite single digit number.\n");
	int n = getchar();
	printf("You've entered: %c\n", n);
}

//TODO
int testH(int x){
	int xSquared = x*x;
	//printf("%d\n", xSquared);
	return xSquared;
}

//Recursive function for factorial
int testI(int num){
	//Test case
	int i;
	if(num == 1) {
		return 1;
	}
	else return num * testI(num-1);
}