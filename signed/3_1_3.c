/*Demonstrate the proper declaration, understanding, and use of C data types and underlying structures:
a. WORD
b. DWORD
c. QUADWORD
d. Short
e. Integer (int)
f. float
g. Character (char)
h. Double (double)
i. Long (long)*/


//WORD: Unsigned datatype that only uses 2 bytes of memory.

//DWORD: Datatype that only uses 4 bytes of memory.

//QUADWORD: Datatype that only uses 8 bytes of memory.

//Short: Modifier for integers. Uses 2 bytes of memory.

//Integer (int): Stores an integer uses 4 bytes of memory.

//float: Stores decimals and has single precision up to 6 decimal places. Uses 4 bytes of memory.

//Character (char): Stores a single character and requires one byte of memory.

//Double (double): Stores decimals and has double precision up to 15 decimal places. Uses 8 bytes of memory.

//Long (long): Modifier for integers. Uses 8 bytes of memory.

#include <stdio.h>

typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef unsigned __int64 QWORD;

int main ()
{
	int a = 1;
	char b = 'B';
	double c = 3.14;
	WORD d = 64;
	DWORD e = 12;
	QWORD f = 21;
	short g = 16;
	long h = 13;
	float i = 3.14;	

	printf("Integer a, %d, takes up %lu byte(s)\n", a, sizeof(int));
	printf("Character b, %c, takes up %lu byte(s)\n", b, sizeof(char));
	printf("Double c, %lf, takes up %lu byte(s)\n", c, sizeof(double));
	printf("Word d, %hu, takes up %lu byte(s)\n", c, sizeof(WORD));
	printf("Dword e, %lu, takes up %lu byte(s)\n", c, sizeof(DWORD));
	printf("Qword f, %llu, takes up %lu byte(s)\n", c, sizeof(QWORD));
	printf("Short g, %hu, takes up %lu byte(s)\n", c, sizeof(short));
	printf("Long h, %lu, takes up %lu byte(s)\n", c, sizeof(long));
	printf("Float i, %f, takes up %lu byte(s)\n", c, sizeof(float));

	return 0;
}