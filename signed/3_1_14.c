#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printArray(int A[], int size) 
{ 
    int i; 
    for (i=0; i < size; i++) 
        printf("%d ", A[i]); 
    printf("\n"); 
} 

void swap(int *xp, int *yp)  
{  
    int temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
}

int main(int argc, char **argv) {
    int arr[] = {15, 1, 33, 10, 42, 2};
    int arr_len = sizeof(arr)/sizeof(arr[0]);
    
    printf("Input array: \n");
    printArray(arr, arr_len);

    int swapped = 0;

    int i = 0;
    int j = 0;
    int end_index = arr_len - 1;
    
    do{
        swapped = 0;
        for(i = 0; i < end_index; i++){
            // printf("%d and %d\n", arr[i], arr[i+1]);
            if(arr[i] > arr[i+1]){
                swapped = 1;
                swap(&arr[i+1], &arr[i]);
            }
            //sets the greatest element seen
            if(arr[i] > j) j = arr[i];
        }
        // printArray(arr, arr_len);
        end_index--; 
        
    }while(swapped);

    printf("Sorted array: \n");
    printArray(arr, arr_len);

    return 0;
    
}
