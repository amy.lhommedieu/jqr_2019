#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

int main(int argc, char **argv) {
	char string[100];
	char newLine[] = "I used to live in Utah.\nMy family lives near the mountains.";
	char addLine[] = "\nThis line has been appended.\n";

	struct stat stats;

	// struct stat {
	// dev_t st_dev; /* device */
	// ino_t st_ino; /* inode */
	// mode_t st_mode; /* protection */
	// nlink_t st_nlink; /* number of hard links */
	// uid_t st_uid; /* user ID of owner */
	// gid_t st_gid; /* group ID of owner */
	// dev_t st_rdev; /* device type (if inode device) */
	// off_t st_size; /* total size, in bytes */
	// blksize_t st_blksize; /* blocksize for filesystem I/O */
	// blkcnt_t st_blocks; /* number of blocks allocated */
	// time_t st_atime; /* time of last access */
	// time_t st_mtime; /* time of last modification */
	// time_t st_ctime; /* time of last change */
	// };

	FILE *readFile;
	FILE *newFile;
	FILE *appendFile;
	FILE *insertFile;

	//readFile 
	readFile = fopen("3_1_7.txt", "r+");
	if(readFile == NULL)
	{
		printf("Error opening readFile!");
	}

	else{
		while(fgets(string, 100, (FILE*)readFile) != NULL){
			printf("%s\n", string);
		}
	}

	fclose(readFile);
	

	//write to file
	newFile = fopen("3_1_7_new.txt", "w");

	if(newFile == NULL)
	{
		printf("Error opening newFile!");
	}

	else {
		fprintf(newFile, "%s", newLine);
	}
	fclose(newFile);


	//append to a file
	appendFile = fopen("3_1_7_new.txt", "a+");

	if(appendFile == NULL)
	{
		printf("Error opening appendFile!");
	}

	else fprintf(appendFile, "%s", addLine);

	//Determine location within file
	int spot = ftell(appendFile);
	printf("Current location: %d", spot);

	fclose(appendFile);


	//Print file information to console
	system("ls -al | grep '3_1_7.txt'");


	//Get file size (can also do the file information)
	stat("3_1_7.txt", &stats);
	printf("File size: %ld\n", stats.st_size);


	//inserting data & determining location within a file
	insertFile = fopen("3_1_7_new.txt", "r+");

	if(insertFile == NULL)
	{
		printf("Error opening insertFile!");
	}

	else {
		fseek(insertFile, 0, SEEK_SET);
		fputs("This is new information. It is overwriting previous data.  ", insertFile);

		printf("Location is char %ld\n", ftell(insertFile));
	}
	fclose(insertFile);


	//delete a file.
	if (remove("3_1_7_new.txt") == 0) printf("File deleted\n");
	else printf("Error deleting file\n");

	return(0);
}
