#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 



void set_speed(int *old_speed, int new_speed){
    old_speed = &new_speed;
}

int main(){
    enum states { 
    IDLE,
    START,
    CHANGE_SPEED,
    STOP
    };

    enum transitions {
        HALT,
        SET_SPEED
    };

    int speed = 0;
    int count = 0;

    enum states state = IDLE;
    enum transitions transition = SET_SPEED;
    
    while(1) {
    switch(state) {

    case IDLE:
        if(count >= 3){
            transition = SET_SPEED;
        }
        switch(transition) {
        case (SET_SPEED):
            set_speed(&speed, rand());
            state = START; 
        }
        break;

    case START:
        switch(transition) {
        case (SET_SPEED):
            set_speed(&speed, rand());
            state = START; 
        case (HALT):
            set_speed(&speed, 0);
            state = STOP;
        }
        break;

    case CHANGE_SPEED:
        switch(transition) {
        case (SET_SPEED):
            set_speed(&speed, rand());
            state = CHANGE_SPEED; 
        case (HALT):
            set_speed(&speed, 0);
            state = STOP;
        }
        break;
    case STOP:
        state = IDLE; // restart cycle on next round
        count = 0;
        break;
    }

    if(count%2 == 0) transition = HALT; //if the speed is even then switch to halt 
    sleep(1);
    count++;
    }
}
