//gcc 3_1_10.c -o 3_1_10.out -w
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int add(int x, int y);

int main(int argc, char **argv) {
	int a = 10;
	int arr[] = {1,2,3,4};
	int *ptr;
	ptr = arr;

	//a.
	int *aPtr;
	aPtr = &a;
	//b.
	printf("%d\n", *aPtr); 

	//c.
	printf("%d\n", &a); //prints address of a
	printf("%d\n", aPtr); //prints address of a

	//d.
	*aPtr = 5;
	printf("%d\n", *&a); //prints value of a
	printf("%d\n", *aPtr); //prints value of a

	//e.
	int (*add_ptr)(int j, int k) = &add;
	add_ptr(3,4);

	//f.
	for(int i = 0; i < 4; i++){
		printf("Array index %d is %d\n", i, *ptr);
		ptr++;
	}
}

int add(int x, int y){
	return x + y;
}