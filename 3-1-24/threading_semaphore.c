#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

sem_t job_id_lock;

void *job_status(void *x);

int main(){

    pthread_t thread1;
    pthread_t thread2;
    int thread_id1;
    int thread_id2;

    int count = 0;

    //pshared == 0 because it is shared between processes
    if(0 != sem_init(&job_id_lock, 0, 1)){
        perror("sem_init error");
        exit(1);
    }

    thread_id1 = pthread_create(&thread1, NULL, &job_status, &count);
    thread_id2 = pthread_create(&thread2, NULL, &job_status, &count);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    sem_destroy(&job_id_lock);

    return 0;
}

void *job_status(void *x){
    sem_wait(&job_id_lock);

    //critical code
    *(int*)x += 1;
    printf("thread %d started.\n", *(int*)x);
    sleep(1);
    printf("thread %d ended.\n", *(int*)x);

    sem_post(&job_id_lock);
}

//Output without semaphore
// thread 1 started.
// thread 2 started.
// thread 2 ended.
// thread 2 ended.

//Output with semaphore
// thread 1 started.
// thread 1 ended.
// thread 2 started.
// thread 2 ended.