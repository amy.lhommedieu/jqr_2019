#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

pthread_mutex_t job_id_lock;

void *job_status(void *x);

int main(){

    pthread_t thread1;
    pthread_t thread2;
    int thread_id1;
    int thread_id2;

    int count = 0;

    pthread_mutex_init(&job_id_lock, NULL);

    thread_id1 = pthread_create(&thread1, NULL, &job_status, &count);
    thread_id2 = pthread_create(&thread2, NULL, &job_status, &count);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    pthread_mutex_destroy(&job_id_lock);

    return 0;
}

void *job_status(void *x){
    pthread_mutex_lock(&job_id_lock);

    //critical code
    *(int*)x += 1;
    printf("thread %d started.\n", *(int*)x);
    sleep(1);
    printf("thread %d ended.\n", *(int*)x);

    pthread_mutex_unlock(&job_id_lock);
}

//Output without mutex
// thread 1 started.
// thread 2 started.
// thread 2 ended.
// thread 2 ended.

//Output with mutex
// thread 1 started.
// thread 1 ended.
// thread 2 started.
// thread 2 ended.