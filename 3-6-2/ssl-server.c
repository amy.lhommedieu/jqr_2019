#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <netdb.h>
#include <netinet/in.h>
#include <resolv.h>
#include "openssl/ssl.h"
#include "openssl/err.h"

#define FAIL    -1

//create a cert: 
//openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout mycert.pem -out mycert.pem

int open_connection(char* port); // Create the SSL socket and intialize the socket address structure
int isRoot();
SSL_CTX* init_server_ctx(void);
void load_certs(SSL_CTX* ctx, char* CertFile, char* KeyFile);
void show_certs(SSL* ssl);
void Servlet(SSL* ssl); /* Serve the connection -- threadable */

int main(int count, char *Argc[])
{
    SSL_CTX *ctx;
    int server;
    char *portnum;
    
    if(!isRoot())
    {
        printf("This program must be run as root/sudo user!!");
        exit(0);
    }
    if ( count != 2 )
    {
        printf("Usage: %s <portnum>\n", Argc[0]);
        exit(0);
    }

    // Initialize the SSL library
    SSL_library_init();
    portnum = Argc[1];
    ctx = init_server_ctx();        /* initialize SSL */
    load_certs(ctx, "mycert.pem", "mycert.pem"); /* load certs */
    server = open_connection(portnum);    /* create server socket */
    while (1)
    {
        struct sockaddr_storage other_addr;
        socklen_t addr_size = sizeof(other_addr);
        SSL* ssl;

        int new_s = accept(server, (struct sockaddr *)&other_addr, &addr_size);

        ssl = SSL_new(ctx);     /* get new SSL state with context */
        SSL_set_fd(ssl, new_s); /* set connection socket to SSL state */
        Servlet(ssl);           /* service connection */
    }
    close(server);          /* close server socket */
    SSL_CTX_free(ctx);         /* release context */
}

int open_connection(char* port)
{
    int status;
    int s; //socket-related

    //creating the structs that hold address information
    struct addrinfo hints;
    struct addrinfo *servinfo; //points to results

    //ensuring hints is zero'ed and initialize how we want
    memset(&hints, 0, sizeof hints); 
    hints.ai_family = AF_UNSPEC;  
    hints.ai_socktype = SOCK_STREAM; //TCP
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me    

    //error checking for pointer to linked-list (servinfo) of struct addrinfos & their structs sockaddr
    if((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return 1;
    }
    
    //get the socket descriptor
    s = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol /*0*/);
    if(s == FAIL)
    {
        fprintf(stderr, "socket error\n");
        return 1;
    }
    
    //error checking for bind (also binds servinfo to the port passed into getaddrinfo())
    if(bind(s, servinfo->ai_addr, servinfo->ai_addrlen) == FAIL)
    {
        fprintf(stderr, "bind error\n");
        return 1;
    }

    //prevents "Address already in use" error message
    int yes = 1;
    if (setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof yes) == FAIL) 
    {
        perror("setsockopt");
        return 1;
    }

    //Listen for incoming connections
    if(listen(s, 10))
    {
        fprintf(stderr, "listen error\n");
        return 1;
    }

    //free the linked-list
    freeaddrinfo(servinfo); 

    return s;
}


int isRoot()
{
    if (getuid() != 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}


SSL_CTX* init_server_ctx(void)
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;
    OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
    SSL_load_error_strings();   /* load all error messages */
    method = TLS_server_method();  /* create new server-method instance */
    ctx = SSL_CTX_new(method);   /* create new context from method */
    if ( ctx == NULL )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    return ctx;
}


void load_certs(SSL_CTX* ctx, char* CertFile, char* KeyFile)
{
    /* set the local certificate from CertFile */
    if ( SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM) <= 0 )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* set the private key from KeyFile (may be the same as CertFile) */
    if ( SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) <= 0 )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* verify private key */
    if ( !SSL_CTX_check_private_key(ctx) )
    {
        fprintf(stderr, "Private key does not match the public certificate\n");
        abort();
    }
}


void show_certs(SSL* ssl)
{
    X509 *cert;
    char *line;
    cert = SSL_get_peer_certificate(ssl); /* Get certificates (if available) */
    if ( cert != NULL )
    {
        printf("Server certificates:\n");
        line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
        printf("Subject: %s\n", line);
        free(line);
        line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
        printf("Issuer: %s\n", line);
        free(line);
        X509_free(cert);
    }
    else
        printf("No certificates.\n");
}


void Servlet(SSL* ssl) /* Serve the connection -- threadable */
{
    char buf[1024] = {0};
    int sd, bytes;
    const char* ServerResponse="Here";
    const char *cpValidMessage = "Where are you?";
    if ( SSL_accept(ssl) == FAIL )     /* do SSL-protocol accept */
        ERR_print_errors_fp(stderr);
    else
    {
        show_certs(ssl);        /* get any certificates */
        bytes = SSL_read(ssl, buf, sizeof(buf)); /* get request */
        buf[bytes] = '\0';
        printf("Client msg: \"%s\"\n", buf);
        if ( bytes > 0 )
        {
            if(strcmp(cpValidMessage,buf) == 0)
            {
                SSL_write(ssl, ServerResponse, strlen(ServerResponse)); /* send reply */
            }
            else
            {
                SSL_write(ssl, "Invalid Message", strlen("Invalid Message")); /* send reply */
            }
        }
        else
        {
            ERR_print_errors_fp(stderr);
        }
    }
    sd = SSL_get_fd(ssl);       /* get socket connection */
    SSL_free(ssl);         /* release SSL state */
    close(sd);          /* close connection */
}

/*output:
No certificates.
Client msg: "Where are you?"*/