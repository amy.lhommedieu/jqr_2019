#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>

#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>

char *filter_non_alphanumeric(char *string_in);
bool verify_non_alphanumeric(char *string_in);

int main(){

    //a. Formatting string vulnerabilities

    char *string_a = "This is a string.";
    printf(string_a); //vulnerable
    printf("\n");
    printf("%s\n", string_a); //better practice

    //b. safe buffer size allocation

    char *string_b = (char *)malloc(sizeof(char)*18); 
    strncpy(string_b, string_a, 17); //size of the intended string plus one for the null teminator
    printf("%s It has been copied into a new destination\n", string_b); 

    //c. Input Sanitization 

    ssize_t read;
    size_t size_string_c;
    char *string_c = NULL;
    printf("Enter a line.\n");
    read = getline(&string_c, &size_string_c, stdin);

    string_c = filter_non_alphanumeric(string_c);

    printf("%s\n", string_c);
 

    //d. Input Validation

    size_t size_string_d;
    char *string_d = NULL;
    printf("Enter another line.\n");
    read = getline(&string_d, &size_string_d, stdin);

    if(verify_non_alphanumeric(string_d)){
        printf("'%s' contains only alpha-numeric characters.\n", string_d);
    }

    //e. State machines

    int count = 0;

    bool state_red = true;
    bool state_yellow = false;
    bool state_green = false;

    bool car_present = false;
    bool car_absent = true;

    //chose to loop three times to not hold up the program too much
    while(count < 2){
        if(state_red){
            printf("Light is red. ");
            if(car_present){
                printf("Car waiting. \n");
                state_red = false;
                state_green = true;
                sleep(1);
            } 
            else if(car_absent){
                printf("No car. \n");
                sleep(2);
                car_absent = false;
                car_present = true;
            } 
            count++; 
        }
        else if(state_yellow){
            printf("Light is yellow. ");
            sleep(1);
            state_yellow = false;
            state_red = true;
        }
        else if(state_green){
            printf("Light is green. ");
            if(car_absent){
                printf("No car. \n");
                state_green = false;
                state_yellow = true;
                sleep(1);
            } 
            else if(car_present){
                printf("Car driving. \n");
                sleep(2);
                car_present = false;
                car_absent = true;
            }  
        }
    }


    //f. SSL Library 

    //Separate file.

    //g. Securely zeroing out memory

    memset(string_b, 0, sizeof(char)*strlen(string_b));
    free(string_b);
    string_b = NULL;

    memset(string_c, 0, sizeof(char)*strlen(string_c));
    free(string_c); 
    string_c = NULL;

    memset(string_d, 0, sizeof(char)*strlen(string_d));
    free(string_d);
    string_d = NULL;

    return 0;
}

char *filter_non_alphanumeric(char *string_in){
    char *string_out = (char *)malloc(sizeof(char)*strlen(string_in));
    int curr_index = 0;
    char temp;
    
    for(int x = 0; x < strlen(string_in); x++){
        temp = string_in[x];
        if((temp > 47 && temp < 58) || (temp > 64 && temp < 91) || (temp > 96 && temp < 123) || 32 == temp){
            string_out[curr_index] = temp;
            curr_index++;
        }
    }
    return string_out;
}

bool verify_non_alphanumeric(char *string_in){
    char temp;
    bool ret = true;
    
    for(int x = 0; x < strlen(string_in); x++){
        temp = string_in[x];
        if(!(temp > 47 && temp < 58) || !(temp > 64 && temp < 91) || !(temp > 96 && temp < 123) || 32 != temp){
            ret = false;
        }
    }
    return ret;
}
