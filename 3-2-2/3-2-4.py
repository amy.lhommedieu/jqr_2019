#! /usr/bin/python
import os

def main():
    #h - Create a new file
    f = open("test.txt", "w+")

    #d - Write data to a file
    f.write('\n')
    f.write('The Coronavirus is sweeping America.\n')
    f.write('There are currently 4,000 cases in the states.\n')
    f.write('Toilet paper has been sold out for days.\n')
    f.close()

    #m - Insert data into a file
    f = open("test.txt", "r+")
    old = f.read()
    print(old)
    f.seek(0)
    f.write("UPDATE: Today is March 16, 2020." + old)


    #f - Close an open file
    f.close()


    #a - Open an existing file
    with open("test.txt", "r+") as f:

        #b - Read data from a file
        print(f.readline())
        f.seek(0)

        #c - Parse data from a file
        print(f.readlines())


        #e - Modify data in a file
        f.seek(24)
        if(f.read(1) == '6'):
            f.seek(24)
            f.write('7')

        #l - Determine location within a file
        print(f.tell())


    #i - Append data to an existing file
    with open("test.txt", "a") as f:
        f.write('Panic is ensuing!')

    #g - Print file information to the console
    file_data = os.stat("test.txt")
    print(file_data)

    #k - Determine the size of a file
    print(file_data.st_size)

    #j - Delete file
    if os.path.exists("test.txt"):
        os.remove("test.txt")
        print("deleted")
    else:
        print("The file does not exist") 

if __name__=="__main__":
    main()