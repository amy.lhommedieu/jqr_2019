#! /usr/bin/python

#Addition (+)
a = 1 + 3
print(a)

#Subtraction (-)
b = a - 2
print(b)

#Multiplication (*)
c = b * 7
print(c)

#Division (/)
d = c / 2
print(d)

#Modulus (%)
e = d % 5
print(e)