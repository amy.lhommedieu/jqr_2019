#! /usr/bin/python

def main():
    colors = ['red', 'yellow', 'green', 'blue']\

    #a - for loop
    print('for loop by element')
    for item in colors:
        print(item)

    print('for loop by index')
    for index in range(len(colors)):
        print(colors[index])

    #b - while loop
    print('while loop')
    x = 0
    while x < len(colors):
        print(colors[x])
        x+=1

    #c - if statement
    print('if statement')
    if 3 < len(colors):
        print(colors[3])

    #d - if->else statement
    print('if->else statement')
    if 2 < len(colors):
        print(colors[2])
    else:
        print(colors[0])

    #e - if->elif->else statement
    print('if>elif->else statement')
    if 5 < len(colors):
        print(colors[5])
    elif 5-1 < len(colors):
        print(colors[5-1])
    else:
        print(colors[0])


if __name__=="__main__":
    main()