#! /usr/bin/python

# Integer (int)
a = 10
print(a)

# Float (float)
b = 4.50
print(b)

# String (str)
c = 'March 16'
print(c)

# List (list)
d = [1, "Amy", True]
print(d[2])

# Multi-Dimensional List
e = [["a", "b", "c"],[1,2,3]]
print(e[0])

# Dictionary (dict)
f = {"Amy":"October 20", "Tyler":"October 11", "June":"June 15"}
print(f.keys())

# Tuple (tuple)
g = ("hello", "world")
print(g[1])

# Singleton
h = ("augusta",)
print(h[0])
