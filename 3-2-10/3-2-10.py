#!/usr/bin/python

import sys
import argparse

def main(argv):
    
    x = len(sys.argv)
    print x-1, 'argument(s).'
    if(2 < x):
        #simple version
        print 'Arguments:', ", ".join(sys.argv[1:])
        flags = ""
        for i in range(x-1):
            argument = sys.argv[i+1]
            if argument[0] == "-":
                flags += sys.argv[i+1]
        print "Flags present: ", flags
    elif(2 == x):
        #more robust
        parser = argparse.ArgumentParser()
        parser.add_argument('--echo', 
            action='store_true', 
            help='echo flag')
        parser.add_argument('--verbose', 
            action='store_true', 
            help='verbose flag')

        my_args = parser.parse_args()

        if my_args.echo:
            print "Program name: ", sys.argv[0]
        if my_args.verbose:
            print "I like to talk"
    else:
        print 'No arguments.\n'

    
    
if __name__ == "__main__":
    main(sys.argv[1:])