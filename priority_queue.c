#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct item{
    void * data;
    uint16_t priority;
    struct item * next;
} item_t;

item_t * create_item(void * value, uint16_t weight){
    if(NULL == value || 0 > weight){
        return NULL;
    }

    item_t * new_item = (item_t *) malloc(sizeof(item_t));

    if(NULL == new_item){
        return NULL;
    }

    new_item->data = value;
    new_item->priority = weight;
    new_item->next = NULL;

    return new_item;
}

item_t * push_item(item_t ** head, item_t * item){
    if(NULL == (*head) || NULL == item){
        return NULL;
    }

    if((*head)->priority < item->priority){
        item->next = *head;
        (*head) = item;
    }
    else{
        item_t * temp = (*head);

        while(NULL != temp->next && temp->next->priority > item->priority){
            temp = temp->next;
        }

        item->next = temp->next;
        temp->next = item;
    }

    return (*head);
}

item_t * pop_item(item_t ** head){
    if(NULL == (*head)){
        return NULL;
    }
    else{
        item_t * temp = (*head);
        (*head) = (*head)->next;
        return temp;
    }
}

item_t * peek(item_t ** head){
    return (*head);
}
