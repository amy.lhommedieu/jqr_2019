#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

pthread_mutex_t job_id_lock;
pthread_t thread1;
pthread_t thread2;

typedef void (*sighandler_t)(int);

void *job_status(void *x);
void sigusr1_handler(int sig);
void sigusr2_handler(int sig);

int main(){
    signal(SIGUSR1, sigusr1_handler);
    signal(SIGUSR2, sigusr2_handler);
    signal(SIGINT, SIG_IGN);

    int count = 0;

    pthread_mutex_init(&job_id_lock, NULL);

    int thread_id1 = pthread_create(&thread1, NULL, &job_status, &count);
    int thread_id2 = pthread_create(&thread2, NULL, &job_status, &count);

    raise(SIGUSR1);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    pthread_mutex_destroy(&job_id_lock);

    pid_t pid = getpid();
    kill(pid, SIGUSR2);

    return 0;
}

void sigusr1_handler(int sig){
    printf("Caught user_signal1, example of raise().\n"); /*unsafe - non-async-signal-safe function*/
    
    //any action you want
}

void sigusr2_handler(int sig){
    printf("Caught user_signal2, example of kill() to send signal to specific pid.\n"); /*unsafe - non-async-signal-safe function*/
    raise(SIGINT);
}

void *job_status(void *x){
    pthread_mutex_lock(&job_id_lock);

    //critical code
    *(int*)x += 1;
    printf("thread %d started.\n", *(int*)x);
    sleep(1);
    printf("thread %d ended.\n", *(int*)x);

    pthread_mutex_unlock(&job_id_lock);

    return NULL;
}