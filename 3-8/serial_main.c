#include <stdlib.h>

// #define DEBUG
#ifndef DEBUG
	#include "serial.h"

   typedef struct test{
      u_int16_t num; //size of 2
      char letter; //size of 1
      char string[20]; //size of 8
   } test_t; //size of 40
#else
   #include "src/serial.c"
#endif


int main(void) {

   unsigned char buf[1024] = {0};
   test_t *src = calloc(1, sizeof(test_t));

   src->num = 12;
   src->letter = 'A';
   strncpy(src->string, "this should work.", strlen("this should work."));

   test_t *dest = calloc(1, sizeof(test_t));
   // dest->string = calloc(64, sizeof(unsigned char));

	unsigned int packetsize, ps2;
   
	packetsize = pack(buf, "HHcs", 0, src->num, (int8_t) src->letter, src->string);

	printf("packet is %u bytes\n", packetsize);

    packi16(buf, packetsize);

	unpack(buf, "HHc64s", &ps2, &dest->num, &dest->letter, &dest->string);

	printf("Packet size: %d bytes letter: '%c' number: %d string: '%s' \n", ps2, dest->letter, dest->num, dest->string);

    return 0;
}