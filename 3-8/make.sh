#!/bin/bash

if [ $1 == 'clean' ]
then
	rm -rf build
else
	if [ ! -d build ]
	then
		mkdir build
	fi

	cd build && \
	cmake ../ && \
	make && \
	cd ..

	#will work if the cMakeLists.txt has an output
	#directory of bin.
	if [ ! -d bin ] && [ -d build/bin ]
	then
		ln -s build/bin ./bin
	fi
fi

date=$(date +%m-%d-%Y_%H%M)

if [ -d ./historical/$date ]
	then
		rm -rf ./historical/$date
fi

mkdir ./historical/$date
mkdir ./historical/$date/src
mkdir ./historical/$date/inc

cp inc/serial* ./historical/$date/inc
cp src/serial* ./historical/$date/src
cp serial* ./historical/$date
cp CMakeLists.txt ./historical/$date

bin/serial
