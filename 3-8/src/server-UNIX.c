#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

// #define DEBUG
#ifndef DEBUG
	#include "../inc/serial-adj.h"

    typedef struct test{
        u_int16_t num; //size of 2
        char letter; //size of 1
        char string[20]; //size of 8
        char *str_2; //size of 8
    } test_t; //size of 40
#else
   #include "serial-adj.c"
#endif

/*derived from Beej's Guide to Networking*/

int main(){
    //this socket descriptor
    unsigned int s;
    //client socket descriptor
    unsigned int s2;
    //length of the path and the unix family
    int len;

    //using sockaddr_un because this is a unix socket
    struct sockaddr_un local;
    struct sockaddr_un remote;

    //getting socket descriptor
    s = socket(AF_UNIX, SOCK_STREAM, 0);

    //error checking for socket call
    if(s == -1){
        fprintf(stderr, "socket descriptor error\n");
        return 1;
    }

    //bind the socket to the address in the Unix domain
    local.sun_family = AF_UNIX; //local declared above
    //touch unix_socket prior to create the file (maybe not necessary)
    strncpy(local.sun_path, "/home/amy/repos/jqr_2019/3-8/unix_socket", sizeof(local.sun_path) - 1);

    //removes the socket if it already exists
    unlink(local.sun_path);
    len = strlen(local.sun_path) + sizeof(local.sun_family);
    
    //error checking for bind (also binds the socket descriptor to the address given)
    if(bind(s, (struct sockaddr *)&local, len) == -1){
        fprintf(stderr, "bind error\n");
        return 1;
    }

    //up to 10 connections can be queued before turned away
    if(listen(s, 10) == -1){
        perror("listen error");
        exit(1);
    }

    int loop = 1; //continuously loop waiting for connections
    unsigned char str[1024] = {0}; //same size as buf in client
    unsigned char * p_str = str;
    test_t *dest = calloc(1, sizeof(test_t));
    dest->str_2 = calloc(64, sizeof(char));
    int packetsize;
    int num_recv;
    int count;

    while(loop){
        printf("...\n");
        //accepts the connection from a client and returns a new socket descriptor
        len = sizeof(struct sockaddr_un);
        s2 = accept(s, (struct sockaddr *)&remote, &len); //remote now holds the client's information
        
        if(s2 == -1){
            perror("accept error");
            exit(1);
        }

        printf("Connected\n");

        num_recv = recv(s2, p_str, 50, 0); /*changed to 50 from 100 to test 25JUN*/

        printf("%d\n", num_recv);

        if(num_recv < 0){
            perror("recv error");
        }

        /*added the serial size check.*/
        else if(num_recv > 0){
            if (send(s2, p_str, num_recv, 0) < 0) {
                perror("send error");
            }
            else{
                unpack(str, "h", &packetsize);
                printf("packetsize: %d\n", packetsize);

                while(packetsize > num_recv){ //make sure you have gotten all the data.
                    num_recv = num_recv + recv(s2, p_str+num_recv, 50, 0);
                    printf("%d\n", num_recv);
                }

                unpack(str, "hHc21s64s", &packetsize, &dest->num, &dest->letter, &dest->string, dest->str_2);
                printf("Packet size: %d bytes letter: '%c' number: %d string: '%s' str_2: '%s' \n", packetsize, dest->letter, dest->num, dest->string, dest->str_2);
                

                count = count + num_recv;
                if (count > packetsize) {
                    printf("exiting...\n");
                    loop = 0;
                }
            }
            //set the pointer to the place after it was filled.
            p_str = p_str + num_recv;
        }

        close(s2);
    }
    return 0;
}

/*Output:

...
Connected
100
Packet size: 24 bytes letter: 'A' number: 12 string: 'this should work.' 
exiting...

*/