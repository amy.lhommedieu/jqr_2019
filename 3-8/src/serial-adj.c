#define DEBUG
#ifndef DEBUG
	#include "serial-cush.h"
#else
	#include "../inc/serial-cush.h"
#endif

typedef struct test{
   	uint16_t num; //size of 2
   	char letter; //size of 1
   	char string[20]; //size of 8 when it was a char *
    char *str_2; //size of 8
} test_t; //size of 40


void packi16(unsigned char *buf, unsigned int i)
{
	*buf++ = i>>8; /*gets the most significant 8 (logically the first 8) and places them in order in buf. Then buf is incremented*/ 
    *buf++ = i; 
}


void packi32(unsigned char *buf, unsigned long int i)
{
	*buf++ = i>>24; *buf++ = i>>16;
	*buf++ = i>>8;  *buf++ = i;
}


void packi64(unsigned char *buf, unsigned long long int i)
{
	*buf++ = i>>56; *buf++ = i>>48;
	*buf++ = i>>40; *buf++ = i>>32;
	*buf++ = i>>24; *buf++ = i>>16;
	*buf++ = i>>8;  *buf++ = i;
}


int16_t unpacki16(unsigned char *buf)
{
	unsigned int i2 = ((unsigned int)buf[0]<<8) | buf[1];
	int i;

	// change unsigned numbers to signed
	if (i2 <= 0x7fffu) { i = i2; }
	else { i = -1 - (unsigned int)(0xffffu - i2); }

	return i;
}

 
uint16_t unpacku16(unsigned char *buf)
{
	return ((unsigned int)buf[0]<<8) | buf[1];
}

 
int32_t unpacki32(unsigned char *buf)
{
	unsigned long int i2 = ((unsigned long int)buf[0]<<24) |
	                       ((unsigned long int)buf[1]<<16) |
	                       ((unsigned long int)buf[2]<<8)  |
	                       buf[3];
	long int i;

	// change unsigned numbers to signed
	if (i2 <= 0x7fffffffu) { i = i2; }
	else { i = -1 - (long int)(0xffffffffu - i2); }

	return i;
}


uint32_t unpacku32(unsigned char *buf)
{
	return ((unsigned long int)buf[0]<<24) |
	       ((unsigned long int)buf[1]<<16) |
	       ((unsigned long int)buf[2]<<8)  |
	       buf[3];
}


int64_t unpacki64(unsigned char *buf)
{
	unsigned long long int i2 = ((unsigned long long int)buf[0]<<56) |
	                            ((unsigned long long int)buf[1]<<48) |
	                            ((unsigned long long int)buf[2]<<40) |
	                            ((unsigned long long int)buf[3]<<32) |
	                            ((unsigned long long int)buf[4]<<24) |
	                            ((unsigned long long int)buf[5]<<16) |
	                            ((unsigned long long int)buf[6]<<8)  |
	                            buf[7];
	long long int i;

	// change unsigned numbers to signed
	if (i2 <= 0x7fffffffffffffffu) { i = i2; }
	else { i = -1 -(long long int)(0xffffffffffffffffu - i2); }

	return i;
}

uint64_t unpacku64(unsigned char *buf)
{
	return ((unsigned long long int)buf[0]<<56) |
	       ((unsigned long long int)buf[1]<<48) |
	       ((unsigned long long int)buf[2]<<40) |
	       ((unsigned long long int)buf[3]<<32) |
	       ((unsigned long long int)buf[4]<<24) |
	       ((unsigned long long int)buf[5]<<16) |
	       ((unsigned long long int)buf[6]<<8)  |
	       buf[7];
}

/*
** pack() -- store data dictated by the format string in the buffer
**
**   bits |signed   unsigned   float   string
**   -----+----------------------------------
**      8 |   c        C         
**     16 |   h        H         f
**     32 |   l        L         d
**     64 |   q        Q         g
**      - |                               s
**
**  (16-bit unsigned length is automatically prepended to strings)
*/ 

uint16_t pack(unsigned char *buf, char *format, ...)
{
	va_list ap;

	signed char c;              // 8-bit
	unsigned char C;

	int h;                      // 16-bit
	unsigned int H;

	long int l;                 // 32-bit
	unsigned long int L;

	long long int q;            // 64-bit
	unsigned long long int Q;

	union float32_t f;                    // floats
	union float64_t d;

	char *s;                    // strings
	uint32_t len;

	unsigned int size = 0;

	va_start(ap, format);

	for(; *format != '\0'; format++) {
        // printf_buf(buf);
        // printf("%c - ", *format);
		switch(*format) {
		case 'c': // 8-bit
			size += 1;
			c = (signed char)va_arg(ap, int); // promoted
			*buf = c;
            *buf++;
			break;

		case 'C': // 8-bit unsigned
			size += 1;
			C = (unsigned char)va_arg(ap, unsigned int); // promoted
			*buf++ = C;
			break;

		case 'h': // 16-bit
			size += 2;
			h = va_arg(ap, int);
			packi16(buf, h);
			buf += 2;
			break;

		case 'H': // 16-bit unsigned
			size += 2;
			H = va_arg(ap, unsigned int);
			packi16(buf, H);
            // printf_buf(buf);
			buf += 2;
			break;

		case 'l': // 32-bit
			size += 4;
			l = va_arg(ap, long int);
			packi32(buf, l);
			buf += 4;
			break;

		case 'L': // 32-bit unsigned
			size += 4;
			L = va_arg(ap, unsigned long int);
			packi32(buf, L);
			buf += 4;
			break;

		case 'q': // 64-bit
			size += 8;
			q = va_arg(ap, long long int);
			packi64(buf, q);
			buf += 8;
			break;

		case 'Q': // 64-bit unsigned
			size += 8;
			Q = va_arg(ap, unsigned long long int);
			packi64(buf, Q);
			buf += 8;
			break;

		case 'f': // float-32
			size += 4;
            check_buf(size);
			f.frac = va_arg(ap, double); // promoted 
			packi32(buf, f.whole);
			buf += 4;
			break;

		case 'd': // float-64
			size += 8;
            check_buf(size);
			d.frac = va_arg(ap, double);
			packi32(buf, d.whole);
			buf += 8;
			break;

		case 's': // string
			s = va_arg(ap, char*);
			len = strlen(s);
			size += len + 2;
			packi16(buf, len);
			buf += 2;
			memcpy(buf, s, len);
			buf += len;
			break;
		}
	}

	va_end(ap);

	return size;
}

/*
** unpack() -- unpack data dictated by the format string into the buffer
**
**   bits |signed   unsigned   float   string
**   -----+----------------------------------
**      8 |   c        C         
**     16 |   h        H         f
**     32 |   l        L         d
**     64 |   q        Q         g
**      - |                               s
**
**  (string is extracted based on its stored length, but 's' can be
**  prepended with a max length)
*/
void unpack(unsigned char *buf, char *format, ...)
{
	va_list ap;

	signed char *c;              // 8-bit
	unsigned char *C;

	int *h;                      // 16-bit
	unsigned int *H;

	long int *l;                 // 32-bit
	unsigned long int *L;

	long long int *q;            // 64-bit
	unsigned long long int *Q;

    float *f;
    double *d;

	union float32_t uf;          // floats
	union float64_t ud;

	char *s;
	uint32_t len, maxstrlen=0, count;

    unsigned int size = 0;

	va_start(ap, format);

    // printf("  - ");
	for(; *format != '\0'; format++) {
        // printf_buf(buf);
        // printf("%c - ", *format);
		switch(*format) {
		case 'c': // 8-bit
            size += 1;
			c = va_arg(ap, signed char*);
			if (*buf <= 0x7f) { *c = *buf;} // re-sign
			else { *c = -1 - (unsigned char)(0xffu - *buf); }
			buf++;
			break;

		case 'C': // 8-bit unsigned
            size += 1;
			C = va_arg(ap, unsigned char*);
			*C = *buf++;
			break;

		case 'h': // 16-bit
            size += 2;
			h = va_arg(ap, int*);
			*h = unpacki16(buf);
			buf += 2;
			break;

		case 'H': // 16-bit unsigned
            size += 2;
			H = va_arg(ap, unsigned int*);
			*H = unpacku16(buf);
			buf += 2;
			break;

		case 'l': // 32-bit
            size += 4;
			l = va_arg(ap, long int*);
			*l = unpacki32(buf);
			buf += 4;
			break;

		case 'L': // 32-bit unsigned
            size += 4;
			L = va_arg(ap, unsigned long int*);
			*L = unpacku32(buf);
			buf += 4;
			break;

		case 'q': // 64-bit
            size += 8;
			q = va_arg(ap, long long int*);
			*q = unpacki64(buf);
			buf += 8;
			break;

		case 'Q': // 64-bit unsigned
            size += 8;
			Q = va_arg(ap, unsigned long long int*);
			*Q = unpacku64(buf);
			buf += 8;
			break;

		case 'f': // float-32
            size += 4;
            check_buf(size);
			f = va_arg(ap, float*);
			uf.whole = unpacku32(buf);
            *f = uf.frac;
			buf += 2;
			break;

		case 'd': // float-64
            size += 8;
            check_buf(size);
			d = va_arg(ap, double*);
			ud.whole = unpacku64(buf);
			*d = ud.frac;
			buf += 8;
			break;

		case 's': // string
			s = va_arg(ap, char*);
			len = unpacku16(buf);
            size += len + 4; //accounts for the null term.
			buf += 2;
			if (maxstrlen > 0 && len > maxstrlen) count = maxstrlen - 1;
			else count = len;
			memcpy(s, buf, count);
			s[count] = '\0';
			buf += len;
			break;

		default:
			if (isdigit(*format)) { // track max str len
				maxstrlen = maxstrlen * 10 + (*format-'0');
			}
		}

		if (!isdigit(*format)) maxstrlen = 0;
	}

	va_end(ap);
}


void check_buf(unsigned int size){
    if(size > BUF_SIZE/8){
        fprintf(stderr, "Exceeding size of buffer. Terminating.\n");
        exit(1);
    }
    return;
}

void printf_buf(unsigned char *buf){
    for(int i = 0; i < 100; i++){
        printf("%d", buf[i]);
    }
    printf("\n");
}


// int main(void) {

//    unsigned char buf[1024] = {0};
//    test_t *src = calloc(1, sizeof(test_t));

//    src->num = 12;
//    src->letter = 'A';
//    strncpy(src->string, "this should work.", strlen("this should work."));

//    test_t *dest = calloc(1, sizeof(test_t));
//    // dest->string = calloc(64, sizeof(unsigned char));

// 	unsigned int packetsize, ps2;
   
// 	packetsize = pack(buf, "HHcs", 0, src->num, (int8_t) src->letter, src->string);

// 	printf("packet is %u bytes\n", packetsize);

//     packi16(buf, packetsize);

// 	unpack(buf, "HHc64s", &ps2, &dest->num, &dest->letter, &dest->string);

// 	printf("Packet size: %d bytes letter: '%c' number: %d string: %s \n", ps2, dest->letter, dest->num, dest->string);

//     return 0;
// }