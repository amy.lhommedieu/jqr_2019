#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#define DEBUG
#ifndef DEBUG
	#include "../inc/serial-adj.h"

    typedef struct test{
        u_int16_t num; //size of 2
        char letter; //size of 1
        char string[20]; //size of 20
        // u_int16_t str_2_size;
        char *str_2; //size of 8
    } test_t; //size of 40
#else
   #include "serial-adj.c"
#endif

// struct sockaddr_un {
//     unsigned short sun_family;  /* AF_UNIX */
//     char sun_path[108];
// }


int main(int argc, char *argv[]){
    int s;
    int num_sent;
    int num_recv;
    int len;

    struct sockaddr_un remote;
    unsigned char str[100];

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
        exit(1);
    }

    printf("...");

    remote.sun_family = AF_UNIX;
    strncpy(remote.sun_path, "/home/amy/repos/jqr_2019/3-8/unix_socket", sizeof(remote.sun_path) - 1);

    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1) {
        perror("connect error");
        exit(1);
    }

    printf("success\n");

    if(2 < argc && strncmp(argv[1], "string", 7) == 0){
        for(int i = 2; i < argc; i++){
            int arg_len = strlen(argv[i]);
            int copied = 0;
            while(copied < arg_len){
                strncpy(str, argv[i] + copied, 100);
                printf("length: %ld, string: %s\n", strlen(str), str);
                copied += 100;
                if ((num_sent=send(s, str, strlen(str), 0)) == -1) {
                    perror("send error");
                    exit(1);
                }

                if ((num_recv=recv(s, str, 100, 0)) > 0) {
                    str[num_recv] = '\0';
                    printf("echo> %s\n", str);
                } 

                else {
                    if (num_recv < 0) perror("recv");
                    else printf("Server closed connection\n");
                    exit(1);
                }
            }
        }
    }
    else if(1 == argc){
        unsigned char buf[100] = {0};
        test_t *src = calloc(1, sizeof(test_t));

        src->num = 12;
        src->letter = 'A';
        strncpy(src->string, "this should work.", strlen("this should work."));
        src->str_2 = calloc(64, sizeof(char));
        src->str_2 = "little bit longer of a string, in dynamic memory.";


        unsigned int packetsize, ps = 0;

        // future: send to tell dest how big to allocate for src->str_2_size

        packetsize = pack(buf, "hHcss", ps, src->num, (int8_t) src->letter, src->string, src->str_2);

        printf("packet is %u bytes\n", packetsize);

        packi16(buf, packetsize);

        // printf("buf: %ld\n", sizeof(buf));
        // if ((num_sent=send(s, buf, sizeof(buf), 0)) == -1) {
        if ((num_sent=send(s, buf, 50, 0)) == -1) {
            perror("send error");
            exit(1);
        }

        
        if(sizeof(buf) > num_sent){ /*added 25 JUNE*/
            int remaining_buf = sizeof(buf) - num_sent; /*tracks how much is left to send from the original buf*/
            while(0 != remaining_buf){
                // void * leftover_buf = &buf[num_sent];
                num_sent = num_sent + send(s, &buf[num_sent], remaining_buf, 0); /*send from the buf starting at the index where the last send left off.*/
                remaining_buf = sizeof(buf) - num_sent; /*update remaining_buf*/
            }
        }

        if ((num_recv=recv(s, str, 100, 0)) > 0) {
            str[num_recv] = '\0';
            printf("echo> ");
            printf_buf(str);
        } 

        else {
            if (num_recv < 0) perror("recv");
            else printf("Server closed connection\n");
            exit(1);
        }
        printf("done\n");
    }
    else{
        printf("Unexpected input.\n");
    }

    close(s);

    return 0;
}

/*Output
...success
packet is 24 bytes
100
echo> 024012650171161041051153211510411111710810032119111114107460000000000000000000000000000000000000000000000000000000000000000000000000000
done
*/