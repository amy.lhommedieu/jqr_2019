#ifndef SERIAL_CUSH_HEADER_LHOMMEDIEU
#define SERIAL_CUSH_HEADER_LHOMMEDIEU

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#define BUF_SIZE 1024

typedef struct test test_t;

//unions make it easier to tranform between the two.
union float32_t{    // float
    float frac;
    uint32_t whole;
};

union float64_t{    // double
    double frac;
    uint64_t whole;
};

// packi16() -- store a 16-bit int into a char buffer (like htons())
void packi16(unsigned char *buf, unsigned int i);


// packi32() -- store a 32-bit int into a char buffer (like htonl())
void packi32(unsigned char *buf, unsigned long int i);

// packi64() -- store a 64-bit int into a char buffer (like htonl())
void packi64(unsigned char *buf, unsigned long long int i);

// unpacki16() -- unpack a 16-bit int from a char buffer (like ntohs())
int16_t unpacki16(unsigned char *buf);

// unpacku16() -- unpack a 16-bit unsigned from a char buffer (like ntohs())
uint16_t unpacku16(unsigned char *buf);

// unpacki32() -- unpack a 32-bit int from a char buffer (like ntohl())
int32_t unpacki32(unsigned char *buf);

// unpacku32() -- unpack a 32-bit unsigned from a char buffer (like ntohl())
uint32_t unpacku32(unsigned char *buf);

// unpacki64() -- unpack a 64-bit int from a char buffer (like ntohl())
int64_t unpacki64(unsigned char *buf);

// unpacku64() -- unpack a 64-bit unsigned from a char buffer (like ntohl())
uint64_t unpacku64(unsigned char *buf);

/*
** pack() -- store data dictated by the format string in the buffer
**
**   bits |signed   unsigned   float   string
**   -----+----------------------------------
**      8 |   c        C         
**     16 |   h        H         f
**     32 |   l        L         d
**     64 |   q        Q         g
**      - |                               s
**
**  (16-bit unsigned length is automatically prepended to strings)
*/ 

uint16_t pack(unsigned char *buf, char *format, ...);

/*
** unpack() -- unpack data dictated by the format string into the buffer
**
**   bits |signed   unsigned   float   string
**   -----+----------------------------------
**      8 |   c        C         
**     16 |   h        H         f
**     32 |   l        L         d
**     64 |   q        Q         g
**      - |                               s
**
**  (string is extracted based on its stored length, but 's' can be
**  prepended with a max length)
*/
void unpack(unsigned char *buf, char *format, ...);
void check_buf(unsigned int size);
void printf_buf(unsigned char *buf);


#endif