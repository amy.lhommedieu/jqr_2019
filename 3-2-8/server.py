#!/usr/bin/env python3

import socket

HOST = '127.0.0.1'
PORT = 5556
msg_confirm = "Server received message."

#creating socket and connecting (using with does not need close())
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    new_socket, addr = s.accept()
    with new_socket:
        print("Connected by", addr)
        while 1:
            msg_recv = new_socket.recv(256)
            if not msg_recv:
                break
            print(msg_recv.decode('ascii'))
            new_socket.sendall(msg_confirm.encode('ascii'))