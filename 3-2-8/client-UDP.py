#!/usr/bin/env python3

import socket

SERVER = '127.0.0.1'
SERVER_PORT = 5555

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
    msg_send = "Today is Monday"

    s.sendto(msg_send.encode('ascii'), (SERVER, SERVER_PORT))

    msg_recv = s.recv(256)

    print(msg_recv.decode('ascii'))