#!/usr/bin/env python3

import socket

HOST = '127.0.0.1'
PORT = 5555

print(socket.gethostname())

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
    s.bind((HOST, PORT))

    msg_recv, new_socket = s.recvfrom(256)

    print('Message from client:', msg_recv.decode('ascii'))

    msg_send = "Message received"

    s.sendto(msg_send.encode('ascii'), new_socket)
