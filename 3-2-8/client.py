#!/usr/bin/env python3

import socket

HOST = '127.0.0.1'
PORT = 5556
msg_send = "Hello World"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.sendall(msg_send.encode('ascii'))
data = s.recv(256)

s.close()

print('Response:', str(data.decode('ascii')))